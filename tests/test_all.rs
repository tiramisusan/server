use std::path::Path;

mod utils;
use utils::*;

mod test_service;
mod test_core;

#[ctor::ctor]
fn init()
{
    #[cfg(feature="use_env_logger")]
    servoiardi::setup_env_logger(true);
}
