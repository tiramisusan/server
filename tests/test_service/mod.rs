use super::*;

#[cfg(feature="service_beverage")]
mod beverage;

#[cfg(feature="service_blog")]
mod blog;

#[cfg(feature="service_cgi")]
mod cgi;

mod files;

mod simple;

#[cfg(feature="service_paste")]
mod paste;
