use super::*;

async fn parse_table(
    ctx: MockContext<'_>,
    req: impl Request
)
    -> Vec<Vec<String>>
{
    let dom = http_get(ctx, req).await;

    let rtn = html::query(&dom, "tr")
        .into_iter()
        .skip(1)
        .map(
            |tr| html::subquery(&dom, tr, "td")
                .into_iter()
                .map(|td| td.inner_text(dom.get_ref().parser()).into_owned())
                .collect::<Vec<String>>()
        )
        .collect();

    log::debug!("{:?}", rtn);

    rtn
}

#[test]
fn test_lists_names()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let tab = parse_table(ctx, "/").await;
        assert_eq!(tab.len(), 3);
        assert_eq!(tab[0][0], "a.txt");
        assert_eq!(tab[1][0], "b.txt");
        assert_eq!(tab[2][0], "c.txt");
    }

    MockServer::default()
        .with_config(r#"files_test: raw_files { path = ./dir }"#)
        .with_asset(FileAsset("dir/a.txt", ""))
        .with_asset(FileAsset("dir/b.txt", ""))
        .with_asset(FileAsset("dir/c.txt", ""))
        .run(test_body)
}

#[test]
fn test_lists_sizes()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let tab = parse_table(ctx, "/").await;
        assert_eq!(tab.len(), 3);
        assert_eq!(tab[0][2], "1.0 B");
        assert_eq!(tab[1][2], "200.0 B");
        assert_eq!(tab[2][2], "3.0 kiB");
    }

    MockServer::default()
        .with_config(r#"files_test: raw_files { path = ./dir }"#)
        .with_asset(FileAsset("dir/1B.txt",   vec![b'x'; 1]))
        .with_asset(FileAsset("dir/200B.txt", vec![b'x'; 200]))
        .with_asset(FileAsset("dir/3kiB.txt", vec![b'x'; 3*1024]))
        .run(test_body)
}

#[test]
fn test_lists_timestamp()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let set_mtime_to_year = |year, path| {
            use chrono::TimeZone;
            let time = filetime::FileTime::from_system_time(
                chrono::Local.with_ymd_and_hms(year, 1, 1, 0, 0, 0)
                    .unwrap()
                    .into()
            );
            filetime::set_file_mtime(ctx.content_path(path), time)
                .unwrap();
        };

        set_mtime_to_year(2000, "dir/2000.txt");
        set_mtime_to_year(2020, "dir/2020.txt");

        let tab = parse_table(ctx, "/default/").await;
        assert_eq!(tab.len(), 2);
        assert!(tab[0][1].starts_with("2000-01-01 00:00:00"));
        assert!(tab[1][1].starts_with("2020-01-01 00:00:00"));

        let tab = parse_table(ctx, "/year-only/").await;
        assert_eq!(tab.len(), 2);
        assert_eq!(tab[0][1], "2000");
        assert_eq!(tab[1][1], "2020");
    }

    MockServer::default()
        .with_asset(FileAsset("dir/2000.txt", ""))
        .with_asset(FileAsset("dir/2020.txt", ""))
        .with_config(r#"
            : uri
            {
                mount = "default",
                default_files: raw_files
                {
                    path = ./dir
                }
            },
            : uri
            {
                mount = "year-only",
                year_only_files: raw_files
                {
                    path = ./dir,
                    dir_page = { datetime_format = "%Y" }
                }
            }
        "#)
        .run(test_body)
}


