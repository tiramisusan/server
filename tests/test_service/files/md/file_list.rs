use super::*;

use std::collections::HashSet;

async fn get_listed_files(ctx: MockContext<'_>, uri: &str) -> HashSet<String>
{
    let dom = http_get(ctx, uri).await;
    html::query(&dom, "li")
        .iter()
        .map(|node| node.inner_text(dom.get_ref().parser()).into_owned())
        .collect()
}

#[test]
fn test_file_list()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        assert_eq!(
            get_listed_files(ctx, "apples.md").await,
            HashSet::from(["pears".to_owned(), "oranges".to_owned()])
        );
        assert_eq!(
            get_listed_files(ctx, "pears.md").await,
            HashSet::from(["apples".to_owned(), "oranges".to_owned()])
        );
        assert_eq!(
            get_listed_files(ctx, "oranges.md").await,
            HashSet::from(["apples".to_owned(), "pears".to_owned()])
        );
    }

    create_server()
        .with_asset(FileAsset("apples.md",  "$$file-list"))
        .with_asset(FileAsset("pears.md",   "$$file-list"))
        .with_asset(FileAsset("oranges.md", "$$file-list"))
        .run(test_body)
}

#[test]
fn test_file_list_with_dir()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        assert_eq!(
            get_listed_files(ctx, "list.md").await,
            HashSet::from(["apples".to_owned(), "pears".to_owned()])
        );
    }

    create_server()
        .with_asset(FileAsset("list.md",          "$$file-list(subdir)"))
        .with_asset(FileAsset("subdir/apples.md", "# Apples"))
        .with_asset(FileAsset("subdir/pears.md",  "# Pears"))
        .run(test_body)
}
