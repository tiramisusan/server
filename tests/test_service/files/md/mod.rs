use super::*;

mod photo_config;
mod file_list;
mod index_md;

fn create_server() -> MockServer
{
    MockServer::default()
        .with_config(r#"md_test: md_files { path = . }"#)
}

#[test]
fn test_hello_world()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let dom = http_get(ctx, "index.md").await;

        assert_eq!(html::query_inner_text(&dom, "title"), "index");
        assert_eq!(html::query_inner_text(&dom, "p"), "Hello world!");
    }

    create_server()
        .with_asset(FileAsset("index.md", "Hello world!"))
        .run(test_body)
}
