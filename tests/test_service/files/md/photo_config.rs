use super::*;

async fn expect_img_suffix(ctx: MockContext<'_>, expect: &str)
{
    let dom = http_get(ctx, "index.md").await;
    let img_src = html::query_tag_attr(&dom, "img", "src");
    assert_eq!(img_src, "img.jpg".to_owned() + expect);
}

async fn expect_img_link(ctx: MockContext<'_>, expect: bool)
{
    let dom = http_get(ctx, "index.md").await;
    if expect
    {
        let a_href  = html::query_tag_attr(&dom, "a", "href");
        assert_eq!(a_href,  "img.jpg");
    }
    else
    {
        assert_eq!(html::query(&dom, "a").len(), 0);
    }
}

#[test]
fn test_default_settings()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        expect_img_suffix(ctx, "?xform=small").await;
        expect_img_link(ctx, true).await;
    }

    create_server()
        .with_asset(FileAsset("index.md", "$$photo(img.jpg) Caption"))
        .run(test_body)
}

#[test]
fn test_img_link_true()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        expect_img_suffix(ctx, "?xform=small").await;
        expect_img_link(ctx, true).await;
    }

    MockServer::default()
        .with_config(r#"
            md_test: md_files
            {
                path = .,
                matthewdown = { img_link = true }
            }
        "#)
        .with_asset(FileAsset("index.md", "$$photo(img.jpg) Caption"))
        .run(test_body)
}

#[test]
fn test_img_link_false()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        expect_img_suffix(ctx, "?xform=small").await;
        expect_img_link(ctx, false).await;
    }

    MockServer::default()
        .with_config(r#"
            md_test: md_files
            {
                path = .,
                matthewdown = { img_link = false }
            }
        "#)
        .with_asset(FileAsset("index.md", "$$photo(img.jpg) Caption"))
        .run(test_body)
}

#[test]
fn test_img_thumbnail_suffix_beef()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        expect_img_suffix(ctx, "?beef").await;
        expect_img_link(ctx, true).await;
    }

    MockServer::default()
        .with_config(r#"
            md_test: md_files
            {
                path = .,
                matthewdown = { img_thumbnail_suffix = "?beef" }
            }
        "#)
        .with_asset(FileAsset("index.md", "$$photo(img.jpg) Caption"))
        .run(test_body)
}
