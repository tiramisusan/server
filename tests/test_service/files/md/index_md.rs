use super::*;

#[test]
fn test_root_index()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let dom = http_get(ctx, "/").await;
        let h1  = html::query_inner_text(&dom, "h1");
        eprintln!("Found h1 {:?}", h1);
        assert_eq!(h1, "Root Index");
    }

    create_server()
        .with_asset(FileAsset("index.md",        "# Root Index"))
        .with_asset(FileAsset("subdir/index.md", "# Subdir Index"))
        .run(test_body)
}

#[test]
fn test_subdir_index()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let dom = http_get(ctx, "subdir/").await;
        let h1  = html::query_inner_text(&dom, "h1");
        eprintln!("Found h1 {:?}", h1);
        assert_eq!(h1, "Subdir Index");
    }

    create_server()
        .with_asset(FileAsset("index.md",        "# Root Index"))
        .with_asset(FileAsset("subdir/index.md", "# Subdir Index"))
        .run(test_body)
}

#[test]
fn test_can_be_disabled()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let dom = http_get(ctx, "/").await;
        let h1  = html::query_inner_text(&dom, "h1");
        eprintln!("Found h1 {:?}", h1);
        assert!(h1.starts_with("Directory Listing"));

        let dom = http_get(ctx, "subdir/").await;
        let h1  = html::query_inner_text(&dom, "h1");
        eprintln!("Found h1 {:?}", h1);
        assert!(h1.starts_with("Directory Listing"));
    }

    MockServer::default()
        .with_config(r#"
            md_test: md_files
            {
                path = .,
                use_index_files = false
            }
        "#)
        .with_asset(FileAsset("index.md",        "# Root Index"))
        .with_asset(FileAsset("subdir/index.md", "# Subdir Index"))
        .run(test_body)
}
