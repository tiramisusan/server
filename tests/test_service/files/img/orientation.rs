use super::*;

#[derive(PartialEq, Debug)]
enum Colour
{
    Red,
    Green,
    Yellow,
    Blue
}

impl From<&image::Rgb<u8>> for Colour
{
    fn from(rgb: &image::Rgb<u8>) -> Colour
    {
        match rgb.0.map(|chan| chan >= 128)
        {
            [true,  false, false] => Colour::Red,
            [false, true,  false] => Colour::Green,
            [true,  true,  false] => Colour::Yellow,
            [false, false, true ] => Colour::Blue,
            _ => panic!("Unknown color: {:?}", rgb)
        }
    }
}

fn get_corners(img: &image::DynamicImage) -> [Colour; 4]
{
    let rgb_img = img.to_rgb8();
    [
        rgb_img.get_pixel(0,                   0),
        rgb_img.get_pixel(rgb_img.width() - 1, 0),
        rgb_img.get_pixel(0,                   rgb_img.height() - 1),
        rgb_img.get_pixel(rgb_img.width() - 1, rgb_img.height() - 1)
    ].map(Colour::from)
}

async fn assert_image_is_right_way_up(ctx: MockContext<'_>, path: &str)
{
    eprintln!("Checking image dims of {:?}", path);
    let img: image::DynamicImage = http_get(ctx, path).await;

    let expect: [Colour; 4] = [
        Colour::Red, Colour::Green, Colour::Yellow, Colour::Blue
    ];
    assert_eq!(get_corners(&img), expect);
}

/// Check that the transformed original image is the right way up.
#[test]
fn test_original_image_xform()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        assert_image_is_right_way_up(ctx, "original.jpg").await;
        assert_image_is_right_way_up(ctx, "original.jpg?xform=thumbnail")
            .await;
    }

    create_server()
        .with_asset(TestAssets("exif_orientation_jpegs"))
        .run(test_body)
}

/// Check that the EXIF test images are the right way up when transformed.
///
/// This tests for a bug where the EXIF metadata is stripped from images when
/// they are transformed, and if it contains an orientation tag, that
/// information is lost, leading to thumbnails etc. in the wrong orientation.
///
#[test]
fn test_xform()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        assert_image_is_right_way_up(ctx, "exif-1.jpg?xform=thumbnail").await;
        assert_image_is_right_way_up(ctx, "exif-2.jpg?xform=thumbnail").await;
        assert_image_is_right_way_up(ctx, "exif-3.jpg?xform=thumbnail").await;
        assert_image_is_right_way_up(ctx, "exif-4.jpg?xform=thumbnail").await;
        assert_image_is_right_way_up(ctx, "exif-5.jpg?xform=thumbnail").await;
        assert_image_is_right_way_up(ctx, "exif-6.jpg?xform=thumbnail").await;
        assert_image_is_right_way_up(ctx, "exif-7.jpg?xform=thumbnail").await;
        assert_image_is_right_way_up(ctx, "exif-8.jpg?xform=thumbnail").await;
    }

    create_server()
        .with_asset(TestAssets("exif_orientation_jpegs"))
        .run(test_body)
}

