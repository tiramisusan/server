use super::*;

struct MockImage(&'static str, u32, u32);

impl Asset for MockImage
{
    fn copy_to(&self, dest: &Path)
    {
        log::info!(
            "Creating mock image (w={}, h={}) {:?}",
            self.1, self.2, self.0
        );
        image::RgbImage::new(self.1, self.2).save(dest.join(self.0))
            .expect("Error saving image");
    }
}

fn create_server() -> MockServer
{
    MockServer::default()
        .with_config(r#"img_test: img_files { path = . }"#)
        .with_asset(MockImage("test.jpg", 128, 128))
}

mod xform;
mod orientation;
