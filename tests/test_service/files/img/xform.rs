use super::*;

async fn assert_image_dims(ctx: MockContext<'_>, path: &str, w: u32, h: u32)
{
    log::info!("Checking image dims of {:?}", path);
    let img: image::DynamicImage = http_get(ctx, path).await;

    assert_eq!((img.width(), img.height()), (w, h));
}

/// Test image dimensions after 'small' transformation.
///
/// This transformation should produce 350 wide images.
///
#[test]
fn test_small_dims()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        assert_image_dims(ctx, "just_right.jpg?xform=small", 350, 700).await;
        assert_image_dims(ctx, "too_wide.jpg?xform=small",   350, 175).await;
        assert_image_dims(ctx, "too_thin.jpg?xform=small",   350, 700).await;
    }

    create_server()
        .with_asset(MockImage("just_right.jpg", 350, 700))
        .with_asset(MockImage("too_wide.jpg", 700, 350))
        .with_asset(MockImage("too_thin.jpg", 175, 350))
        .run(test_body)
}

/// Test image dimensions after 'thumbnail' transformation.
///
/// This transformation should produce 64x64 images regardless of input.
///
#[test]
fn test_thumbnail_dims()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        assert_image_dims(ctx, "too_wide.jpg?xform=thumbnail",  64, 64).await;
        assert_image_dims(ctx, "too_tall.jpg?xform=thumbnail",  64, 64).await;
        assert_image_dims(ctx, "too_small.jpg?xform=thumbnail", 64, 64).await;
        assert_image_dims(ctx, "too_big.jpg?xform=thumbnail",   64, 64).await;
    }

    create_server()
        .with_asset(MockImage("too_wide.jpg", 128, 64))
        .with_asset(MockImage("too_tall.jpg", 64, 128))
        .with_asset(MockImage("too_small.jpg", 32, 32))
        .with_asset(MockImage("too_big.jpg", 128, 128))
        .run(test_body)
}

const CACHE_PATH: &'static str = "img_test/thumbnail/test.jpg";
const CACHE_URL:  &'static str = "test.jpg?xform=thumbnail";

/// Test that requesting a transformed image twice in quick succession.
#[test]
fn test_duplicate_quick_queries()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        // Request the URL to generate a cached copy.
        // (the second request ensures the cached copy is flushed to disk)
        let a_fut = http_get::<_, _, Vec<u8>>(ctx, CACHE_URL);
        let b_fut = http_get::<_, _, Vec<u8>>(ctx, CACHE_URL);

        let (a, b) = tokio::join!(a_fut, b_fut);

        // Ensure that the cached copy is the same as our response.
        assert_eq!(a, b);
    }

    create_server()
        .run(test_body)
}

/// Test that when transforming an image, a cached copy is created.
#[test]
fn test_cache_is_created()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        // Request the URL to generate a cached copy.
        // (the second request ensures the cached copy is flushed to disk)
        let orig: Vec<u8> = http_get(ctx, CACHE_URL).await;
        let _:    Vec<u8> = http_get(ctx, CACHE_URL).await;

        // Find the hopefully cached copy.
        let cached = ctx.read_scratch_file(CACHE_PATH);

        // Ensure that the cached copy is the same as our response.
        assert_eq!(orig, cached);
    }

    create_server()
        .run(test_body)
}

/// Test that a cached transform is used until the original is updated.
#[test]
fn test_xform_cache_is_used()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        // Request the URL twice to generate a cached copy
        // (the second request ensures the cached copy is flushed to disk)
        http_get::<_, _, Vec<u8>>(ctx, CACHE_URL).await;
        http_get::<_, _, Vec<u8>>(ctx, CACHE_URL).await;

        // Replace the cached copy with an incorrectly sized image
        ctx.add_scratch_asset(MockImage(CACHE_PATH, 1, 1));
        // Ensure that the modified cached image is used.
        assert_image_dims(ctx, CACHE_URL, 1, 1).await;

        // Update the original image
        ctx.add_asset(MockImage("test.jpg", 128, 128));
        // Ensure that the cached copy is now updated.
        assert_image_dims(ctx, CACHE_URL, 64, 64).await;
    }

    create_server()
        .run(test_body)
}
