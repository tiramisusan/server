use super::*;

#[test]
fn test_simple()
{
    async fn test_hello_world(ctx: MockContext<'_>)
    {
        let text: String = http_get(ctx, "hello_world").await;
        assert_eq!(text, "Hello World!\n");
    }

    async fn test_static_moved(ctx: MockContext<'_>)
    {
        let rsp: reqwest::Response =
            http_get(ctx, "static_moved".with_check_status(false)).await;

        assert_eq!(rsp.status(), 301);
        assert_eq!(rsp.headers()["Location"], "/redirect");
    }

    async fn test_static_string(ctx: MockContext<'_>)
    {
        let text: String = http_get(ctx, "static_str").await;
        assert_eq!(text, "Static String!");
    }

    let server = MockServer::default()
        .with_config(r#"
            : uri
            {
                mount = "hello_world",
                hello_world_test: hello_world {}
            },
            : uri
            {
                mount = "static_moved",
                static_moved_test: static_moved { path = "/redirect" }
            },
            : uri
            {
                mount = "static_str",
                static_string_test: static_str { string = "Static String!" }
            }
        "#);

    server.run(test_hello_world);
    server.run(test_static_moved);
    server.run(test_static_string);
}
