use super::*;

#[test]
fn test_teapot()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let status: i32 = http_get(ctx, "".with_check_status(false)).await;
        assert_eq!(status, 418);
    }

    MockServer::default()
        .with_config("teapot_test: teapot {}")
        .run(test_body)
}
