use super::*;

/// Test that a simple blog has the correct title.
#[test]
fn test_hello_world()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let dom = http_get(ctx, "").await;
        assert_eq!(html::query_inner_text(&dom, "title"), "Test Blog");
    }

    MockServer::default()
        .with_config(r#"
            blog_test: blog
            {
                posts_path = .,
                name = "Test Blog"
            }
        "#)
        .run(test_body);
}

