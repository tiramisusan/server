use super::*;

#[test]
fn test_simple_paste()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        const UPLOAD: &str = "Pasted";
        let href: String = http_get(
            ctx,
            "/".with_method(reqwest::Method::POST).with_body(UPLOAD)
        ).await;
        assert_eq!(&href, "http://example.com/0\n");

        let download: String = http_get(ctx, "0").await;
        assert_eq!(download, UPLOAD);
    }

    MockServer::default()
        .with_config(r#"
            paste_test: paste { base_href = "http://example.com" }
        "#)
        .run(test_body)
}

#[test]
fn test_wraparound()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        const UPLOAD: &str = "Pasted";
        let req = || http_get::<_, _, String>(
               ctx,
               "/".with_method(reqwest::Method::POST).with_body(UPLOAD)
            );

        assert_eq!(req().await, "http://example.com/0\n");
        assert_eq!(req().await, "http://example.com/1\n");
        assert_eq!(req().await, "http://example.com/2\n");
        assert_eq!(req().await, "http://example.com/0\n");
        assert_eq!(req().await, "http://example.com/1\n");
        assert_eq!(req().await, "http://example.com/2\n");
    }

    MockServer::default()
        .with_config(r#"
            paste_test: paste
            {
                base_href = "http://example.com",
                max_files = 3
            }
        "#)
        .run(test_body)
}

#[test]
fn test_file_too_big()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let req = |n| http_get::<_, _, i32>(
                ctx,
                "/"
                    .with_method(reqwest::Method::POST)
                    .with_body(vec![b'x'; n])
                    .with_check_status(false)
            );

        assert_eq!(req(1024).await, 200);
        assert_eq!(req(1025).await, 413);
    }

    MockServer::default()
        .with_config(r#"
            paste_test: paste
            {
                base_href = "http://example.com",
                max_bytes = 1024
            }
        "#)
        .run(test_body)
}

#[test]
fn test_simple_paste_using_put()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        const UPLOAD: &str = "Pasted";
        let href: String = http_get(
            ctx,
            "/".with_method(reqwest::Method::PUT).with_body(UPLOAD)
        ).await;
        assert_eq!(&href, "http://example.com/0\n");

        let download: String = http_get(ctx, "0").await;
        assert_eq!(download, UPLOAD);
    }

    MockServer::default()
        .with_config(r#"
            paste_test: paste { base_href = "http://example.com" }
        "#)
        .run(test_body)
}

