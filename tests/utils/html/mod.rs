//use super::*;

use std::borrow::Cow;

pub fn query<'a>(dom: &'a tl::VDomGuard, s: &str) -> Vec<&'a tl::Node<'a>>
{
    dom.get_ref()
        .query_selector(s)
        .expect("Error compiling query")
        .map(|m| m.get(dom.get_ref().parser()).unwrap())
        .collect()
}

pub fn query_one<'a>(dom: &'a tl::VDomGuard, s: &str) -> &'a tl::Node<'a>
{
    let vec = query(dom, s);

    assert_eq!(vec.len(), 1);
    vec[0]
}

pub fn query_inner_text<'a>(dom: &'a tl::VDomGuard, s: &str) -> Cow<'a, str>
{
    query_one(dom, s)
        .inner_text(dom.get_ref().parser())
}

pub fn query_tag<'a>(dom: &'a tl::VDomGuard, s: &str) -> &'a tl::HTMLTag<'a>
{
    query_one(dom, s)
        .as_tag().unwrap()
}

pub fn query_tag_attr<'a>(dom: &'a tl::VDomGuard, s: & str, tag: &'a str)
    -> Cow<'a, str>
{
    query_tag(dom, s)
        .attributes()
        .get(tag)
        .expect("Could not find attribute")
        .expect("Empty attribute")
        .as_utf8_str()
}

pub fn subquery<'a>(
    dom:  &'a tl::VDomGuard,
    node: &'a tl::Node<'a>,
    s:    &str
)
    -> Vec<&'a tl::Node<'a>>
{
    let tl::Node::Tag(html) = node else { panic!("Expected HTML node") };
    html
        .query_selector(dom.get_ref().parser(), s)
        .expect("Error compiling query")
        .map(|m| m.get(dom.get_ref().parser()).unwrap())
        .collect()
}
