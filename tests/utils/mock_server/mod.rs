use super::*;

use std::{
    net::SocketAddr,
    future::Future,
    path::{Path, PathBuf}
};

mod context;
pub use context::MockContext;

pub struct MockServer
{
    root_dir:     tempdir::TempDir,
    content_dir:  PathBuf,
    scratch_dir:  PathBuf,
    config:       String,
}

/// A hacky way of expressing the type of a test callback.
///
/// I can't work out how to express this using HRTBs, so it just has its own
/// trait.
///
pub trait Callback<'a>:
    FnOnce(MockContext<'a>) -> Self::Return
{
    type Return: Future<Output=()> + 'a;
}

impl<'a, R, F> Callback<'a> for F
    where
        F: FnOnce(MockContext<'a>) -> R,
        R: Future<Output=()> + 'a
{
    type Return   = R;
}

impl Default for MockServer
{
    fn default() -> Self
    {
        let root_dir = tempdir::TempDir::new("servoiardi-test")
            .expect("Could not create temporary server directory");

        let scratch_dir = root_dir.path().join("scratch");
        let content_dir = root_dir.path().join("content");

        Self
        {
            root_dir,
            content_dir: content_dir.clone(),
            scratch_dir: scratch_dir.clone(),
            config: String::new()
        }
            .with_config(r#"addr = "127.0.0.1:0""#)
            .with_config(format!("scratch_directory = {:?}", scratch_dir))
            .with_asset(DirectoryAsset(&scratch_dir))
            .with_asset(DirectoryAsset(&content_dir))
    }
}

impl MockServer
{
    pub fn with_config(mut self, config: impl AsRef<str>) -> Self
    {
        if self.config.len() != 0
        {
            self.config.push_str(",\n");
        }

        self.config.push_str(config.as_ref());
        self
    }

    pub fn with_asset(self, files: impl Asset) -> Self
    {
        files.copy_to(&self.content_dir);
        self
    }

    pub fn run<C>(&self, cb: C) where C: for<'a> Callback<'a>
    {
        // Create the server config file.
        FileAsset("Server.conf", format!(": server {{\n{}\n}}\n", self.config))
            .copy_to(&self.content_dir);

        log::info!(": server {{\n{}\n}}\n", self.config);

        let loader = servoiardi::load_configs(std::iter::once(
                self.content_dir.join("Server.conf").to_str().unwrap()
            ))
            .unwrap_or_else(|e| {
                log::error!(
                    "Error loading test-server config:\n{}",
                    servoiardi::format_error_string(e.as_ref())
                );
                panic!("Error loading config!")
            });

        let server_block = loader.roots().next().unwrap();
        let server = servoiardi::Server::new(server_block, &loader, &None)
            .unwrap_or_else(|e| {
                panic!(
                    "Error creating server:\n{}",
                    servoiardi::format_error_string(e.as_ref())
                );
            });

        let addr = server.local_addr();
        let context = MockContext
            {
                addr:        &addr,
                content_dir: &self.content_dir,
                scratch_dir: &self.scratch_dir
            };

        let mut controller = server.start();
        log::info!("Started server on {}", context.addr);

        tokio::runtime::Runtime::new()
            .unwrap()
            .block_on(cb(context));

        controller.stop();
        log::info!("Stopped server");
    }
}
