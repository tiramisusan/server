use super::*;

#[derive(Copy, Clone)]
pub struct MockContext<'a>
{
    pub addr:        &'a SocketAddr,
    pub content_dir: &'a Path,
    pub scratch_dir: &'a Path
}

impl<'a> Into<SocketAddr> for MockContext<'a>
{
    fn into(self) -> SocketAddr { self.addr.clone() }
}

impl<'a> AsRef<Path> for MockContext<'a>
{
    fn as_ref(&self) -> &Path { self.content_dir }
}

impl<'a> MockContext<'a>
{
    pub fn content_path(&self, path: impl AsRef<Path>) -> PathBuf
    {
        self.content_dir.join(path)
    }

    pub fn scratch_path(&self, path: impl AsRef<Path>) -> PathBuf
    {
        self.scratch_dir.join(path)
    }

    pub fn add_asset(&self, files: impl Asset)
    {
        files.copy_to(self.content_dir);
    }

    pub fn add_scratch_asset(&self, files: impl Asset)
    {
        files.copy_to(self.scratch_dir);
    }

    pub fn read_file<P>(&self, path_ref: P) -> Vec<u8>
        where P: AsRef<Path>
    {
        let path = self.content_path(path_ref);
        let data = std::fs::read(&path).unwrap();

        log::info!("Read {} bytes from {:?}", data.len(), &path);

        data
    }

    pub fn read_scratch_file<P>(&self, path_ref: P) -> Vec<u8>
        where P: AsRef<Path>
    {
        self.read_file(self.scratch_path(path_ref))
    }
}
