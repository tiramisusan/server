use std::{
    path::{Path, PathBuf},
    fs
};

fn copy_from_dir(src: impl AsRef<Path>, dst: impl AsRef<Path>)
{
    for maybe_entry in fs::read_dir(src).unwrap()
    {
        let entry = maybe_entry.unwrap();
        let src_path = entry.path();
        let dst_path = dst.as_ref().join(entry.file_name());

        let meta = fs::metadata(&src_path).unwrap();

        if meta.is_file()
        {
            log::debug!("Copying file from {:?} to {:?}", &src_path, &dst_path);
            fs::copy(&src_path, &dst_path).unwrap();
        }
        else if meta.is_dir()
        {
            log::debug!("Creating directory {:?}", &dst_path);
            fs::create_dir(&dst_path).unwrap();
            copy_from_dir(&src_path, &dst_path);
        }
    }
}

pub trait Asset
{
    fn copy_to(&self, dest: &Path);
}

pub struct TestAssets<P>(pub P);

impl<P> Asset for TestAssets<P>
    where P: AsRef<Path>
{
    fn copy_to(&self, dest: &Path)
    {
        log::info!("Copying assets from {:?} to {:?}", self.0.as_ref(), dest);
        let manifest_dir = std::env::var("CARGO_MANIFEST_DIR")
            .expect("Expected to find CARGO_MANIFEST_DIR environment var");

        let src = <String as AsRef<Path>>::as_ref(&manifest_dir)
            .join("tests/assets")
            .join(self.0.as_ref());

        copy_from_dir(src, dest);
    }
}

pub struct FileAsset<P, D>(pub P, pub D);

impl<P, D> Asset for FileAsset<P, D>
    where P: AsRef<Path>, D: AsRef<[u8]>
{
    fn copy_to(&self, dest: &Path)
    {
        let path = dest.join(self.0.as_ref());
        fs::create_dir_all(path.parent().unwrap()).unwrap();
        fs::write(
            dest.join(self.0.as_ref()),
            self.1.as_ref()
        ).unwrap()
    }
}

pub struct DirectoryAsset<P>(pub P);

impl<P> Asset for DirectoryAsset<P>
    where P: AsRef<Path>
{
    fn copy_to(&self, dest: &Path)
    {
        fs::create_dir_all(dest.join(self.0.as_ref())).unwrap()
    }
}
