//use super::*;

mod request;
pub use request::Request;

mod response;
pub use response::Response;

pub async fn http_get<A, Q, P>(addr: A, req: Q) -> P
    where Q: Request, P: Response, A: Into<std::net::SocketAddr>
{
    let params = req.to_params();
    let uri = format!("http://{}/{}", addr.into(), params.uri);

    let mut req = reqwest::Client::builder()
        .redirect(reqwest::redirect::Policy::none())
        .build().expect("Could not build HTTP client")
        .request(params.method, uri)
        .headers(TryFrom::try_from(&params.headers).unwrap());

    if let Some(body) = params.body
    {
        req = req
            .header("Content-Length", body.len().to_string())
            .body(body)
    }

    let mut rsp = req.send().await.expect("Error performing HTTP GET");

    if params.check_status
    {
        rsp = rsp.error_for_status().expect("Unexpected non-OK HTTP response");
    }

    P::new(rsp).await
}
