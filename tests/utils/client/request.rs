//use super::*;

use std::collections::HashMap;

pub struct RequestParams
{
    pub uri:          String,
    pub headers:      HashMap<String, String>,
    pub check_status: bool,
    pub method:       reqwest::Method,
    pub body:         Option<Vec<u8>>
}

pub trait Request
{
    fn to_params(self) -> RequestParams;

    fn with_headers<K, V, H>(self, headers: H) -> RequestParams
        where
            HashMap<K, V>: FromIterator<(K, V)>,
            K: AsRef<str>,
            V: AsRef<str>,
            H: IntoIterator<Item=(K, V)>,
            Self: Sized
    {
        let mut params = self.to_params();
        for (k, v) in headers.into_iter()
        {
            params.headers.insert(k.as_ref().to_owned(), v.as_ref().to_owned());
        }
        params
    }

    fn with_check_status(self, check: bool) -> RequestParams
        where Self: Sized
    {
        let mut params = self.to_params();
        params.check_status = check;
        params
    }

    fn with_method(self, method: reqwest::Method) -> RequestParams
        where Self: Sized
    {
        let mut params = self.to_params();
        params.method = method;
        params
    }

    fn with_body(self, body: impl AsRef<[u8]>) -> RequestParams
        where Self: Sized
    {
        let mut params = self.to_params();
        params.body = Some(Vec::from(body.as_ref()));
        params
    }
}

impl Request for RequestParams
{
    fn to_params(self) -> RequestParams { self }
}

impl Request for &str
{
    fn to_params(self) -> RequestParams
    {
        RequestParams
        {
            uri:          self.to_owned(),
            headers:      Default::default(),
            check_status: true,
            method:       reqwest::Method::GET,
            body:         None
        }
    }
}
