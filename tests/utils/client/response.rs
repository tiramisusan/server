//use super::*;

#[async_trait::async_trait]
pub trait Response
{
    async fn new(rsp: reqwest::Response) -> Self;
}

#[async_trait::async_trait]
impl Response for String
{
    async fn new(rsp: reqwest::Response) -> Self
    {
        rsp.text()
            .await.expect("Error getting response text")
    }
}

#[async_trait::async_trait]
impl Response for Vec<u8>
{
    async fn new(rsp: reqwest::Response) -> Self
    {
        rsp.bytes()
            .await.expect("Error getting response bytes")
            .to_vec()
    }
}

#[async_trait::async_trait]
impl Response for tl::VDomGuard
{
    async fn new(rsp: reqwest::Response) -> Self
    {
        let text = rsp.text()
            .await.expect("Error getting response text");

        unsafe
        {
            tl::parse_owned(
                text,
                tl::ParserOptions::new()
                    .track_classes()
                    .track_ids()
            ).expect("Could not create VDom")
        }
    }
}

#[cfg(feature="use_image")]
#[async_trait::async_trait]
impl Response for image::DynamicImage
{
    async fn new(rsp: reqwest::Response) -> Self
    {
        let data = rsp.bytes()
            .await.expect("Error getting response bytes")
            .to_vec();

        image::load_from_memory(&data)
            .expect("Error loading image")
    }
}

#[async_trait::async_trait]
impl Response for i32
{
    async fn new(rsp: reqwest::Response) -> Self
    {
        rsp.status().as_u16() as i32
    }
}

#[async_trait::async_trait]
impl Response for reqwest::Response
{
    async fn new(rsp: reqwest::Response) -> Self { rsp }
}
