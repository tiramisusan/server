#!/bin/bash

set_exif_orientation()
{
    local FILE=$1
    local EXIF_TAG=$2

    exiv2 -M "set Exif.Image.Orientation Short $2" "$1"
}

create_exif()
{
    local ORIGINAL=$1
    local EXIF_TAG=$2
    local TRANSFORM=$3

    local OUTPUT="exif-${EXIF_TAG}.jpg"

    convert "$ORIGINAL" $TRANSFORM "$OUTPUT"
    set_exif_orientation "$OUTPUT" "$EXIF_TAG"
}

ORIGINAL="original.jpg"

create_exif "$ORIGINAL" 1 ""
create_exif "$ORIGINAL" 8 "-rotate 90"
create_exif "$ORIGINAL" 3 "-rotate 180"
create_exif "$ORIGINAL" 6 "-rotate 270"

create_exif "$ORIGINAL" 2 "-transpose -rotate 90"
create_exif "$ORIGINAL" 7 "-transpose -rotate 180"
create_exif "$ORIGINAL" 4 "-transpose -rotate 270"
create_exif "$ORIGINAL" 5 "-transpose"
