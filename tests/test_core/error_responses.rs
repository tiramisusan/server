use super::*;

fn mock_simple_html() -> MockServer
{
    MockServer::default()
        .with_config(r#"
            : uri
            {
                mount = "redirect",
                moved_test: static_moved { path = "index.html" }
            },
            files_test : raw_files { path = . }
        "#)
        .with_asset(FileAsset("index.html", "<!DOCTYPE html><html></html>"))
}

fn request_for_404() -> impl Request
{
    "not_exists"
        .with_check_status(false)
}

fn request_for_304() -> impl Request
{
    use chrono::{Duration, Utc};
    "index.html".with_headers([
            (
                "If-Modified-Since",
                (Utc::now() + Duration::seconds(10)).to_rfc2822()
            )
        ].into_iter())
        .with_check_status(false)
}

fn request_for_301() -> impl Request
{
    "redirect"
        .with_check_status(false)
}

fn request_for_403() -> impl Request
{
    "forbidden"
        .with_check_status(false)
}

fn request_for_405() -> impl Request
{
    "index.html"
        .with_check_status(false)
        .with_method(reqwest::Method::PUT)
}

fn request_for_501() -> impl Request
{
    "index.html"
        .with_check_status(false)
        .with_method(reqwest::Method::TRACE)
}

async fn check_text_title(ctx: MockContext<'_>, req: impl Request, title: &str)
{
    let rsp: String =
        http_get(ctx, req.with_headers([("Accept", "text/plain")])).await;

    assert_eq!(
        rsp.split('\n').next().unwrap(),
        &format!("=== {} ===", title)
    )
}

async fn check_html_title(ctx: MockContext<'_>, req: impl Request, title: &str)
{
    let dom = http_get(ctx, req.with_headers([("Accept", "text/html")])).await;

    assert_eq!(
        html::query_inner_text(&dom, "title"),
        title
    );
}

async fn check_no_body(ctx: MockContext<'_>, req: impl Request)
{
    let rsp: Vec<u8> = http_get(ctx, req).await;
    assert_eq!(rsp, Vec::<u8>::new());
}

async fn check_status(ctx: MockContext<'_>, req: impl Request, status: i32)
{
    let error_response: i32 = http_get(ctx, req).await;
    assert_eq!(error_response, status);
}

// Test the 404 (Not found) response
#[test]
fn test_404()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        check_status(ctx, request_for_404(), 404).await;
        check_text_title(ctx, request_for_404(), "404 - Not Found").await;
        check_html_title(ctx, request_for_404(), "404 - Not Found").await;
    }

    mock_simple_html().run(test_body);
}

// Test the 301 (Moved) response
#[test]
fn test_301()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        check_status(ctx, request_for_301(), 301).await;
        check_text_title(ctx, request_for_301(), "301 - Moved Permanently")
            .await;
        check_html_title(ctx, request_for_301(), "301 - Moved Permanently")
            .await;
    }

    mock_simple_html().run(test_body);
}

// Test the 304 (Not Modified) response.
#[test]
fn test_304()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        check_status(ctx, request_for_304(), 304).await;
        // 304 Should have no body
        check_no_body(ctx, request_for_304()).await;
    }

    mock_simple_html().run(test_body);
}

// Test the 403 (Forbidden) response.
#[test]
fn test_403()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        // Create a forbidden symlink.
        //
        // This link leads to the root directory, so if we try and access it,
        // the server should realize and forbid it.
        tokio::fs::symlink("/", ctx.content_path("forbidden")).await.unwrap();

        check_status(ctx, request_for_403(), 403).await;
        check_text_title(ctx, request_for_403(), "403 - Forbidden").await;
        check_html_title(ctx, request_for_403(), "403 - Forbidden").await;
    }

    mock_simple_html().run(test_body);
}

// Test for 405 (Method Not Allowed) response.
#[test]
fn test_405()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        check_status(ctx, request_for_405(), 405).await;
        check_text_title(ctx, request_for_405(), "405 - Method Not Allowed")
            .await;
        check_html_title(ctx, request_for_405(), "405 - Method Not Allowed")
            .await;
    }

    mock_simple_html().run(test_body);
}

// Test for 501 (Not Implemented) response.
#[test]
fn test_501()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        check_status(ctx, request_for_501(), 501).await;
        check_text_title(ctx, request_for_501(), "501 - Not Implemented").await;
        // No check for HTML since we fail during the request, we can't tell
        // whether to serve HTML or text.
    }

    mock_simple_html().run(test_body);
}
