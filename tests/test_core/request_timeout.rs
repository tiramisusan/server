use super::*;

use std::net::SocketAddr;
use tokio::{
    task::JoinSet,
    net::TcpStream,
    io
};

const NUM_THREADS: usize = 32;

async fn consume_connections(addr: &SocketAddr) -> JoinSet<()>
{
    let mut set = JoinSet::new();

    for _ in 0..NUM_THREADS
    {
        let addr_clone = addr.clone();
        let mut stream = TcpStream::connect(&addr_clone).await.unwrap();

        set.spawn(async move {
            io::copy(&mut stream, &mut io::sink()).await.unwrap();
        });
    }

    set
}

async fn check_is_accessible(ctx: MockContext<'_>)
{
    let res: i32 = http_get(ctx, "".with_check_status(false)).await;
    assert_eq!(res, 404);
}

async fn check_is_inaccessible(ctx: MockContext<'_>)
{
    let mut stream = TcpStream::connect(&ctx.addr).await.unwrap();

    assert_eq!(
        tokio::time::timeout(
            Duration::from_secs(1),
            io::copy(&mut stream, &mut io::sink())
        )
            .await.unwrap().unwrap(),
        0
    );
}

#[test]
fn test_high_timeout()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let _attack = consume_connections(&ctx.addr).await;

        tokio::time::sleep(std::time::Duration::from_secs(1)).await;
        check_is_inaccessible(ctx).await;
    }

    MockServer::default()
        .with_config("request_timeout = null")
        .with_config(r#"max_concurrent = 16"#)
        .run(test_body);
}

#[test]
fn test_low_timeout()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let _attack = consume_connections(&ctx.addr).await;

        tokio::time::sleep(std::time::Duration::from_secs(1)).await;
        check_is_accessible(ctx).await;
    }

    MockServer::default()
        .with_config(r#"request_timeout = { secs = 0, nanos = 500000000 }"#)
        .with_config(r#"max_concurrent = 16"#)
        .run(test_body);
}
