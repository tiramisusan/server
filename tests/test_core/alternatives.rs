use super::*;

#[test]
fn test_text_page_accept()
{
    const HTML: &'static str = "text/html";
    const TEXT: &'static str = "text/plain";
    const CASES: &'static [(&'static str, &'static str)] =
    &[
        // Test simple Accept header
        // Explicitly ask for HTML
        ("text/html", HTML),
        // Explicitly ask for TEXT
        ("text/plain", TEXT),

        // Test quality values of Accept header
        // Explicitly prefer HTML
        ("text/html;q=0.8, text/plain;q=0.4, */*;q=0.2", HTML),
        // Explicitly perfer TEXT
        ("text/html;q=0.4, text/plain;q=0.8, */*;q=0.2", TEXT),

        // Test ordering of Accept header
        // Prefer HTML due to order
        ("text/html, text/plain", HTML),
        // Prefer TEXT due to order
        ("text/plain, text/html", TEXT),

        // Test with real browser Accept header
        // Curl, wget (expect text)
        ("*/*", TEXT),
        // Firefox (expect HTML)
        ("text/html,application/xhtml+xml,application/xml;q=0.9,\
         image/avif,image/webp,*/*;q=0.8", HTML),
        // Edge (expect HTML)
        ("text/html, application/xhtml+xml, image/jxr, */*", HTML),
        // Chrome (expect HTML)
        ("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,\
         image/apng,*/*;q=0.8", HTML),
        // Opera (expect HTML)
        ("text/html,application/xml;q=0.9,application/xhtml+xml,image/png,\
        image/webp,image/jpeg,image/gif,image/x-xbitmap,*/*;q=0.1", HTML)
    ];

    async fn test_body(ctx: MockContext<'_>)
    {
        for (accept, expect) in CASES
        {
            log::info!("Using accept: {:?}", accept);
            log::info!("Expecting Content-Type: {:?}", expect);

            let req = "/404"
                .with_check_status(false)
                .with_headers([("Accept", *accept)]);

            let rsp: reqwest::Response = http_get(ctx, req).await;

            assert_eq!(rsp.headers()["Content-Type"], expect)
        }
    }

    MockServer::default().run(test_body)
}
