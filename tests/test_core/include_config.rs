use super::*;

async fn assert_exists(ctx: MockContext<'_>, n: i32)
{
    let uri = format!("{}/i_am_number.txt", n);
    let rsp: String = http_get(ctx, uri.as_str()).await;

    assert_eq!(rsp, format!("{}\n", n));
}

async fn assert_does_not_exist(ctx: MockContext<'_>, n: i32)
{
    let uri = format!("{}/i_am_number.txt", n);
    let rsp: i32 = http_get(ctx, uri.as_str().with_check_status(false)).await;

    assert_eq!(rsp, 404);
}

#[test]
fn test_include_none()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        assert_does_not_exist(ctx, 1).await;
        assert_does_not_exist(ctx, 2).await;
        assert_does_not_exist(ctx, 3).await;
    }

    MockServer::default()
        .with_assets("included_confs")
        .run(test_body);
}

#[test]
fn test_include_1()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        assert_exists(ctx, 1).await;
        assert_does_not_exist(ctx, 2).await;
        assert_does_not_exist(ctx, 3).await;
    }

    MockServer::default()
        .with_include("1.mounts.toml")
        .with_assets("included_confs")
        .run(test_body);
}

#[test]
fn test_include_2()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        assert_does_not_exist(ctx, 1).await;
        assert_exists(ctx, 2).await;
        assert_exists(ctx, 3).await;
    }

    MockServer::default()
        .with_include("subdir/2.mounts.toml")
        .with_assets("included_confs")
        .run(test_body);
}

#[test]
fn test_include_all()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        assert_exists(ctx, 1).await;
        assert_exists(ctx, 2).await;
        assert_exists(ctx, 3).await;
    }

    MockServer::default()
        .with_include("1.mounts.toml")
        .with_include("subdir/2.mounts.toml")
        .with_assets("included_confs")
        .run(test_body);
}


