use super::*;

#[test]
fn test_invalid_http_header()
{
    async fn test_body(ctx: MockContext<'_>)
    {
        let text: String = http_get(
            ctx,
            "/".with_headers([
                ("Invalid.Header", "ValidValue"),
                ("ValidHeader",    "ĨñṽãłĩđṼãłũẽ")
            ])
        )
            .await;

        assert_eq!(text, "Hello World!\n");
    }

    let server = MockServer::default()
        .with_config(r#"hello_world_test: hello_world {}"#);

    server.run(test_body);
}
