#!/bin/bash

set -e

TARGET=debug

build_dep_files()
{
    cargo rustc $* -- --emit dep-info
}

canonicalize_paths()
{
    local PWD=$(pwd)
    sort | grep -v '$^' | while read -r PTH
    do
        realpath --relative-to="$PWD" "$PTH"
    done
}

read_dep_files()
{
    printf "Reading deps from $@\n" 1>&2
    cat $@ | cut -d: -f1- | tr ' ' '\n' | canonicalize_paths
}

find_source_files()
{
    printf "Finding source files in $@\n" 1>&2
    find $1 -name '*.rs' | canonicalize_paths
}

check_dep_files()
{
    local SOURCE_DIR=$1
    shift
    comm \
        --total --check-order -13 \
        <(read_dep_files $@) \
        <(find_source_files $SOURCE_DIR)
}

build_dep_files --lib
build_dep_files --bin servoiardi
check_dep_files src/ target/debug/libservoiardi.d target/debug/servoiardi.d
