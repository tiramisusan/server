use super::*;

pub trait Kind: 'static + Sync + Send
{
    type Params: 'static;
    type Output: 'static;

    fn factory() -> &'static Factory<Self>
        where Self: Sized;
}
