use super::*;

macro_rules! register
{
    ($kind:ty, $comp:ty) =>
    {
        #[ctor::ctor]
        fn register()
        {
            <$kind as $crate::component::Kind>::factory()
                .add_component::<$comp>()
        }
    }
}

macro_rules! declare_kind
{
    ($v:vis $kind:ident, $($n:ident = $t:ty),*) =>
    {
        $v struct $kind;
        static FACTORY: $crate::component::Factory<$kind> =
            $crate::component::Factory::new();
        impl $crate::component::Kind for $kind
        {
            $(type $n = $t;)*

            fn factory() -> &'static $crate::component::Factory<$kind>
            {
                &FACTORY
            }
        }
    }
}

pub(crate) use register;
pub(crate) use declare_kind;
