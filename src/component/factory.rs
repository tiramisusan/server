use super::*;

use std::sync::Mutex;

pub(crate) struct Factory<KindTy>(Mutex<Inner<KindTy>>)
    where KindTy: Kind;

unsafe impl<KindTy> Sync for Factory<KindTy> where KindTy: Kind {}
unsafe impl<KindTy> Send for Factory<KindTy> where KindTy: Kind {}

struct Inner<KindTy>
    where KindTy: Kind
{
    builders:       Option<HashMap<&'static str, BoxedBuilder<KindTy>>>,
}

impl<KindTy> Factory<KindTy>
    where KindTy: Kind
{
    pub const fn new() -> Factory<KindTy>
    {
        Factory(Mutex::new(Inner { builders: None }))
    }

    pub fn can_build(&self, kind: &str) -> bool
    {
        let mut inner = self.0.lock().unwrap();

        inner.builders
            .get_or_insert_with(Default::default)
            .contains_key(kind)
    }

    pub fn create(
        &self,
        block:  &confiner::Block,
        params: &KindTy::Params
    )
        -> StdResult<KindTy::Output>
    {
        let mut inner = self.0.lock().unwrap();

        inner.builders.get_or_insert_with(Default::default)
            .get(block.kind.as_str())
            .ok_or_else(|| format!("Unknown kind of block: {:?}", block.kind))?
            .create(block, params)
    }

    pub fn add_component<CompTy>(&self)
        where CompTy: Component<KindTy> + ComponentOutput<KindTy>
    {
        let mut inner = self.0.lock().unwrap();

        let builder = new_builder::<CompTy, KindTy>();
        inner.builders.get_or_insert_with(Default::default)
            .insert(CompTy::name(), builder);
    }
}
