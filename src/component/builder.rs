use super::*;

use std::marker::PhantomData;

struct BuilderImpl<CompTy, KindTy>(PhantomData<(CompTy, KindTy)>);

impl<CompTy, KindTy> Builder<KindTy> for BuilderImpl<CompTy, KindTy>
    where
        CompTy: Component<KindTy> + ComponentOutput<KindTy>,
        KindTy: Kind
{
    fn create(&self, block: &confiner::Block, params: &KindTy::Params)
        -> StdResult<KindTy::Output>
    {
        use serde::Deserialize;

        // Deserialize the thing-specific config into a definite type.
        let des_conf = CompTy::Config::deserialize(block.as_ref())
            .annotate_with(|| format!(
                "Parse config for {:?}: {}", block.name, CompTy::name()
            ))?;

        // Initialize the thing
        CompTy::create(des_conf, params)
            .annotate_with(|| format!(
                "Building component {:?}: {}", block.name, CompTy::name()
            ))
            .map(CompTy::output)
    }
}

pub trait Builder<KindTy>
    where KindTy: Kind
{
    fn create(&self, block: &confiner::Block, params: &KindTy::Params)
        -> StdResult<KindTy::Output>;
}

pub type BoxedBuilder<KindTy> = Box<dyn Builder<KindTy> + Sync + Send>;

pub fn new_builder<CompTy, KindTy>() -> BoxedBuilder<KindTy>
    where
        CompTy: Component<KindTy> + ComponentOutput<KindTy>,
        KindTy: Kind
{
    let builder_impl = BuilderImpl::<CompTy, KindTy>(Default::default());
    Box::new(builder_impl)
}
