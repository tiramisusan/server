use super::*;

mod kind;
pub(crate) use kind::*;

mod factory;
pub(crate) use factory::*;

mod builder;
use builder::*;

mod macros;
pub(crate) use macros::*;

pub trait Component<KindTy>: 'static + Sync + Send
    where KindTy: Kind
{
    type Config: for<'de> serde::Deserialize<'de>;

    fn name() -> &'static str;

    fn create(conf: Self::Config, params: &KindTy::Params)
        -> StdResult<Self>
        where Self: Sized;
}

pub trait ComponentOutput<KindTy>: 'static
    where KindTy: Kind
{
    fn output(self) -> KindTy::Output;
}
