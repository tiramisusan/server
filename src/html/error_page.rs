use super::*;

pub fn make_error_page(err: &http::Error) -> TextPage
{
    let (status, message) = err.status_code_and_message();
    let details           = err.to_string();

    TextPage::new(&format!("{} - {}", status, message), details)
}
