use super::*;

pub type TemplateEnv = minijinja::Environment<'static>;
pub type TemplateSrc = minijinja::Source;

macro_rules! add_default_template
{
    ($source:ident, $name:literal) =>
    {
        ($source).add_template(
            $name,
            include_str!(concat!("../../default_templates/", $name))
        )
            .expect("Could not load default template")
    }
}

pub fn load_template_path(source: &mut TemplateSrc, path: impl AsRef<Path>)
{
    let meta = std::fs::metadata(path.as_ref())
        .unwrap_or_else(
            |e| panic!("Could not read metadata for {:?}: {}", path.as_ref(), e)
        );

    if meta.is_file()
    {
        load_template_file(source, path);
    }
    else if meta.is_dir()
    {
        load_template_dir(source, path);
    }
}

pub fn load_template_dir(source: &mut TemplateSrc, path: impl AsRef<Path>)
{
    let dir = std::fs::read_dir(path.as_ref())
        .unwrap_or_else(
            |e| panic!("Could not read template dir {:?}: {}", path.as_ref(), e)
        );

    for maybe_entry in dir
    {
        let entry = maybe_entry.expect("Could not read directory entry");
        load_template_path(source, &entry.path());
    }
}

pub fn load_template_file(source: &mut TemplateSrc, path: impl AsRef<Path>)
{
    let name = path.as_ref()
        .file_name()
        .unwrap_or_else(|| panic!("Could not get name of {:?}", path.as_ref()))
        .to_str()
        .unwrap_or_else(|| panic!("Invalid file name {:?}", path.as_ref()));

    if name.starts_with('.')
    {
        return;
    }

    let loaded = std::fs::read_to_string(path.as_ref())
        .unwrap_or_else(
            |e| panic!("Could not read template {:?}: {}", path.as_ref(), e)
        );

    source.add_template(name, loaded)
        .expect("Error adding templates");
}

fn new_template_env<P>(templates: &[P]) -> TemplateEnv
    where P: AsRef<Path>
{
    let mut source = minijinja::Source::new();

    add_default_template!(source, "dir_page.html");
    add_default_template!(source, "img_dir_page.html");
    add_default_template!(source, "page.html");
    add_default_template!(source, "blog_list.html");
    add_default_template!(source, "blog_post.html");
    add_default_template!(source, "paste_form.html");
    add_default_template!(source, "paste_list.html");
    add_default_template!(source, "text_page.html");
    add_default_template!(source, "text_page.txt");

    for path in templates
    {
        load_template_path(&mut source, path);
    }

    let mut env = TemplateEnv::new();

    env.set_source(source);

    env
}

pub struct Environment
{
    templates: TemplateEnv
}

impl Environment
{
    pub fn new<P>(templates: &[P]) -> Self
        where P: AsRef<Path>
    {
        Self { templates: new_template_env(templates) }
    }

    pub fn render_template<S>(&self, name: &str, data: S)
        -> http::Result<String>
        where S: serde::Serialize
    {
        let template = self.templates.get_template(name)?;
        let output   = template.render(data)?;

        Ok(output)
    }
}
