use super::*;

use serde::ser::Serialize;

pub struct TemplatePage<S>
    where S: Serialize
{
    title:    String,
    template: String,
    context:  S
}

impl<S> TemplatePage<S> where S: Serialize
{
    pub fn new(title: &str, template: &str, context: S) -> Self
    {
        Self
        {
            title:    String::from(title),
            template: String::from(template),
            context
        }
    }
}

#[async_trait]
impl<S> http::Render for TemplatePage<S> where S: Serialize + Sync + Send
{
    async fn render<'a, 'b>(
        self:     Box<Self>,
        response: &mut http::Response,
        html_env: &html::Environment,
        req:      Option<&'b mut http::Request<'a>>
    )
        -> http::Result<http::ContentData>
    {
        let body = html_env.render_template(&self.template, &self.context)?;

        Box::new(RawPage::new(&self.title, body))
            .render(response, html_env, req)
            .await
    }
}
