use super::*;

use std::io::Cursor;

#[derive(serde::Serialize)]
struct Context<'a>
{
    title: &'a str,
    body:  &'a str
}

/// A page containing a raw HTML body
pub struct RawPage
{
    title: String,
    body:  String
}

impl RawPage
{
    pub fn new(title: &str, body: String) -> Self
    {
        Self { title: String::from(title), body }
    }
}

#[async_trait]
impl http::Render for RawPage
{
    async fn render<'a, 'b>(
        self:     Box<Self>,
        response: &mut http::Response,
        html_env: &html::Environment,
        _req:     Option<&'b mut http::Request<'a>>
    )
        -> http::Result<http::ContentData>
    {
        use http::header::*;

        let context = Context { title: &self.title, body: &self.body };
        let page = html_env.render_template("page.html", context)?;

        response.headers()
            .add(&ContentLen(page.len()))?
            .add(&ContentType::from(MediaType::html()))?;

        Ok(Box::pin(Cursor::new(page)))
    }
}
