use super::*;

use std::io::Cursor;

#[derive(serde::Serialize)]
struct Context<'a>
{
    title: &'a str,
    text:  &'a str
}

/// A page containing a raw HTML body
pub struct TextPage
{
    title:         String,
    text:          String,
    html_template: String,
    text_template: String
}

impl TextPage
{
    pub fn new(title: &str, text: String) -> Self
    {
        Self {
            title: String::from(title),
            text,
            text_template: "text_page.txt".to_owned(),
            html_template: "text_page.html".to_owned()
        }
    }

    pub fn set_text_template(&mut self, template: &str)
    {
        self.text_template = template.to_owned();
    }

    pub fn set_html_template(&mut self, template: &str)
    {
        self.html_template = template.to_owned();
    }

    fn context(&self) -> Context
    {
        Context { title: &self.title, text: &self.text }
    }

    fn render_text(
        self:     Box<Self>,
        response: &mut http::Response,
        html_env: &html::Environment
    )
        -> http::Result<http::ContentData>
    {
        use http::header::*;

        let body = html_env.render_template(
            &self.text_template, self.context()
        )?;

        response.headers()
            .add(&ContentLen(body.len()))?
            .add(&ContentType::from(MediaType::text()))?;

        Ok(Box::pin(Cursor::new(body)))
    }

    async fn render_html<'a, 'b>(
        self:     Box<Self>,
        response: &mut http::Response,
        html_env: &html::Environment,
        request:  Option<&'b mut http::Request<'a>>
    )
        -> http::Result<http::ContentData>
    {
        use http::Render;

        let page = TemplatePage::new(
            &self.title, &self.html_template, self.context()
        );

        Box::new(page)
            .render(response, html_env, request)
            .await
    }

}

#[async_trait]
impl http::Render for TextPage
{
    async fn render<'a, 'b>(
        self:         Box<Self>,
        response:     &mut http::Response,
        html_env:     &html::Environment,
        mut request:  Option<&'b mut http::Request<'a>>
    )
        -> http::Result<http::ContentData>
    {
        if let Some(r) = request.as_mut()
        {
            use http::header::*;

            http::request::choose_alternative!
            {
                request=r,

                Text(media_type(MediaType::text()), uri_param("text")) =>
                    self.render_text(response, html_env),

                Html(media_type(MediaType::html()), uri_param("html")) =>
                    self.render_html(response, html_env, request).await
            }
        }
        else
        {
            self.render_text(response, html_env)
        }
    }
}
