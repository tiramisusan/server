use super::*;

pub(crate) struct Boundaries
{
    crlf:      Boundary,
    eof:       Boundary,
    multipart: Option<Boundary>
}

impl Default for Boundaries
{
    fn default() -> Self
    {
        Self
        {
            eof:       Boundary::new(b""),
            crlf:      Boundary::new(b"\r\n"),
            multipart: None
        }
    }
}

impl Boundaries
{
    pub fn set_multipart_boundary(&mut self, boundary: &[u8])
    {
        self.multipart = Some(Boundary::new(boundary));
    }

    pub fn get(&self, boundary: http::Until) -> http::Result<&Boundary>
    {
        match boundary
        {
            http::Until::CrLf      => Ok(&self.crlf),
            http::Until::Eof       => Ok(&self.eof),
            http::Until::Multipart =>
                match self.multipart.as_ref()
                {
                    Some(multipart) => Ok(multipart),
                    None => Err(http::Error::from(
                        "Tried to use multipart boundary without setting it. "
                    ))
                }
        }
    }
}
