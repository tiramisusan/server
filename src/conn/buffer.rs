use super::*;

pub(crate) struct Buffer<R>
{
    stream:      R,
    buf:         Vec<u8>,
    buf_index:   usize,
    limit:       Option<usize>
}

fn limit_slice_size<'a, T>(slice: &'a [T], n: Option<usize>) -> &'a [T]
{
    if let Some(limit) = n
    {
        if limit < slice.len()
        {
            return &slice[..limit];
        }
    }

    slice
}

const DEFAULT_CHUNK_SIZE: usize = 1024;
const DEFAULT_JIGGLE:     usize = 64;

impl<R> Buffer<R>
    where R: io::AsyncRead + Unpin + Sync + Send
{
    pub fn new(stream: R) -> Self
    {
        Self
        {
            stream,
            buf:        Vec::with_capacity(DEFAULT_CHUNK_SIZE + DEFAULT_JIGGLE),
            buf_index:  0,
            limit:      None
        }
    }

    pub fn set_limit(&mut self, limit: Option<usize>)
    {
        self.limit = limit
    }

    /// Get a reference to all remaining non-consumed data.
    pub fn remaining(&self) -> &[u8]
    {
        limit_slice_size(&self.buf[self.buf_index..], self.limit)
    }

    /// Consume some bytes and return a reference to them.
    pub fn consume(&mut self, num_requested: usize) -> &[u8]
    {
        let num_remain = self.remaining().len();
        let n = std::cmp::min(num_remain, num_requested);

        debug!("Consuming {} of {} bytes from connection", n, num_remain);
        assert!(n <= num_remain);

        let to_emit = &self.buf[self.buf_index..(self.buf_index + n)];

        if let Some(ref mut limit) = self.limit
        {
            *limit -= n
        }
        self.buf_index += n;

        to_emit
    }

    /// Whether we have reached EOF.
    ///
    /// At EOF, no further data will be read, but there may still be remaining
    /// data to consume.
    ///
    pub fn is_at_eof(&self) -> bool { self.next_read_size() == 0 }

    /// Whether we have no more data available.
    ///
    pub fn has_no_more_data(&self) -> bool
    {
        self.is_at_eof() && self.remaining().len() == 0
    }

    pub async fn read_from_stream(&mut self) -> io::Result<()>
    {
        if self.is_at_eof()
        {
            return Ok(())
        }

        self.discard_consumed();

        let orig_buf_len = self.buf.len();
        self.extend_buf_with_uninit(self.next_read_size());

        match self.stream.read(&mut self.buf[orig_buf_len..]).await
        {
            Ok(num_read) =>
            {
                self.buf.truncate(orig_buf_len + num_read);
                self.handle_num_read(num_read);
                Ok(())
            }
            Err(e) =>
            {
                self.buf.truncate(orig_buf_len);
                Err(e)
            }
        }
    }

    /// Extend the internal buffer with uninitialized memory.
    fn extend_buf_with_uninit(&mut self, to_read: usize)
    {
        // Extend the buffer with uninitialized memory.
        let new_len = self.buf.len() + to_read;
        self.buf.reserve(to_read);
        unsafe { self.buf.set_len(new_len); }
    }

    fn discard_consumed(&mut self)
    {
        self.buf.drain(..self.buf_index);
        self.buf_index = 0;
    }

    fn next_read_size(&self) -> usize
    {
        if let Some(limit) = self.limit
        {
            std::cmp::min(limit - self.remaining().len(), DEFAULT_CHUNK_SIZE)
        }
        else
        {
            DEFAULT_CHUNK_SIZE
        }
    }

    fn handle_num_read(&mut self, num_read: usize)
    {
        // Handle EOF - we limit ourselves to the number of bytes we
        // have remaining.
        if num_read == 0
        {
            debug!("Reached connection EOF");
            self.set_limit(Some(self.remaining().len()));
        }
        else
        {
            debug!("Read {} bytes from connection", num_read);
        }
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    macro_rules! assert_at_start
    {
        ($reader:expr) =>
        {{
            let slice: &[u8] = &[];
            assert_not_eof!($reader, slice);
        }}
    }

    macro_rules! assert_not_eof
    {
        ($reader:expr, $slice:expr) =>
        {{
            assert!(!$reader.is_at_eof());
            assert!(!$reader.has_no_more_data());
            assert_eq!($reader.remaining(), $slice);
        }}
    }

    macro_rules! assert_eof
    {
        ($reader:expr, $slice:expr) =>
        {{
            assert!($reader.is_at_eof());
            assert!(!$reader.has_no_more_data());
            assert_eq!($reader.remaining(), $slice);
        }}
    }

    macro_rules! assert_no_more_data
    {
        ($reader:expr) =>
        {{
            assert!($reader.is_at_eof());
            assert!($reader.has_no_more_data());
            assert_eq!($reader.remaining().len(), 0);
        }}
    }

    fn seq_vec(n: usize) -> Vec<u8> { (0..n).map(|i| i as u8).collect() }

    /// Test reading less than a single chunk.
    #[tokio::test]
    async fn test_short_read()
    {
        for n in [ 2, 8, DEFAULT_CHUNK_SIZE - 1, DEFAULT_CHUNK_SIZE ]
        {
            info!("Testing n={}", n);

            let vec        = seq_vec(n);
            let mut reader = Buffer::new(vec.as_slice());

            assert_at_start!(reader);

            reader.read_from_stream().await.unwrap();

            assert_not_eof!(reader, &vec);
            assert_eq!(reader.consume(n / 2), &vec[..(n / 2)]);

            assert_not_eof!(reader, &vec[(n / 2)..]);
            assert_eq!(reader.consume(n - (n / 2)), &vec[(n / 2)..]);

            reader.read_from_stream().await.unwrap();

            assert_no_more_data!(reader);
        }
    }

    /// Test reading more than a single chunk.
    #[tokio::test]
    async fn test_long_read()
    {
        for n in [
            DEFAULT_CHUNK_SIZE + 1,
            DEFAULT_CHUNK_SIZE * 10,
            DEFAULT_CHUNK_SIZE * 10 + 1
        ]
        {
            let vec = seq_vec(n);
            for read_size in [
                1,
                7,
                8,
                DEFAULT_CHUNK_SIZE - 1,
                DEFAULT_CHUNK_SIZE,
                DEFAULT_CHUNK_SIZE + 1
            ]
            {
                info!("Testing n={}, read_size={}", n, read_size);

                let mut reader = Buffer::new(vec.as_slice());
                let mut num_read = 0;

                assert_at_start!(reader);

                while !reader.has_no_more_data()
                {
                    while !reader.is_at_eof() &&
                        reader.remaining().len() < read_size
                    {
                        reader.read_from_stream().await.unwrap();
                    }

                    let slice = reader.consume(read_size);
                    assert_eq!(slice, &vec[num_read..(num_read + slice.len())]);
                    num_read += slice.len();
                }

                assert_no_more_data!(reader);

                assert_eq!(num_read, n);
            }
        }
    }

    #[tokio::test]
    async fn test_limited_read()
    {
        let vec = seq_vec(16);
        let mut reader = Buffer::new(vec.as_slice());

        assert_at_start!(reader);

        reader.read_from_stream().await.unwrap();
        assert_not_eof!(reader, &vec);

        reader.set_limit(Some(8));
        assert_eof!(reader, &vec[..8]);

        assert_eq!(reader.consume(8), &vec[..8]);
        assert_no_more_data!(reader);

        reader.set_limit(Some(8));
        assert_eof!(reader, &vec[8..16]);

        assert_eq!(reader.consume(8), &vec[8..16]);
        assert_no_more_data!(reader);
    }
}
