use super::*;

fn is_ascii(bytes: &[u8]) -> bool
{
    bytes.into_iter().find(|&&b| b >= 0x80).is_none()
}

pub fn latin1_to_str<'a>(bytes: Cow<[u8]>) -> Cow<str>
{
    // Check if our bytes are ASCII!
    if is_ascii(&bytes)
    {
        match bytes
        {
            Cow::Owned(s) =>
                // SAFETY: We just checked the string was ASCII.
                Cow::Owned(unsafe { String::from_utf8_unchecked(s) }),
            Cow::Borrowed(s) =>
                // SAFETY: We just checked the string was ASCII.
                Cow::Borrowed(unsafe { std::str::from_utf8_unchecked(s) })
        }
    }
    else
    {
        // Convert from latin-1 to utf-8
        let owned = bytes.into_iter()
            .map(|&b| b as char)
            .collect::<String>();

        Cow::Owned(owned)
    }
}

pub fn str_to_latin1<'a>(string: &'a str) -> Cow<'a, [u8]>
{
    if string.is_ascii()
    {
        string.as_bytes()
            .into()
    }
    else
    {
        string.chars()
            .map(|c| u8::try_from(c).unwrap_or(b'?'))
            .collect::<Vec<_>>()
            .into()
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    use test_case::test_case;

    #[test_case(
        b"abc".as_slice().into() => "abc".to_owned()
        ; "Borrowed ASCII"
    )]
    #[test_case(
        b"\xc0\xdf\xe7".as_slice().into() => "Àßç".to_owned()
        ; "Borrowed non-ASCII"
    )]
    #[test_case(
        b"abc".to_vec().into() => "abc".to_owned()
        ; "Owned ASCII"
    )]
    #[test_case(
        b"\xc0\xdf\xe7".to_vec().into() => "Àßç".to_owned()
        ; "Owned non-ASCII"
    )]
    #[test_case(
        b"".as_slice().into() => "".to_owned()
        ; "Borrowed empty"
    )]
    #[test_case(
        b"".to_vec().into() => "".to_owned()
        ; "Owned empty"
    )]
    fn test_latin1(input: Cow<[u8]>) -> String
    {
        let s      = latin1_to_str(input.clone()).into_owned();
        let output = str_to_latin1(&s);

        assert_eq!(&input, &output);

        s
    }
}
