use super::*;

pub(crate) struct Chunk<'a, R>
    where R: Unpin + Sync + Send
{
    matched:  bool,
    boundary: &'a Boundary,
    buffer:   &'a mut Buffer<R>
}

fn slice_or_none<T>(slice: &[T]) -> Option<&[T]>
{
    if slice.len() == 0 { None } else { Some(slice) }
}

async fn next_impl<'a, 'b, R>(
    matched:  &'a mut bool,
    boundary: &'a Boundary,
    buf:      &'b mut Buffer<R>
)
    -> io::Result<Option<&'b [u8]>>
    where R: io::AsyncRead + Unpin + Sync + Send
{
    if *matched
    {
        return Ok(None);
    }

    loop
    {
        match boundary.match_slice(buf.remaining())
        {
            // The case where we know that the first n characters cannot be a
            // match.
            MatchResult::NoMatch(n) =>
            {
                // If we have reached EOF, and do not have a match, we emit all
                // data in the buffer, even if partially matched, because it can
                // never form a full match anymore.
                if buf.is_at_eof()
                {
                    *matched = true;
                    return Ok(slice_or_none(buf.consume(buf.remaining().len())))
                }
                // If we have zero characters we can concretely say are
                // not-matching, and zero that we can say are matching, we need
                // some more data!
                //
                // Read some extra data and retry.
                else if n == 0
                {
                    buf.read_from_stream().await?;
                }
                // If we have some characters that we know don't match, emit
                // them.
                else
                {
                    return Ok(Some(buf.consume(n)));
                }
            }
            // The case where we have a match at position n.
            MatchResult::Match(n) =>
            {
                *matched = true;

                let to_consume = n + boundary.len();
                let data_and_boundary = buf.consume(to_consume);

                return Ok(slice_or_none(&data_and_boundary[..n]));
            }
        }
    }
}

unsafe fn break_borrow_check<'a, 'b, T>(x: &'a mut T) -> &'b mut T
{
    unsafe { &mut *(x as *mut T) }
}

impl<'a, R> Chunk<'a, R>
    where R: io::AsyncRead + Unpin + Sync + Send
{
    pub fn new(boundary: &'a Boundary, buffer: &'a mut Buffer<R>) -> Self
    {
        Self { boundary, buffer, matched: false }
    }

    /// Get a slice containing part of this chunk.
    pub async fn next<'b>(&'b mut self) -> io::Result<Option<&'b [u8]>>
    {
        next_impl(&mut self.matched, self.boundary, self.buffer).await
    }

    pub async fn collect(mut self) -> io::Result<Cow<'a, [u8]>>
    {
        let mut vec = Vec::new();

        while let Some(slice) = next_impl(
            &mut self.matched,
            self.boundary,
            // SAFETY: We only return the borrowed slice from the first loop!
            unsafe { break_borrow_check(self.buffer) }
        ).await?
        {
            if vec.len() == 0 && self.matched
            {
                return Ok(Cow::Borrowed(slice));
            }

            vec.extend_from_slice(slice);
        }

        Ok(Cow::Owned(vec))
    }
}
