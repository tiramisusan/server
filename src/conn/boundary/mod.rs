use super::*;

mod chunk;
use chunk::Chunk;

/// A state machine that matches bytes against a sequence.
pub(crate) struct Boundary
{
    /// The needle to search for.
    needle: Vec<u8>,

    /// The state to enter from the current state if the next byte is not
    /// matched.
    cancel_state: Vec<usize>
}

/// The result of matching on a slice.
#[derive(PartialEq, Debug)]
pub(crate) enum MatchResult
{
    /// There is no possible match in the first N bytes.
    NoMatch(usize),
    /// There is a definite match at the Nth byte.
    Match(usize)
}

/// A structure containing state for matching to a boundary.
pub(crate) struct Matcher<'a>
{
    boundary: &'a Boundary,
    state:    usize
}

/// Get the cancel state given a matched portion of a needle.
fn cancel_state_value(needle: &[u8]) -> usize
{
    for i in 1..needle.len()
    {
        let state = needle.len() - i;
        if needle[i..] == needle[..state]
        {
            return state;
        }
    }

    0
}

impl Boundary
{
    pub fn new(needle_data: &[u8]) -> Self
    {
        let needle = Vec::from(needle_data);
        let cancel_state: Vec<usize> = (0..needle.len())
            .map(|i| cancel_state_value(&needle[..i]))
            .collect();

        Self { needle, cancel_state }
    }

    fn len(&self) -> usize { self.needle.len() }

    fn match_slice(&self, slice: &[u8]) -> MatchResult
    {
        if self.len() == 0
        {
            return MatchResult::NoMatch(slice.len());
        }

        let mut matcher = Matcher { boundary: self, state: 0 };

        for (index, b) in slice.iter().enumerate()
        {
            matcher.match_byte(*b);

            if matcher.has_match()
            {
                return MatchResult::Match((1 + index) - matcher.state);
            }
        }

        MatchResult::NoMatch(slice.len() - matcher.state)
    }

    pub fn next_chunk<'a, R>(&'a self, buf: &'a mut Buffer<R>)
        -> Option<Chunk<'a, R>>
        where R: io::AsyncRead + Unpin + Sync + Send
    {
        if buf.has_no_more_data()
        {
            None
        }
        else
        {
            Some(Chunk::new(self, buf))
        }
    }
}

impl<'a> Matcher<'a>
{
    /// The next byte doesn't look right! Cancel the current match.
    pub fn cancel(&mut self)
    {
        self.state = self.boundary.cancel_state[self.state];
    }

    pub fn has_match(&self) -> bool
    {
        self.state == self.boundary.needle.len()
    }

    /// Match a byte.
    ///
    /// Return a slice of previously maybe-matched bytes that can now be
    /// emitted, and the current match result.
    pub fn match_byte(&mut self, b: u8)
    {
        while self.state > 0 && b != self.boundary.needle[self.state]
        {
            self.cancel();
        }

        if b == self.boundary.needle[self.state]
        {
            self.state += 1;
        }
    }
}


#[cfg(test)]
mod test
{
    use super::*;
    use MatchResult::*;
    use test_case::test_case;

    #[test_case("a",     "a--",        true,  0)]
    #[test_case("a",     "aaa",        true,  0)]
    #[test_case("a",     "-a-",        true,  1)]
    #[test_case("a",     "---",        false, 3)]
    #[test_case("a",     "",           false, 0)]
    #[test_case("ab",    "aba",        true,  0)]
    #[test_case("ab",    "aab",        true,  1)]
    #[test_case("ab",    "a",          false, 0)]
    #[test_case("ab",    "aaa",        false, 2)]
    #[test_case("1234",  "1121231234", true,  6)]
    #[test_case("12345", "1121231234", false, 6)]
    #[test_case("1212",  "12-12-12",   false, 6)]
    #[test_case("1212",  "12-1212",    true,  3)]
    #[test_case("1212",  "1212-12",    true,  0)]
    #[test_case("12123", "121212312",  true,  2)]
    fn test_match(needle: &str, haystack: &str, mtch: bool, exp: usize)
    {
        let boundary = Boundary::new(needle.as_bytes());
        assert_eq!(
            boundary.match_slice(haystack.as_bytes()),
            if mtch { Match(exp) } else { NoMatch(exp) }
        );
    }
}
