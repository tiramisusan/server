use super::*;

mod buffer;
use buffer::Buffer;

mod boundary;
use boundary::Boundary;

mod boundaries;
use boundaries::Boundaries;

mod latin1;
use latin1::{latin1_to_str, str_to_latin1};

//mod line_reader;
//use line_reader::LineReader;

//mod copy;

use std::net::SocketAddr;

pub struct Connection<Rd=net::tcp::OwnedReadHalf, Wr=net::tcp::OwnedWriteHalf>
{
    input:      Buffer<Rd>,
    output:     Wr,
    addr:       SocketAddr,
    boundaries: Boundaries
}

#[async_trait]
impl<Rd, Wr> http::HttpConnection for Connection<Rd, Wr>
    where
        Rd: tokio::io::AsyncRead + Sync + Send + Unpin,
        Wr: tokio::io::AsyncWrite + Sync + Send + Unpin
{
    fn set_multipart_boundary(&mut self, boundary: &[u8])
    {
        self.boundaries.set_multipart_boundary(boundary);
    }

    fn set_byte_limit(&mut self, limit: Option<usize>)
    {
        debug!("Setting HTTP connection limit {:?}", limit);
        self.input.set_limit(limit);
    }

    async fn read_line(&mut self, until: http::Until)
        -> http::Result<Option<Cow<str>>>
    {
        if let Some(chunk) = self.boundaries.get(until)?
            .next_chunk(&mut self.input)
        {
            let bytes = chunk.collect().await?;
            let string = latin1_to_str(bytes);
            Ok(Some(string))
        }
        else
        {
            Ok(None)
        }
    }

    async fn read_content<D>(&mut self, dst: &mut D, until: http::Until)
        -> http::Result<Option<usize>>
        where D: io::AsyncWrite + Unpin + Send
    {
        if let Some(mut chunk) = self.boundaries
            .get(until)?
            .next_chunk(&mut self.input)
        {
            let mut count: usize = 0;
            while let Some(slice) = chunk.next().await?
            {
                dst.write_all(slice).await?;
                count += slice.len();
            }

            info!("Received {} bytes of content.", count);

            Ok(Some(count))
        }
        else
        {
            Ok(None)
        }
    }

    async fn write_line(&mut self, s: &str) -> io::Result<()>
    {
        let bytes = str_to_latin1(s);

        self.output.write_all(&bytes).await?;
        self.output.write_all(b"\r\n").await?;

        Ok(())
    }

    async fn write_content<S>(&mut self, mut content: S) -> io::Result<()>
        where S: io::AsyncRead + Unpin + Send
    {
        match io::copy(&mut content, &mut self.output).await
        {
            Ok(bytes) =>
            {
                info!("Sent {} bytes of content.", bytes);
                Ok(())
            }
            Err(err)  =>
            {
                error!("Error sending content: {}", err);
                Err(err)
            }
        }
    }

    fn get_addr(&self) -> SocketAddr { self.addr }
}

impl Connection
{
    pub fn new(stream: net::TcpStream, addr: SocketAddr) -> Self
    {
        let (read, write) = stream.into_split();

        Self {
            input:      Buffer::new(read),
            output:     write,
            addr,
            boundaries: Default::default()
        }
    }
}

#[cfg(test)]
pub type MockConnection = Connection<&'static [u8], Vec<u8>>;

#[cfg(test)]
impl MockConnection
{
    pub fn new_mock(input: &'static [u8]) -> Self
    {
        Self {
            input:      Buffer::new(input),
            output:     Default::default(),
            addr:       "0.0.0.0:0".parse().unwrap(),
            boundaries: Default::default()
        }
    }

}
