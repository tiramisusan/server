use super::*;

fn format_console_log(
    buf:    &mut env_logger::fmt::Formatter,
    record: &log::Record<'_>
)
    -> Result<(), std::io::Error>
{
    let level_style  = buf.default_level_style(record.level());
    let level_letter = record.level().as_str().split_at(1).0;
    let filename     = record.file().unwrap_or("UNKNOWN");
    let lineno       = record.line().unwrap_or(0);

    use std::io::Write;
    writeln!(
        buf,
        "[{}] {} ({}:{})",
        level_style.value(level_letter),
        record.args(),
        buf.style().set_dimmed(true).value(filename),
        buf.style().set_dimmed(true).value(lineno)
    )?;

    Ok(())
}

pub fn setup_env_logger(test: bool)
{
    let env = env_logger::Env::new()
        .default_filter_or("INFO");

    let logger = env_logger::Builder::from_env(env)
        .format(format_console_log)
        .is_test(test)
        .build();

    add_logger(logger);
}

