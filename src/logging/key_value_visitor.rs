//use super::*;

use std::collections::BTreeMap;
use log::kv::{Key, Value};

#[derive(Default)]
pub struct KeyValueVisitor<'a>(pub BTreeMap<Key<'a>, Value<'a>>);

impl<'a> log::kv::Visitor<'a> for KeyValueVisitor<'a>
{
    fn visit_pair(&mut self, key: Key<'a>, value: Value<'a>)
        -> Result<(), log::kv::Error>
    {
        self.0.insert(key, value);
        Ok(())
    }
}

