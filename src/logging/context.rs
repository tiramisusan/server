use super::*;

use std::cell::RefCell;

pub struct Context
{
    request_id: usize,
    log_level:  RefCell<log::LevelFilter>
}

tokio::task_local!
{
    static CURRENT_CONTEXT: Context
}

impl Context
{
    fn new(request_id: usize) -> Self
    {
        Self { request_id, log_level: RefCell::new(log::max_level()) }
    }
}

pub fn run<F>(f: F, request_id: usize)
    -> tokio::task::futures::TaskLocalFuture<Context, F>
    where F: std::future::Future
{
    CURRENT_CONTEXT.scope(Context::new(request_id), f)
}

#[allow(dead_code)]
pub fn set_log_level(filter: log::LevelFilter)
{
    if CURRENT_CONTEXT.try_with(|ctx| ctx.log_level.replace(filter)).is_err()
    {
        error!("Called set_log_level with no context.");
    }
}

pub fn get_log_level() -> log::LevelFilter
{
    CURRENT_CONTEXT.try_with(|ctx| *ctx.log_level.borrow())
        .unwrap_or_else(|_| log::max_level())
}

pub fn get_request_id() -> Option<usize>
{
    CURRENT_CONTEXT.try_with(|ctx| ctx.request_id)
        .ok()
}
