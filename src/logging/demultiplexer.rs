//use super::*;

use std::sync::RwLock;

#[derive(Default)]
pub struct Demultiplexer
{
    loggers: RwLock<Vec<Box<dyn log::Log>>>
}

impl log::Log for Demultiplexer
{
    fn enabled(&self, metadata: &log::Metadata<'_>) -> bool
    {
        self.loggers.read().unwrap().iter()
            .any(|logger| logger.enabled(metadata))
    }

    fn log(&self, record: &log::Record<'_>)
    {
        for logger in self.loggers.read().unwrap().iter()
        {
            logger.log(record);
        }
    }

    fn flush(&self)
    {
        for logger in self.loggers.read().unwrap().iter()
        {
            logger.flush();
        }
    }
}

impl Demultiplexer
{
    pub fn add_logger(&self, logger: Box<dyn log::Log>)
    {
        self.loggers.write().unwrap()
            .push(logger)
    }
}
