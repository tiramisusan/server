use super::*;

fn is_enabled(metadata: &log::Metadata<'_>) -> bool
{
    context::get_log_level() >= metadata.level()
}

pub fn log_annotated_record<'a>(
    log:    &impl log::Log,
    record: &log::Record<'_>,
    req_id: usize
)
{
    // Extract the key-value pairs of the record
    let mut kvs = KeyValueVisitor::default();
    record.key_values().visit(&mut kvs).unwrap();

    // Insert our new key-value pair
    kvs.0.insert(
        log::kv::Key::from("request_id"),
        log::kv::Value::from(req_id));

    log.log(
        &record.to_builder()
            .args(format_args!("[{}] {}", req_id, record.args()))
            .key_values(&kvs.0)
            .build()
    );
}

#[derive(Default)]
pub struct Annotator<L>
{
    pub inner: L
}

impl<L> log::Log for Annotator<L>
    where L: log::Log
{
    fn enabled(&self, metadata: &log::Metadata<'_>) -> bool
    {
        self.inner.enabled(metadata) && is_enabled(metadata)
    }

    fn log(&self, record: &log::Record<'_>)
    {
        if is_enabled(record.metadata())
        {
            if let Some(req_id) = context::get_request_id()
            {
                log_annotated_record(&self.inner, record, req_id);
            }
            else
            {
                self.inner.log(record);
            }
        }
    }

    fn flush(&self)
    {
        self.inner.flush();
    }
}
