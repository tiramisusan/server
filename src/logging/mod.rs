use super::*;

pub mod context;

mod annotator;
use annotator::Annotator;

mod demultiplexer;
use demultiplexer::Demultiplexer;

mod key_value_visitor;
use key_value_visitor::KeyValueVisitor;

#[cfg(feature="use_env_logger")]
pub mod with_env_logger;
#[cfg(feature="use_env_logger")]
pub use with_env_logger::setup_env_logger;

type Logger = Annotator<Demultiplexer>;

static LOGGER_ONCE: std::sync::Once = std::sync::Once::new();
static mut LOGGER:  Option<Logger> = None;

fn get_logger() -> &'static Logger
{
    LOGGER_ONCE.call_once(|| {
        let logger = unsafe { LOGGER.insert(Logger::default()) };
        log::set_logger(logger)
            .expect("Logger was already set!");
        log::set_max_level(log::LevelFilter::Trace);
    });
    unsafe { LOGGER.as_ref().unwrap() }
}

pub fn add_logger(log: impl log::Log + 'static + Sized + Send)
{
    get_logger().inner.add_logger(Box::new(log))
}
