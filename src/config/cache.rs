use super::*;

#[derive(Default, Clone)]
pub enum CacheConfig
{
    #[default]
    Normal,
    NoCache,
    PurgeBefore(DateTime<Utc>)
}

impl<'de> serde::Deserialize<'de> for CacheConfig
{
    fn deserialize<D>(deserializer: D) -> Result<CacheConfig, D::Error>
        where D: serde::de::Deserializer<'de>
    {
        let s: String = serde::Deserialize::deserialize(deserializer)?;

        match s.as_str()
        {
            "normal"       => Ok(CacheConfig::Normal),
            "no-cache"     => Ok(CacheConfig::NoCache),
            "purge-on-restart" => Ok(CacheConfig::PurgeBefore(Utc::now())),
            _ => Err(serde::de::Error::unknown_variant(
                s.as_str(), &["normal", "no-cache", "purge-on-restart"]
            ))
        }
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    fn deser(s: &str) -> CacheConfig
    {
        use serde::de::*;
        CacheConfig::deserialize(
            value::StrDeserializer::<value::Error>::new(s)
        )
            .unwrap()
    }

    #[test]
    fn test_deserialize()
    {
        use CacheConfig::*;
        assert!(matches!(deser("normal"),           Normal));
        assert!(matches!(deser("no-cache"),         NoCache));
        assert!(matches!(deser("purge-on-restart"), PurgeBefore(_)));
    }
}

