use super::*;

/// Create a `Default` implementation from a `Deserialize` implementation.
///
/// The `Deserialize` implementation is fed an empty TOML table to produce a
/// default value for the structure.
///
macro_rules! impl_default_from_deser
{
    ($type:ty) =>
    {
        impl Default for $type
        {
            fn default() -> $type
            {
                use serde::de::*;
                let iter = std::iter::empty::<((), ())>();
                let map = value::MapDeserializer::<_, value::Error>::new(iter);
                <$type as serde::Deserialize>::deserialize(map)
                    .unwrap()
            }
        }

        #[cfg(test)]
        mod default_from_deser_test
        {
            use super::*;
            #[test]
            fn test_default_from_deser()
            {
                let _ = <$type as Default>::default();
            }
        }
    }
}

pub(crate) use impl_default_from_deser;
