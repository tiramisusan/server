use super::*;

fn img_link_default() -> bool { true }
fn img_suffix_default() -> String { "?xform=small".to_owned() }

/// A configuration for parsing matthewdown into HTML.
///
/// This is a wrapper around `matthewdown::HtmlConfig` that implements
/// `serde::Deserialize` with different defaults.
///
#[derive(serde::Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct MdConfig
{
    /// Place `<img>` tags inside `<a>` tags linking to the photo file.
    ///
    /// This is enabled by default in servoiardi.
    ///
    #[serde(default="img_link_default")]
    pub img_link: bool,

    /// Add a suffix to source URIs in `<img>` tags.
    ///
    /// This is the string `"?xform=small"` by default in servoiardi. Our
    /// image service will by default resize images to 350px wide when given
    /// this query string.
    ///
    #[serde(default="img_suffix_default")]
    pub img_thumbnail_suffix: String
}

impl_default_from_deser!(MdConfig);

impl MdConfig
{
    /// Convert this to a proper matthewdown config.
    pub fn md_html_config(&self) -> matthewdown::HtmlConfig
    {
        let mut config = matthewdown::HtmlConfig::default();

        config.img_link = self.img_link;
        config.img_thumbnail_suffix = Some(self.img_thumbnail_suffix.clone());

        config
    }
}
