use super::*;

pub fn load_configs<S>(paths: impl Iterator<Item=S>)
    -> StdResult<confiner::Loader>
    where S: AsRef<Path>
{
    let mut loader = confiner::FileLoader::default();

    for path in paths
    {
        let path = path.as_ref();
        loader.load(path)
            .annotate_with(|| format!("Error loading {:?}", path))?;
    }

    Ok(loader.into_inner())
}
