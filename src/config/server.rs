use super::*;

const fn default_num_threads()    -> usize { 4 }
const fn default_max_concurrent() -> usize { 64 }
const fn default_request_timeout() -> Option<Duration>
{
    Some(Duration::from_secs(30))
}

fn default_scratch_directory() -> PathBuf
{
    std::env::temp_dir().join("server-scratch")
}

/// A configuration for a server.
///
/// A servoiardi executable can host multiple servers.
///
#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ServerConfig
{
    /// The address and port to host the server on.
    pub addr: String,

    /// The maximum number of requests to concurrently handle.
    ///
    /// Excess requests will simply have their connections closed.
    ///
    #[serde(default="default_max_concurrent")]
    pub max_concurrent: usize,

    /// The default timeout for a connection to make an HTTP request.
    ///
    /// This timeout only applies while making the initial request, which is
    /// then dispatched to an endpoint. A connection may then spend additional
    /// time uploading or downloading data.
    ///
    #[serde(default="default_request_timeout")]
    pub request_timeout: Option<Duration>,

    /// The number of tokio threads to use for this server.
    #[serde(default="default_num_threads")]
    pub num_threads: usize,

    /// The directories to search for HTML templates.
    ///
    /// A default set of templates are compiled into the server, but templates
    /// can be placed in these directories to override them.
    ///
    #[serde(default)]
    pub template_directories: Vec<PathBuf>,

    /// The directory where services can store their state.
    ///
    /// The state for each service is stored in a subdirectory of the service
    /// name, and is accessed by the service using the `Scratch` class.
    ///
    /// By default, this is stored in the system temporary directory, where it
    /// may be erased on reboot.
    ///
    #[serde(default="default_scratch_directory")]
    pub scratch_directory: PathBuf,

    #[serde(default)]
    pub cache: CacheConfig
}
