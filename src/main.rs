#[derive(Default)]
struct Args
{
    override_addr: Option<std::net::SocketAddr>,
    write_ports:   Option<std::path::PathBuf>,
    config_files:  Vec<std::path::PathBuf>,
    extra:         Vec<String>
}

fn usage() -> !
{
    let exe_name = std::env::args().next().unwrap_or(String::from("server"));

    eprintln!(r#"
Servoiardi - My little webserver

Usage:
    {} [options] <server config [options] ...>

Options:
    --help, -h           Display this message.
    --addr, -a <addr>    Use the specified address for every server.
    --write-ports <file> Write the TCP ports in use to a file.
    --moka               Serve a little moka pot.
    --serve-files <path> Serve the specified file(s).

"#, &exe_name);
    std::process::exit(0)
}

fn fatal(msg: &str, err: &dyn std::error::Error) -> !
{
    eprintln!("{}:\n\n{}", msg, servoiardi::format_error_string(err));
    usage()
}

fn parse<T>(args: &mut impl Iterator<Item=String>) -> T
    where T:
        std::str::FromStr,
        <T as std::str::FromStr>::Err: std::error::Error
{
    args.next()
        .unwrap_or_else(|| { eprintln!("Expected value for option"); usage() })
        .parse()
        .unwrap_or_else(|e| fatal("Error parsing value", &e))
}

fn parse_args(mut args: impl Iterator<Item=String>) -> Args
{
    let mut rtn = Args::default();

    while let Some(next) = args.next()
    {
        match next.as_str()
        {
            "--help"|"-h"   => usage(),
            "--addr"|"-a"   => rtn.override_addr = Some(parse(&mut args)),
            "--write-ports" => rtn.write_ports = Some(parse(&mut args)),
            "--moka"        => rtn.extra.push(moka_serv()),
            "--serve-files" => rtn.extra.push(file_serv(parse(&mut args))),
            oth             => rtn.config_files.push(From::from(oth))
        }
    }

    rtn
}

fn moka_serv() -> String
{
    r#":server { addr="0.0.0.0:8080", m:moka {} }"#.to_owned()
}

fn file_serv(f: String) -> String
{
    format!(
        r#":server {{ addr="0.0.0.0:8080", f:raw_files {{ path={:?} }} }}"#,
        f
    )
}

fn main()
{
    #[cfg(feature="use_env_logger")]
    servoiardi::setup_env_logger(false);

    let args = parse_args(std::env::args().skip(1));

    let mut loader = servoiardi::load_configs(args.config_files.iter())
        .unwrap_or_else(|e| fatal("Error parsing arguments", e.as_ref()));

    for extra in args.extra
    {
        loader.add_document(&extra, ".")
            .unwrap_or_else(|e| fatal("Error adding extra config", e.as_ref()));
    }

    let servers = loader.roots()
        .map(|conf| servoiardi::Server::new(conf, &loader, &args.override_addr))
        .collect::<Result<Vec<servoiardi::Server>, _>>()
        .unwrap_or_else(|e| fatal("Could not create servers", e.as_ref()));

    if let Some(path) = &args.write_ports
    {
        let ports = servers.iter()
            .map(|serv| serv.local_addr().port().to_string())
            .collect::<Vec<_>>();

        std::fs::write(path, &ports.join("\n"))
            .unwrap_or_else(|e| fatal("Could not write ports", &e));
    }

    if servers.len() == 0
    {
        eprintln!("Found no servers.");
        usage();
    }
    else
    {
        let _controllers = servers.into_iter()
            .map(servoiardi::Server::start)
            .collect::<Vec<_>>();

        loop
        {
            std::thread::sleep(std::time::Duration::from_millis(500));
        }
    }
}
