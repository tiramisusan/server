use super::*;

/// A token for a wildcard!
#[derive(Clone, Copy, PartialEq, Debug)]
pub enum WildcardToken
{
    Question,
    SingleAst,
    DoubleAst,
    Ch(char)
}

/// Match a question mark.
///
/// `s` must contain one character.
///
fn match_question(s: &str) -> Match
{
    let mut chars = s.chars();

    if let Some(c1) = chars.next()
    {
        if chars.next().is_none() && c1 != '/'
        {
            Matched
        }
        else
        {
            Impossible
        }
    }
    else
    {
        Possible
    }
}

/// Match a single asterisk.
///
/// `s` must contain zero or more characters that aren't slashes.
///
fn match_single_ast(s: &str) -> Match
{
    if s.contains('/')
    {
        Impossible
    }
    else
    {
        Matched
    }
}

/// Match a double asterisk.
///
/// `s` must contain zero or more characters.
///
fn match_double_ast(_s: &str) -> Match
{
    Matched
}

/// Match a specific character.
///
/// `s` must contain only our specific single character.
///
fn match_ch(s: &str, ch: char) -> Match
{
    let mut chars = s.chars();

    if let Some(c1) = chars.next()
    {
        if chars.next().is_none() && c1 == ch
        {
            Matched
        }
        else
        {
            Impossible
        }
    }
    else
    {
        Possible
    }
}

impl Pattern for WildcardToken
{
    fn check(&self, s: &str) -> Match
    {
        use WildcardToken::*;
        match self
        {
            Question  => match_question(s),
            SingleAst => match_single_ast(s),
            DoubleAst => match_double_ast(s),
            Ch(ch)    => match_ch(s, *ch)
        }
    }
}

pub struct WildcardPattern(Vec<WildcardToken>);

impl<T> From<&T> for WildcardPattern
    where T: AsRef<str>
{
    fn from(s: &T) -> WildcardPattern
    {
        let mut toks = Vec::new();

        for ch in s.as_ref().chars()
        {
            use WildcardToken::*;
            match ch
            {
                '?' =>
                {
                    toks.push(Question);
                }
                '*' =>
                {
                    if toks.last() == Some(&SingleAst)
                    {
                        *(toks.last_mut().unwrap()) = DoubleAst
                    }
                    else
                    {
                        toks.push(SingleAst);
                    }
                }
                _   =>
                {
                    toks.push(Ch(ch));
                }
            }
        }

        WildcardPattern(toks)
    }
}

impl Pattern for WildcardPattern
{
    fn check(&self, s: &str) -> Match { self.0.as_slice().check(s) }
}

#[cfg(test)]
mod tests
{
    use super::*;

    use test_case::test_case;

    #[test_case("cat",     "cat"; "Simple Match")]
    #[test_case("c?t",     "cat"; "Single qmark")]
    #[test_case("???",     "cat"; "All qmarks")]
    #[test_case("*",       "cat"; "Wildcard")]
    #[test_case("cat*",    "cat"; "End zero-length wildcard")]
    #[test_case("*cat",    "cat"; "Initial zero-length wildcard")]
    #[test_case("**",      "c/t"; "Two wildcards with slash")]
    #[test_case("*?",      "cat"; "Wildcard and qmark")]
    #[test_case("*a*a*a*", "taramasalata"; "Alternating wildcards")]
    #[test_case("",        ""; "Empty string")]
    #[test_case("*",       ""; "Empty string 1 wildcard")]
    #[test_case("**",      ""; "Empty string 2 wildcards")]
    #[test_case("****",    ""; "Empty string 4 wildcards")]
    fn is_match(pattern: &str, s: &str)
    {
        println!("Checking pattern {:?} matches string {:?}", pattern, s);
        let pat = WildcardPattern::from(&pattern);
        assert!(pat.is_match(s));
    }

    #[test_case("cot",  "cat"; "Middle difference")]
    #[test_case("bat",  "cat"; "Initial difference")]
    #[test_case("cat?", "cat"; "Final missing qmark")]
    #[test_case("?cat", "cat"; "Initial missing qmark")]
    #[test_case("*",    "c/t"; "Single wildcard with slash")]
    #[test_case("c?t",  "c/t"; "Qmark with slash")]
    #[test_case("",     "x";   "Nonempty not empty")]
    #[test_case("x",    "";    "Empty not nonempty")]
    fn is_not_match(pattern: &str, s: &str)
    {
        println!(
            "Matching pattern {:?} does not match string {:?}", pattern, s
        );
        let pat = WildcardPattern::from(&pattern);
        assert!(!pat.is_match(s));
    }

    #[test_case("cat?", "cat"; "Final missing wildcard")]
    #[test_case("*x",   "cat"; "Initial wildcard")]
    fn is_possible(pattern: &str, s: &str)
    {
        println!(
            "Matching pattern {:?} possibly matches string {:?}", pattern, s
        );
        let pat = WildcardPattern::from(&pattern);
        assert_eq!(pat.check(s), Possible);
    }

}
