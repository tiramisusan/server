mod wildcard;
pub use wildcard::WildcardPattern;

/// A value to indicate whether a match exists.
#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Match
{
    /// The string matches perfectly.
    Matched,
    /// The string does not match, but if the string were longer, it might.
    Possible,
    /// The string does not match, and would not even if it were longer.
    Impossible,
}

use Match::*;

/// A trait implemented by things that can match strings.
pub trait Pattern
{
    /// Check whether a string matches the pattern.
    fn check(&self, s: &str) -> Match;

    /// Check whether a string is a perfect match.
    fn is_match(&self, s: &str) -> bool
    {
        self.check(s) == Matched
    }

}

/// We can implement our pattern trait for vectors of other pattern things!
///
/// This makes it easy to build up more complex matching machinery from simple
/// blocks!
impl<T> Pattern for &[T] where T: Pattern
{
    fn check(&self, s: &str) -> Match
    {
        let mut is_possible = false;

        // Handle the case of an empty pattern
        if self.len() == 0
        {
            // An empty string matches an empty pattern
            if s.len() == 0
            {
                return Matched;
            }
            // Otherwise, there is no match, and lengthening the string
            // could not yield a match.
            else
            {
                return Impossible;
            }
        }

        // Get an iterator of the byte offsets where the string can be split.
        // This is the start and end of each UTF-8 charatcter
        let offsets = s.char_indices()
            .map(|(ind, _ch)| ind)
            .chain(std::iter::once(s.len()));

        // Iterate through every splittable offset
        for ind in offsets
        {
            // Try splitting the string at this offset
            let (substr, remainder) = s.split_at(ind);

            // Try matching the first half of the split string against the
            // current matching token.
            match self[0].check(substr)
            {
                Matched =>
                {
                    // If we have a match, we examine the remainder of the
                    // string by checking it against the remainder of the
                    // pattern.
                    match (&self[1..]).check(remainder)
                    {
                        // If both halves of the string match our pattern,
                        // then we've found a match!
                        Matched    => { return Matched }
                        // If the second half of the string is a possible match,
                        // but the first half is a perfect match, then keep
                        // looking for a perfect match, but remember that this
                        // is a possible match anyway.
                        Possible   => { is_possible = true }
                        // If the second half is not a match we just keep
                        // iterating.
                        Impossible => { }
                    }
                }
                // If the first half of the string is a possible match, then we
                // just keep iterating.
                Possible   => { }
                Impossible =>
                {
                    // If the first half of the string cannot be a match, then
                    // we need to stop iterating. If found a possible match,
                    // return that, but otherwise, a match is impossible.
                    if is_possible
                    {
                        return Possible;
                    }
                    else
                    {
                        return Impossible;
                    }
                }
            }
        }

        // If we've iterated the whole string, but found no match yet, then we
        // have a possible match!
        return Possible;
    }
}
