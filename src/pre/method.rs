use super::*;

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
struct Config { allow: Vec<String> }

struct Method { allow: http::MethodSet }

impl component::Component<PreComponent> for Method
{
    type Config = Config;

    fn name() -> &'static str { "method" }

    fn create(conf: Config, _: &Params) -> StdResult<Self>
    {
        let mut allow = Default::default();

        for arg in conf.allow
        {
            use io::ErrorKind::Other;
            allow |= http::Method::from_str(&arg)
                .map_err(|_| io::Error::new(Other, "Unknown Method"))?;
        }

        Ok(Self { allow })
    }
}

component::register!(PreComponent, Method);

#[async_trait]
impl Pre for Method
{
    async fn handle<'uri>(&self, req: &http::Request<'_>, uri: &'uri http::Uri)
        -> Option<http::Result<&'uri http::Uri>>
    {
        if self.allow.contains(req.method())
        {
            Some(Ok(uri))
        }
        else
        {
            None
        }
    }
}
