use super::*;

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
struct Config
{
    #[serde(default)]
    mount:   String,
    #[serde(default)]
    pattern: Vec<String>,
}

struct Uri
{
    mount:   http::UriBuf,
    pattern: Vec<pattern::WildcardPattern>
}

impl component::Component<PreComponent> for Uri
{
    type Config = Config;

    fn name() -> &'static str { "uri" }

    fn create(conf: Config, _: &Params) -> StdResult<Self>
    {
        Ok(Self
        {
            mount: http::UriBuf::decode(&conf.mount)
                .map_err(|e| e.to_string())?,
            pattern: conf.pattern.iter().map(From::from).collect()
        })
    }
}

component::register!(PreComponent, Uri);

#[async_trait]
impl Pre for Uri
{
    async fn handle<'uri>(&self, _: &http::Request<'_>, uri: &'uri http::Uri)
        -> Option<http::Result<&'uri http::Uri>>
    {
        use pattern::Pattern;
        if let Some(mounted) = uri.mounted_uri(&self.mount)
        {
            if self.pattern.len() == 0 ||
                self.pattern.iter().any(|pat| pat.is_match(mounted.as_ref()))
            {
                return Some(Ok(mounted));
            }
        }

        None
    }
}
