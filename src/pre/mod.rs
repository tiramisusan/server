use super::*;

mod uri;
mod cache_override;
mod null;
mod method;

#[async_trait]
pub trait Pre: Sync + Send + 'static
{
    async fn cache_disposition(&self, uri: &http::Uri)
        -> Option<endpoint::CacheDisposition>
    {
        let _ = uri;
        None
    }

    async fn handle<'uri>(
        &self,
        request: &http::Request<'_>,
        uri: &'uri http::Uri
    )
        -> Option<http::Result<&'uri http::Uri>>
    {
        let _ = request;
        let _ = uri;
        Some(Ok(uri))
    }
}

pub(crate) struct Params;

component::declare_kind!(
    pub(crate) PreComponent, Params=Params, Output=Box<dyn Pre>
);

impl<T> component::ComponentOutput<PreComponent> for T
    where T: Pre
{
    fn output(self) -> Box<dyn Pre> { Box::new(self) }
}
