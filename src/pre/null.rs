use super::*;

#[derive(serde::Deserialize)]
struct Config {}

struct Null;

impl component::Component<PreComponent> for Null
{
    type Config = Config;

    fn name() -> &'static str { "null" }

    fn create(_: Config, _: &Params) -> StdResult<Self> { Ok(Self) }
}

component::register!(PreComponent, Null);

impl Pre for Null {}
