use super::*;

use std::pin::Pin;
use std::task::{Context, Poll};

async fn create_containing_dir(path: &Path) -> io::Result<()>
{
    if let Some(parent) = path.parent()
    {
        fs::create_dir_all(parent).await?;
    }

    Ok(())
}

async fn open_for_reading(path: &Path) -> io::Result<fs::File>
{
    fs::File::open(path).await
}

async fn open_for_writing(path: &Path) -> io::Result<fs::File>
{
    create_containing_dir(path).await?;
    fs::File::create(path).await
}

async fn open_for_modify(path: &Path) -> io::Result<fs::File>
{
    create_containing_dir(path).await?;
    fs::OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .truncate(false)
        .open(path)
        .await
}


/// A locked reference to a file.
///
/// This provides either read or read-write access to a managed file.
///
pub struct LockedFile<L>
    where L: Send + 'static
{
    file:  Option<fs::File>,
    sync:  bool,
    guard: Option<L>
}

impl<L> Borrow<fs::File> for LockedFile<L>
    where L: Send + 'static
{
    fn borrow(&self) -> &fs::File { self.file.as_ref().unwrap() }
}

impl<L> BorrowMut<fs::File> for LockedFile<L>
    where L: Send + 'static
{
    fn borrow_mut(&mut self) -> &mut fs::File { self.file.as_mut().unwrap() }
}

impl<L> Deref for LockedFile<L>
    where L: Send + 'static
{
    type Target = fs::File;
    fn deref(&self) -> &fs::File { self.file.as_ref().unwrap() }
}

impl<L> DerefMut for LockedFile<L>
    where L: Send + 'static
{
    fn deref_mut(&mut self) -> &mut fs::File { self.file.as_mut().unwrap() }
}

macro_rules! impl_forwarded_async_io
{
    ($( $method:ident ( $( $arg_n:ident : $arg_t:ty ),* ) -> $rtn:ty );*;) =>
    {$(
        fn $method(self: Pin<&mut Self>, $( $arg_n : $arg_t ),*)
            -> $rtn
        {
            let f: &mut fs::File = &mut self.get_mut();
            std::pin::Pin::new(f).$method($($arg_n),*)
        }
    )*}
}

impl<L> io::AsyncRead for LockedFile<L>
    where L: Send + Unpin + 'static
{
    impl_forwarded_async_io!(
        poll_read(cx: &mut Context, buf: &mut io::ReadBuf<'_>)
            -> Poll<io::Result<()>>;
    );
}

impl<L> io::AsyncWrite for LockedFile<L>
    where L: Send + Unpin + 'static
{
    impl_forwarded_async_io!(
        poll_write(c: &mut Context, b: &[u8]) -> Poll<io::Result<usize>>;
        poll_flush(c: &mut Context)           -> Poll<io::Result<()>>;
        poll_shutdown(c: &mut Context)        -> Poll<io::Result<()>>;
    );
}

impl<L> io::AsyncSeek for LockedFile<L>
    where L: Send + Unpin + 'static
{
    impl_forwarded_async_io!(
        start_seek(pos: io::SeekFrom) -> io::Result<()>;
        poll_complete(cx: &mut Context) -> Poll<io::Result<u64>>;
    );
}

impl<L> LockedFile<L>
    where L: file_guard::ReadGuard
{
    /// Create a new readable locked file.
    pub async fn new_read(path: &Path, guard: L) -> io::Result<Self>
    {
        info!("Opening scratch file {:?} for reading", path);
        let file = open_for_reading(path).await?;
        Ok(Self { file: Some(file), sync: false, guard: Some(guard) })
    }
}

impl<L> LockedFile<L>
    where L: file_guard::WriteGuard
{
    /// Create a new writable locked file.
    pub async fn new_write(path: &Path, guard: L) -> io::Result<Self>
    {
        info!("Opening scratch file {:?} for writing", path);
        let file = open_for_writing(path).await?;
        Ok(Self { file: Some(file), sync: true, guard: Some(guard) })
    }

    pub async fn new_modify(path: &Path, guard: L) -> io::Result<Self>
    {
        info!("Opening scratch file {:?} for modifying", path);
        let file = open_for_modify(path).await?;
        Ok(Self { file: Some(file), sync: true, guard: Some(guard) })
    }
}

impl<L> Drop for LockedFile<L>
    where L: Send + 'static
{
    fn drop(&mut self)
    {
        if self.sync
        {
            let guard = self.guard.take().unwrap();
            let file  = self.file.take().unwrap();
            tokio::spawn(async move {
                let _ = file.sync_all().await;
                drop(guard);
            });
        }
    }
}
