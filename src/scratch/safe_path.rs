use super::*;

/// Check if a byte is safe to include as a path component.
fn is_safe_byte(b: &u8) -> bool
{
    matches!(*b, b'a'..=b'z' | b'A'..=b'Z' | b'0'..=b'9' | b'-' | b'_' | b'.')
}

/// Make a path component safe!
pub fn make_safe_path<'a>(path: &'a str) -> Cow<'a, str>
{
    let bytes = path.as_bytes();

    if bytes.iter().all(is_safe_byte)
    {
        Cow::Borrowed(path)
    }
    else
    {
        let owned = bytes.iter()
            .map(|b| {
                if is_safe_byte(b)
                {
                    unsafe { String::from_utf8_unchecked(vec![*b]) }
                }
                else
                {
                    format!("%{:02x}", b)
                }
            })
            .collect::<String>();

        Cow::Owned(owned)
    }
}
