use super::*;

/// A trait to mark guards that can be held to read a file.
pub trait ReadGuard: Send + 'static {}

/// A trait to mark guards that can be held to read a file.
pub trait WriteGuard: Send + 'static {}

impl ReadGuard  for <DefaultLock as file_lock::FileLock>::ReadGuard  {}
impl WriteGuard for <DefaultLock as file_lock::FileLock>::WriteGuard {}

