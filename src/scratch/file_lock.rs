use super::*;

/// A trait for read/write locks.
#[async_trait]
pub trait FileLock: Default
{
    type ReadGuard:  file_guard::ReadGuard;
    type WriteGuard: file_guard::WriteGuard;

    async fn write(self: Arc<Self>) -> Self::WriteGuard;
    async fn read(self: Arc<Self>)  -> Self::ReadGuard;
}

#[async_trait]
impl FileLock for sync::RwLock<()>
{
    type ReadGuard  = sync::OwnedRwLockReadGuard<()>;
    type WriteGuard = sync::OwnedRwLockWriteGuard<()>;

    async fn write(self: Arc<Self>) -> Self::WriteGuard
    {
        self.write_owned().await
    }

    async fn read(self: Arc<Self>) -> Self::ReadGuard
    {
        self.read_owned().await
    }
}
