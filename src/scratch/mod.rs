use super::*;

use tokio::{fs, sync};
use std::{
    borrow::{Borrow, BorrowMut},
    ops::{Deref, DerefMut}
};

mod safe_path;
use safe_path::make_safe_path;

mod locked_file;
mod file_guard;
mod file_lock;

mod test;

pub type DefaultLock = sync::RwLock<()>;

/// A structure for managing a directory of files owned by a service.
///
/// The structure provides exclusive write, multiple read access to the
/// descendants of the managed directory.
pub struct Scratch<L=DefaultLock>
{
    /// The path of the directory being managed.
    path: PathBuf,

    /// A map of paths to locks.
    ///
    /// A read-lock is taken out against the locks corresponding to files open
    /// for reading, and a write-lock is taken against the locks corresponding
    /// to files open for writing.
    ///
    /// TODO: USE A WEAKMAP
    locks: sync::Mutex<HashMap<PathBuf, Arc<L>>>
}

impl<L> Scratch<L>
{
    pub fn new(base: impl AsRef<Path>, name: &str) -> Self
    {
        Self {
            path: base.as_ref().join(&*make_safe_path(name)),
            locks: Default::default()
        }
    }

    /// Get a path indexed by a key.
    fn get_path<'a>(&self, name: &[&str]) -> PathBuf
    {
        let mut rtn = self.path.clone();

        for component in name.iter()
        {
            rtn.push(&*make_safe_path(component));
        }

        rtn
    }
}

/// Get whether a path is older than a particular age.
///
/// If it does not exist, it is considered older.
///
async fn is_older(path: &Path, age: SystemTime) -> bool
{
    if let Ok(modtime) = fs::metadata(&path).await
        .and_then(|meta| meta.modified())
    {
        modtime <= age
    }
    else
    {
        true
    }
}

pub type WriteFile<L=DefaultLock> =
    locked_file::LockedFile<<L as file_lock::FileLock>::WriteGuard>;

pub type ReadFile<L=DefaultLock> =
    locked_file::LockedFile<<L as file_lock::FileLock>::ReadGuard>;

impl<L> Scratch<L>
    where L: file_lock::FileLock
{
    /// Find a lock associated with a path.
    async fn acquire_lock(&self, path: &Path) -> Arc<L>
    {
        self.locks.lock().await
            .entry(path.to_owned()).or_default().clone()
    }

    /// Open a file for reading.
    pub async fn open_read<'a>(&self, name: &[&str]) -> io::Result<ReadFile<L>>
    {
        let path = self.get_path(name);
        let guard = self.acquire_lock(&path).await.read().await;

        ReadFile::<L>::new_read(&path, guard).await
    }

    /// Open a file for writing.
    ///
    /// The file is created if it does not exist, and truncated if it does.
    ///
    pub async fn open_write<'a>(&self, name: &[&str])
        -> io::Result<WriteFile<L>>
    {
        let path = self.get_path(name);
        let guard = self.acquire_lock(&path).await.write().await;

        WriteFile::<L>::new_write(&path, guard).await
    }

    /// Open a file for writing if it is older than a certain time.
    ///
    /// The file is created if it does not exist, and truncated if it does.
    ///
    pub async fn open_write_if_older<'a>(&self, name: &[&str], age: SystemTime)
        -> io::Result<Option<WriteFile<L>>>
    {
        let path = self.get_path(name);

        // We do an initial check whether the file is older. If not, then we can
        // avoid taking the lock, because no matter what, the file should not
        // become older.
        if is_older(&path, age).await
        {
            let guard = self.acquire_lock(&path).await.write().await;

            // Perform a second check while holding the locked file.
            if is_older(&path, age).await
            {
                let file = WriteFile::<L>::new_write(&path, guard).await?;
                return Ok(Some(file));
            }
        }

        Ok(None)
    }

    /// Open a file for reading and writing.
    ///
    /// The file is created if it does not exist, and is NOT truncated.
    ///
    pub async fn open_modify<'a>(&self, name: &[&str])
        -> io::Result<WriteFile<L>>
    {
        let path = self.get_path(name);
        let guard = self.acquire_lock(&path).await.write().await;

        WriteFile::<L>::new_modify(&path, guard).await
    }
}
