#[cfg(test)]
use super::*;

#[cfg(test)]
mod test_inner
{
    use super::*;

    #[cfg(test)]
    #[ctor::ctor]
    fn init()
    {
        #[cfg(feature="use_env_logger")]
        setup_env_logger(true);
    }

    fn temp_dir() -> tempdir::TempDir
    {
        tempdir::TempDir::new("servoiardi-scratch-test").unwrap()
    }

    async fn write_hello_world(scratch: &Scratch, key: &[&str])
    {
        let mut f = scratch.open_write(key).await.unwrap();
        f.write_all(b"Hello world!").await.unwrap();
    }

    async fn assert_hello_world(scratch: &Scratch, key: &[&str])
    {
        let mut s = String::new();
        let mut f = scratch.open_read(key).await.unwrap();
        f.read_to_string(&mut s).await.unwrap();

        assert_eq!(s, "Hello world!");
    }

    #[tokio::test]
    /// Spawn many async writers and readers, and ensure they have atomic access.
    async fn test_write_then_async_read()
    {
        use std::sync::atomic::*;

        let tmp       = temp_dir();
        let scratch   = Arc::new(Scratch::<DefaultLock>::new(&tmp, "test"));
        let write_ctr = Arc::new(AtomicU32::new(0));
        let read_ctr  = Arc::new(AtomicU32::new(0));

        async fn write_job(
            scratch: Arc<Scratch>, w_ctr: Arc<AtomicU32>
        )
        {
            let mut f = scratch.open_write(&["file"]).await.unwrap();

            w_ctr.fetch_add(1, Ordering::SeqCst);
            let val = w_ctr.load(Ordering::SeqCst).to_string();

            f.write_all(val.as_bytes()).await.unwrap();
        }

        async fn read_job(
            scratch: Arc<Scratch>, w_ctr: Arc<AtomicU32>, r_ctr: Arc<AtomicU32>
        )
        {
            r_ctr.fetch_add(1, Ordering::SeqCst);

            let mut s = String::new();
            let mut f = scratch.open_read(&["file"]).await.unwrap();
            f.read_to_string(&mut s).await.unwrap();

            let val = w_ctr.load(Ordering::SeqCst).to_string();
            info!("{}", val);
            assert_eq!(val, s);
        }

        const NUM_ITERS: u32 = 100;

        write_job(scratch.clone(), write_ctr.clone()).await;

        let to_join = (0..NUM_ITERS)
            .map(|_| (
                tokio::spawn(write_job(scratch.clone(), write_ctr.clone())),
                tokio::spawn(read_job(
                    scratch.clone(), write_ctr.clone(), read_ctr.clone()))
            ))
            .collect::<Vec<_>>();

        for (a, b) in to_join { a.await.unwrap(); b.await.unwrap(); }

        assert_eq!(write_ctr.load(Ordering::SeqCst), NUM_ITERS + 1);
        assert_eq!(read_ctr.load(Ordering::SeqCst),  NUM_ITERS);
    }


    #[tokio::test]
    /// Test writing many files.
    async fn test_write_many_files()
    {
        let tmp = temp_dir();
        let scratch = Scratch::<DefaultLock>::new(&tmp, "test");

        const NUM_ITERS: u32 = 1000;

        for n in 1..NUM_ITERS
        {
            write_hello_world(&scratch, &[&n.to_string()]).await;
        }

        for n in 1..NUM_ITERS
        {
            assert_hello_world(&scratch, &[&n.to_string()]).await;
        }
    }


    /// Test writing a simple file.
    #[tokio::test]
    async fn test_write_file()
    {
        let tmp = temp_dir();
        let scratch = Scratch::<DefaultLock>::new(&tmp, "test");

        write_hello_world(&scratch, &["file"]).await;
        assert_hello_world(&scratch, &["file"]).await;
    }

    /// Test writing a simple file using `open_write_if_older()`.
    #[tokio::test]
    async fn test_write_file_if_older()
    {
        let tmp = temp_dir();
        let scratch = Scratch::<DefaultLock>::new(&tmp, "test");

        {
            let epoch = SystemTime::UNIX_EPOCH;
            let mut f = scratch.open_write_if_older(&["file"], epoch).await
                .unwrap().unwrap();
            f.write_all(b"Hello world!").await.unwrap();
        }

        assert_hello_world(&scratch, &["file"]).await;
    }

    /// Test writing to a deep directory.
    #[tokio::test]
    async fn test_write_deep_directory()
    {
        let tmp = temp_dir();
        let scratch = Scratch::<DefaultLock>::new(&tmp, "test");

        write_hello_world(&scratch,  &["a", "b", "c"]).await;
        assert_hello_world(&scratch, &["a", "b", "c"]).await;
    }

    /// Test that `open_write_if_older` respects the age of a file.
    #[tokio::test]
    async fn test_open_write_if_older_age()
    {
        let tmp = temp_dir();
        let scratch = Scratch::<DefaultLock>::new(&tmp, "test");

        async fn opens_file(scratch: &Scratch, age: SystemTime) -> bool
        {
            scratch.open_write_if_older(&["file"], age).await
                    .unwrap().is_some()
        }

        fn secs(n: u64) -> Duration { Duration::from_secs(n) }

        let now = SystemTime::now();

        // Check that we can open a file if it does not exist, regardless of the
        // age.
        assert!(opens_file(&scratch, SystemTime::UNIX_EPOCH).await);
        // Check that once a file exists, we can't open it again.
        assert!(!opens_file(&scratch, SystemTime::UNIX_EPOCH).await);

        // Check that we can't open a file that is newer than the specified time.
        assert!(!opens_file(&scratch, now - secs(1)).await);

        // Check that we can open a file that is older than a time in the future.
        assert!(opens_file(&scratch, SystemTime::now() + secs(1)).await);
        // Check that we can open a file that is older than the present.
        assert!(opens_file(&scratch, SystemTime::now()).await);
    }

    #[tokio::test]
    async fn test_open_modify()
    {
        let tmp = temp_dir();
        let scratch = Scratch::<DefaultLock>::new(&tmp, "test");

        async fn increment(scratch: &Scratch) -> u32
        {
            let mut file = scratch.open_modify(&["key"]).await.unwrap();

            let mut s = String::new();
            file.read_to_string(&mut s).await.unwrap();

            info!("Read {:?}", s);

            let rtn: u32 = s.parse().unwrap_or(0);

            file.rewind().await.unwrap();
            file.write((rtn + 1).to_string().as_bytes()).await.unwrap();

            rtn
        }

        assert_eq!(increment(&scratch).await, 0);
        assert_eq!(increment(&scratch).await, 1);
        assert_eq!(increment(&scratch).await, 2);
        assert_eq!(increment(&scratch).await, 3);
    }
}
