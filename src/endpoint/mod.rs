use super::*;

mod node;
pub(crate) use node::Node;

mod slash_disposition;
pub use slash_disposition::SlashDisposition;

mod cache_disposition;
pub use cache_disposition::CacheDisposition;

pub(crate) struct Endpoint
{
    name:         String,
    _scratch:     Arc<scratch::Scratch>,
    service:      Box<dyn service::Service>,
    cache_config: config::CacheConfig,
}

impl Endpoint
{
    fn create(
        block:       &confiner::Block,
        loader:      &confiner::Loader,
        server_conf: &config::ServerConfig
    )
        -> StdResult<Self>
    {
        if loader.children(block)?.len() > 0
        {
            return Err(From::from("Services should not have children."));
        }

        let Some(name) = block.name.clone()
            else { return Err(From::from("Services must be named.")) };

        let scratch = Arc::new(scratch::Scratch::new(
            &server_conf.scratch_directory, &name));

        let params = service::Params { scratch: scratch.clone() };
        let service = <service::ServiceComponent as component::Kind>::factory()
            .create(block, &params)?;

        let cache_config = server_conf.cache.clone();

        Ok(Self { name, _scratch: scratch, service, cache_config })
    }

    async fn handle(&self, request: &mut http::Request<'_>, uri: &http::Uri)
        -> http::Result<http::Response>
    {
        debug!("Endpoint {:?} handling request", self.name);

        let allowed = self.service.allowed_methods();
        if !allowed.contains(request.method())
        {
            return Err(http::Error::MethodNotAllowed(allowed));
        }

        // Get the slash disposition and handle the request, redirecting if
        // necessary.
        self.service.slash_disposition(uri).await
            .handle_request(request)?;

        // Get the cache disposition of the response
        let cache_dispos = self.service.cache_disposition(uri).await
            .apply_config(&self.cache_config);

        // Handle all headers
        cache_dispos.handle_headers(request.headers())?;

        // Handle the request
        let mut response = self.service.handle(request, uri).await?;

        // Consume the body if the handler did not
        request.consume_unread_body().await?;

        // Add any caching headers
        cache_dispos.add_headers(&mut response)?;

        Ok(response)
    }
}
