use super::*;

/// Behaviour for handling trailing slashes
#[derive(Debug)]
pub enum SlashDisposition
{
    /// Requests must have a trailing slash
    AlwaysTrail,
    /// Requests must not have a trailing slash
    NeverTrail,
    /// Requests are allowed to have a slash or not
    DontCare
}

/// Create an HTTP redirect overriding the trailing slash.
fn slash_redirect(request: &http::Request, slash: bool) -> http::Error
{
    let mut new_uri = request.full_uri_and_params().clone();
    new_uri.trailing_slash = slash;

    http::Error::Moved(format!("/{}", new_uri))
}

impl SlashDisposition
{
    /// Handle the slashes for a request
    pub fn handle_request(&self, request: &http::Request<'_>)
        -> http::Result
    {
        let trail  = request.full_uri_and_params().trailing_slash;

        // Never redirect an empty '/' URI - it is both trailing and non-trailing
        if request.full_uri().is_empty() { return Ok(()) }

        match (self, trail)
        {
            (SlashDisposition::AlwaysTrail, false) =>
                Err(slash_redirect(request, true)),
            (SlashDisposition::NeverTrail,  true) =>
                Err(slash_redirect(request, false)),
            (_, _) =>
                Ok(())
        }
    }
}
