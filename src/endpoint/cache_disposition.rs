use super::*;

use http::header::{
    CacheControl,
    CacheControlDirective::{self, *},
};

const fn seconds(n: u64) -> Duration { Duration::from_secs(n) }
const fn minutes(n: u64) -> Duration { seconds(60 * n) }
const fn hours(n: u64)   -> Duration { minutes(60 * n) }
const fn days(n: u64)    -> Duration { hours(24 * n) }

/// A description of the desired caching behaviour for a particular resource.
#[derive(Clone, Debug)]
#[allow(dead_code)]
pub enum CacheDisposition
{
    /// Don't cache this.
    NoCache,
    /// This was last modified at a particular time.
    ///
    /// Revalidate the modification time regularly.
    ///
    Modified(DateTime<Utc>),
    /// This content has a particular hash.
    ///
    /// Revalidate the hash regularly.
    ///
    Hash(String),
    /// The content is immutable.
    ///
    /// It should not change, and does not need to be regularly revalidated or
    /// updated.
    ///
    Immutable,
    /// Expire cached copies after an exact duration.
    Expires(Duration)
}

/// Get a Cache-Control header disallowing storage.
fn no_store_header() -> CacheControl<'static>
{
    const DIRECTIVES: &[CacheControlDirective] = &[NoStore];
    CacheControl::from(DIRECTIVES)
}

/// Get a Cache-Control header for an immutable resource.
fn immutable_header() -> CacheControl<'static>
{
    const DIRECTIVES: &[CacheControlDirective] = &[
        MaxAge(days(1)),
        Immutable
    ];
    CacheControl::from(DIRECTIVES)
}

/// Get a Cache-Control header to revalidate the response regularly.
fn revalidate_header() -> CacheControl<'static>
{
    const DIRECTIVES: &[CacheControlDirective] = &[
        MaxAge(minutes(1)),
        StaleWhileRevalidate(minutes(1))
    ];
    CacheControl::from(DIRECTIVES)
}

/// Get a Cache-Control header that expires after a particular period.
fn expires_header(expires: &Duration) -> CacheControl<'static>
{
    CacheControl::from(vec![MaxAge(*expires), MustRevalidate])
}

impl CacheDisposition
{
    /// Return an error response telling the client to use its cached copy.
    fn use_cached(&self) -> http::Result
    {
        Err(http::Error::NotModified(self.clone()))
    }

    /// Check whether a client can use a cached copy.
    ///
    /// This checks this cache disposition against the client's request headers.
    ///
    fn can_use_cached(&self, map: &http::header::Map) -> bool
    {
        match self
        {
            Self::Hash(curr) =>
            {
                match map.decode::<http::header::IfNoneMatch>()
                {
                    Some(header) => header.matches(curr),
                    None         => false
                }
            }
            Self::Modified(curr) =>
            {
                match map.decode::<http::header::IfModifiedSince>()
                {
                    Some(header) => curr.timestamp() <= header.0.timestamp(),
                    None         => false
                }
            }
            _ => { false }
        }
    }

    /// Examine client headers, and return an 403 Not Modified if possible.
    pub fn handle_headers(&self, map: &http::header::Map) -> http::Result
    {
        if self.can_use_cached(map)
        {
            self.use_cached()
        }
        else
        {
            Ok(())
        }
    }

    /// Add headers describing this cache disposition to an HTTP response.
    pub fn add_headers(&self, response: &mut http::Response) -> http::Result
    {
        match self
        {
            CacheDisposition::NoCache =>
            {
                response.headers()
                    .add_default(&no_store_header())?;
            }
            CacheDisposition::Modified(date) =>
            {
                response.headers()
                    .add_default(&revalidate_header())?
                    .add_default(&http::header::LastModified(*date))?;
            }
            CacheDisposition::Hash(tag) =>
            {
                response.headers()
                    .add_default(&revalidate_header())?
                    .add_default(&http::header::ETag::new_strong(tag))?;
            }
            CacheDisposition::Immutable =>
            {
                response.headers()
                    .add_default(&immutable_header())?;
            }
            CacheDisposition::Expires(expiry) =>
            {
                response.headers()
                    .add_default(&expires_header(expiry))?;
            }
        }

        Ok(())
    }

    pub fn apply_config(mut self, config: &config::CacheConfig) -> Self
    {
        match config
        {
            config::CacheConfig::Normal  => self,
            config::CacheConfig::NoCache => Self::NoCache,
            config::CacheConfig::PurgeBefore(before) =>
            {
                match &mut self
                {
                    CacheDisposition::Modified(ref mut date)
                        if *date < *before =>
                    {
                        *date = *before;
                    }
                    CacheDisposition::Hash(tag) =>
                    {
                        tag.push_str(&before.to_rfc3339());
                    }
                    _ => {}
                }
                self
            }
        }
    }
}
