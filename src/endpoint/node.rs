use super::*;

pub(crate) enum Node
{
    List(Vec<Arc<Node>>),
    Pre(Box<dyn pre::Pre>, Arc<Node>),
    Endpoint(Endpoint)
}

impl Node
{
    pub fn create_list(
        block:       &confiner::Block,
        loader:      &confiner::Loader,
        server_conf: &config::ServerConfig,
        by_name:     &mut HashMap<String, Arc<Node>>
    )
        -> StdResult<Self>
    {
        Ok(Self::List(
            loader.children(&block)?
                .into_iter()
                .map(|child| Self::create(&child, loader, server_conf, by_name))
                .collect::<Result<Vec<_>, _>>()?
        ))
    }

    fn create(
        block:       &confiner::Block,
        loader:      &confiner::Loader,
        server_conf: &config::ServerConfig,
        by_name:     &mut HashMap<String, Arc<Node>>
    )
        -> StdResult<Arc<Self>>
    {
        // Does this block already exist?
        if let Some(extant) = block.name.as_ref()
            .and_then(|key| by_name.get(key))
        {
            Ok(extant.clone())
        }
        // Create a new node...
        else
        {
            let node: Self;

            // Can we build this block as a pre-block?
            if <pre::PreComponent as component::Kind>::factory()
                .can_build(block.kind.as_str())
            {
                let pre = <pre::PreComponent as component::Kind>::factory()
                    .create(block, &pre::Params)
                    .annotate_with(
                        || format!("Creating pre {:?}", block.name)
                    )?;


                let list_node = Self::create_list(
                        block, loader, server_conf, by_name
                    )?;

                node = Self::Pre(pre, Arc::new(list_node));
            }
            // Can we build this block as a service-block?
            else if <service::ServiceComponent as component::Kind>::factory()
                .can_build(block.kind.as_str())
            {
                let endpoint = Endpoint::create(block, loader, server_conf)
                    .annotate_with(
                        || format!("Creating endpoint {:?}", block.name)
                    )?;
                node = Self::Endpoint(endpoint);
            }
            // No clue what this is!
            else
            {
                return Err(From::from(
                    format!("Unknown block - {:?}", &block.kind)
                ));
            }

            let arc_node = Arc::new(node);

            // Store the new node.
            if let Some(name) = block.name.clone()
            {
                by_name.insert(name, arc_node.clone());
            }

            Ok(arc_node)
        }
    }

    #[async_recursion]
    pub async fn handle(&self, request: &mut http::Request<'_>, uri: &http::Uri)
        -> Option<http::Result<http::Response>>
    {
        match self
        {
            Self::List(children) =>
            {
                for child in children
                {
                    match Box::pin(child.handle(request, uri)).await
                    {
                        Some(rsp) => return Some(rsp),
                        None      => {}
                    }
                }

                None
            }
            Self::Pre(pre, child) =>
            {
                match pre.handle(request, uri).await
                {
                    Some(Ok(child_uri)) =>
                    {
                        Box::pin(child.handle(request, child_uri)).await
                    }
                    Some(Err(err)) => Some(Err(err)),
                    None           => None
                }
            }
            Self::Endpoint(ep) =>
            {
                Some(ep.handle(request, uri).await)
            }
        }
    }
}

