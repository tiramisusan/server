use super::*;

pub struct Setup<'a>
{
    request: &'a http::Request<'a>,
    uri:     &'a http::Uri,
    conf:    &'a Config,
    cmd:     Command
}

impl<'a> Setup<'a>
{
    pub fn new(
        conf:    &'a Config,
        request: &'a http::Request<'a>,
        uri:     &'a http::Uri
    )
        -> Self
    {
        let cmd = Command::new(&conf.executable);
        Self { request, uri, conf, cmd }
    }

    pub fn setup(mut self) -> http::Result<Command>
    {
        self.cmd.env_clear();

        self.add_general_env()?;
        self.add_http_env();
        self.add_http_args();

        self.set_current_dir();

        Ok(self.cmd)
    }

    fn set_current_dir(&mut self)
    {
        self.cmd.current_dir(&self.conf.directory);
    }

    fn add_http_args(&mut self)
    {
// TODO
    }

    fn add_http_env(&mut self)
    {
// TODO
    }

    fn add_general_env(&mut self) -> http::Result
    {
        let port   = self.request.port().to_string();
        let ip     = self.request.ip().to_string();
        let uri    = self.uri;
        let method = self.request.method().to_string();
        let name   = self.conf.executable.file_name().unwrap_or(".".as_ref());
        let host   = hostname::get()?;
        let path   = self.request.full_uri_and_params().to_string();

        self.cmd.env("SERVER_SOFTWARE",   "MyServer/Version");
        self.cmd.env("SERVER_NAME",       &host);
        self.cmd.env("GATEWAY_INTERFACE", "CGI/1.1");
        self.cmd.env("SERVER_PROTOCOL",   "HTTP/1.1");
        self.cmd.env("SERVER_PORT",       port);
        self.cmd.env("REQUEST_METHOD",    method);
        self.cmd.env("PATH_INFO",         path);
        self.cmd.env("PATH_TRANSLATED",   uri);
        self.cmd.env("SCRIPT_NAME",       name);
// TODO self.cmd.env       ("QUERY_STRING"   ),
        self.cmd.env("REMOTE_HOST",       ip);
// TODO self.cmd.env("CONTENT_TYPE", )
// TODO self.cmd.env("CONTENT_LENGTH", )
//
        Ok(())
    }
}
