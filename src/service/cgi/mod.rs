use super::*;

use tokio::process::{Command};
use std::{
    path::{Path, PathBuf},
    process::Output,
    fmt
};

mod setup;
use setup::Setup;

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
struct Config
{
    directory:     PathBuf,
    build_command: String,
    executable:    PathBuf,
    #[serde(default)]
    body_only:     bool
}

struct Cgi { conf: Config }

fn cmd_err(cmd: &str) -> io::Error
{
    io::Error::new(
        io::ErrorKind::Other,
        format!("Failed run command {:?}", cmd).as_str()
    )
}

const SHELL: &'static str = "/bin/sh";

fn run_command(cmd: &str, dir: impl AsRef<Path>) -> io::Result<()>
{
    info!("Running command {:?} in dir {:?}", cmd, dir.as_ref());

    let mut child = std::process::Command::new(SHELL)
        .arg("-c").arg(cmd)
        .current_dir(dir)
        .spawn()?;

    let exit = child.wait()?;

    if exit.success()
    {
        Ok(())
    }
    else
    {
        Err(cmd_err(cmd))
    }
}

struct CgiResult
{
    out: Output
}

impl CgiResult
{
    async fn from_command(mut cmd: Command) -> http::Result<Self>
    {
        let out = cmd.output().await?;
        let err = String::from_utf8_lossy(&out.stderr);

        if out.status.success()
        {
            if err.len() > 0
            {
                error!("CGI Command stderr:\n{}", &err);
            }

            Ok(Self { out })
        }
        else
        {
            Err(http::Error::from(
                format!("CGI command was not successful: {}", &err)
            ))
        }
    }

    fn page(&self) -> html::RawPage
    {
        html::RawPage::new("CGI", self.to_string())
    }

    fn body_only_response(&self) -> http::Result<http::Response>
    {
        http::Response::new(self.page())
    }

    fn normal_response(&self) -> http::Result<http::Response>
    {
        use http::header::*;

        http::Response::new(self.to_string())?
            .with_header(&ContentType::from(MediaType::html()))
    }

}

impl fmt::Display for CgiResult
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        f.write_str(&String::from_utf8_lossy(&self.out.stdout))
    }
}

impl component::Component<ServiceComponent> for Cgi
{
    type Config = Config;

    fn name() -> &'static str { "cgi" }

    fn create(conf: Self::Config, _: &Params) -> StdResult<Self>
    {
        run_command(&conf.build_command, &conf.directory)?;

        Ok(Self { conf })
    }
}

component::register!(ServiceComponent, Cgi);

#[async_trait]
impl Service for Cgi
{
    async fn slash_disposition(&self, uri: &http::Uri) -> SlashDisposition
    {
        if uri.is_empty()
        {
            SlashDisposition::AlwaysTrail
        }
        else
        {
            SlashDisposition::NeverTrail
        }
    }

    async fn handle(&self, request: &mut http::Request<'_>, uri: &http::Uri)
        -> http::Result<http::Response>
    {
        let cmd    = Setup::new(&self.conf, request, uri).setup()?;
        let result = CgiResult::from_command(cmd).await?;

        if self.conf.body_only
        {
            result.body_only_response()
        }
        else
        {
            result.normal_response()
        }
    }
}
