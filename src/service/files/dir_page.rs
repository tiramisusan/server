use super::*;

use tokio::fs::{DirEntry, read_dir};
use std::{
    fs::Metadata,
    path::Path,
    ffi::OsStr
};

fn default_datetime_format() -> String { "%F %T %Z".to_owned() }

#[derive(serde::Deserialize)]
pub struct DirPageConfig
{
    /// The datetime format to use.
    ///
    /// See `chrono::strftime` for details on formatting.
    #[serde(default="default_datetime_format")]
    datetime_format: String,

    /// The template to use.
    #[serde(default)]
    template: String
}

config::impl_default_from_deser!(DirPageConfig);

impl DirPageConfig
{
    pub fn use_default_template(&mut self, template: &str)
    {
        if self.template.len() == 0
        {
            self.template = template.to_owned();
        }
    }
}

#[derive(serde::Serialize)]
pub(super) struct Row
{
    name:     String,
    size:     String,
    is_img:   bool,
    is_dir:   bool,
    modified: String,
    created:  String
}

#[derive(serde::Serialize)]
pub(super) struct List
{
    name:    String,
    is_root: bool,
    rows:    Vec<Row>
}

struct Formatter<'a>
{
    conf: &'a DirPageConfig,
    meta: &'a Metadata,
    name: &'a OsStr
}

impl<'a> Formatter<'a>
{
    fn format_bytes(&self, mut num: f64) -> String
    {
        const UNIT_NAMES: &[&str] = &["B", "kiB", "MiB", "GiB", "TiB"];

        let mut unit_ind = 0;
        while unit_ind < UNIT_NAMES.len() && num > 1024.0
        {
            num  /= 1024.0;
            unit_ind += 1;
        }

        format!("{:.1} {}", num, UNIT_NAMES[unit_ind])
    }

    fn format_date(&self, time: SystemTime) -> String
    {
        DateTime::<Local>::from(time)
            .format(&self.conf.datetime_format)
            .to_string()
    }

    fn size(&self) -> String { self.format_bytes(self.meta.len() as f64) }

    fn name(&self) -> String
    {
        let mut name = self.name.to_string_lossy().into_owned();

        // Add a trailing slash for directories
        if self.meta.is_dir() { name.push('/'); }

        name
    }

    fn modified(&self) -> String
    {
        self.meta.modified()
            .map(|d| self.format_date(d))
            .unwrap_or_else(|_| "Unknown".to_owned())
    }

    fn created(&self) -> String
    {
        self.meta.created()
            .map(|d| self.format_date(d))
            .unwrap_or_else(|_| "Unknown".to_owned())
    }

    fn create_row(&self) -> Row
    {
        Row
        {
            name:     self.name(),
            size:     self.size(),
            modified: self.modified(),
            created:  self.created(),
            is_dir:   self.meta.is_dir(),
            is_img:   self.meta.is_file() &&
                http::header::MediaType::guess_from_path(self.name)
                    .is_image()
        }
    }
}

impl Row
{
    async fn from_dir_entry(entry: DirEntry, conf: &DirPageConfig)
        -> http::Result<Self>
    {
        let name = entry.file_name();
        let meta = entry.metadata().await?;

        Ok((Formatter { conf, name: &name, meta: &meta }).create_row())
    }
}

impl List
{
    async fn new(uri: &http::Uri, dir: &Path, conf: &DirPageConfig)
        -> http::Result<Self>
    {
        let mut list = read_dir(dir).await
            .map_err(|_| http::Error::new_not_found(dir))?;

        let mut rows = Vec::new();
        let     name = String::from(
            dir
                .file_name()
                .unwrap_or(".".as_ref())
                .to_string_lossy()
        );

        while let Some(entry) = list.next_entry().await?
        {
            rows.push(Row::from_dir_entry(entry, conf).await?);
        }

        rows.sort_by(|a, b| <String as Ord>::cmp(&a.name, &b.name));

        Ok(Self { name, is_root: uri.is_empty(), rows })
    }
}

pub(super) async fn make_dir_page(
    uri: &http::Uri,
    dir: &Path,
    conf: &DirPageConfig
)
    -> http::Result<impl http::Render>
{
    let list = List::new(uri, dir, conf).await?;
    Ok(html::TemplatePage::new(&list.name.clone(), &conf.template, list))
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn test_dir_page_config_default()
    {
        // Check that default doesn't panic!
        let _ = DirPageConfig::default();
    }
}
