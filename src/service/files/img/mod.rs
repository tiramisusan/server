use super::*;

use tokio::fs;
use image::{
    DynamicImage,
    imageops::FilterType
};

mod transform;
use transform::Transform;

mod loader;
use loader::Loader;

mod orientation;
use orientation::Orientation;

pub struct Img
{
    config:  ImgConfig,
    scratch: Arc<scratch::Scratch>
}

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ImgConfig
{
    #[serde(default="transform::default_transforms")]
    transform: HashMap<String, Transform>,
    #[serde(default)]
    ignore_orientation: bool
}

impl Img
{
    /// Find a `Transform` by name.
    ///
    /// If no such `Transform` exists, return an error.
    ///
    fn find_transform(&self, xform_name: &str) -> http::Result<&Transform>
    {
        self.config.transform.get(xform_name)
            .ok_or_else(|| http::Error::from(
                format!("Unknown transform '{}'", xform_name)
            ))
    }

    /// Load a file from a path, and apply a `Transform`.
    async fn transform_path(&self, input_path: &Path, xform_name: &str)
        -> http::Result<Vec<u8>>
    {
        info!("Applying {} transform to {:?}.", xform_name, input_path);

        let input_bytes = fs::read(input_path).await?;
        let format      = image::guess_format(&input_bytes)?;

        let output = self.find_transform(xform_name)?
            .load_and_apply(input_bytes, &self.config)?;

        let mut output_bytes = std::io::Cursor::new(Vec::<u8>::new());
        output.write_to(&mut output_bytes, format)?;

        Ok(output_bytes.into_inner())
    }

    /// Get a transformed version of an image from a path.
    ///
    /// If a cached copy of the transform exists, that is used, and otherwise
    /// the transform is performaed again.
    ///
    async fn transform_path_cached(
        &self,
        uri:        &http::Uri,
        input:      &Path,
        xform_name: &str,
    )
        -> http::Result<Vec<u8>>
    {
        // Get an identifier for the cached copy of the transform.
        let key: [&str; 2] = [ xform_name, uri.as_ref() ];

        let ts = fs::metadata(input).await?.modified()?;

        // If the cached copy of the transformed file is older than the
        // original, or doesn't exist, open it for writing.
        if let Some(mut wr_file) =
            self.scratch.open_write_if_older(&key, ts).await?
        {
            info!("No cached transformed copy of {:?} found.", input);
            let bytes = self.transform_path(input, xform_name).await?;
            wr_file.write_all(&bytes).await?;

            Ok(bytes)
        }
        // If the cached copy is up-to-date, open it for reading.
        else
        {
            info!("Found cached transformed copy of {:?}.", input);
            let mut rd_file = self.scratch.open_read(&key).await?;

            let mut bytes = Vec::<u8>::new();
            rd_file.read_to_end(&mut bytes).await?;

            Ok(bytes)
        }
    }
}

#[async_trait]
impl FilesType for Img
{
    type Config = ImgConfig;

    fn name() -> &'static str { "img_files" }

    fn default_dir_template() -> &'static str { "img_dir_page.html" }

    fn create(config: ImgConfig, scratch: Arc<scratch::Scratch>) -> Self
    {
        Self { config, scratch }
    }

    async fn handle_file(
        &self,
        request:  &http::Request<'_>,
        uri:      &http::Uri,
        path:     &Path
    )
        -> http::Result<http::Response>
    {
        if let Some(xform_name) = request.uri_param("xform")
        {
            let xformed = self.transform_path_cached(uri, path, xform_name)
                .await?;
            http::Response::new(xformed)
        }
        else
        {
            http::Response::new(open_file(path).await?)
        }
    }
}

component::register!(ServiceComponent, Files<Img>);
