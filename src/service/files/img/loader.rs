use super::*;

use image::codecs::jpeg::JpegDecoder;
use std::io::Cursor;

pub(super) struct Loader<'a>
{
    bytes:       &'a [u8],
    config:      &'a ImgConfig,
    format:      image::ImageFormat,
    max_size:    Option<(u32, u32)>
}

impl<'a> Loader<'a>
{
    pub fn new(bytes: &'a [u8], config: &'a ImgConfig) -> http::Result<Self>
    {
        let format = image::guess_format(bytes)?;
        Ok(Self { bytes, config, format, max_size: None })
    }

    pub fn with_max_size(mut self, max_size: Option<(u32, u32)>) -> Self
    {
        self.max_size = max_size;
        self
    }

    fn get_exif(&self) -> Option<exif::Exif>
    {
        if self.format == image::ImageFormat::Jpeg
        {
            exif::Reader::new()
                .read_from_container(&mut Cursor::new(self.bytes))
                .ok()
        }
        else
        {
            None
        }
    }

    fn get_orientation(&self) -> Option<Orientation>
    {
        let exif_tag = self.get_exif()?
            .get_field(exif::Tag::Orientation, exif::In::PRIMARY)?
            .value
            .get_uint(0)?;

        Orientation::from_exif_tag(exif_tag)
    }

    pub fn load(&self) -> http::Result<DynamicImage>
    {
        let mut img = match self.max_size
            {
                // If the image is in JPEG format, and we have specific maximum
                // dimensions, use the JPEG-specific decoder with downsampling
                // as we load the image, for faster load times.
                Some((w, h)) if self.format == image::ImageFormat::Jpeg =>
                {
                    let mut decod = JpegDecoder::new(Cursor::new(self.bytes))?;
                    decod.scale(w as u16, h as u16)?;
                    DynamicImage::from_decoder(decod)?
                }
                _ =>
                {
                    image::load_from_memory(&self.bytes)?
                }
            };

        if !self.config.ignore_orientation
        {
            if let Some(orient) = self.get_orientation()
            {
                img = orient.apply(img)
            }
        }

        Ok(img)
    }
}
