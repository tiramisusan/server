use super::*;

macro_rules! xform
{
    ($name:expr, $($fields:tt)*) =>
    {(
        $name.to_owned(), Transform { $($fields)*, ..Default::default() }
    )}
}

pub(super) fn default_transforms() -> HashMap<String, Transform>
{
    HashMap::from([
        xform!("small", width: Some(350)),
        xform!("thumbnail", height: Some(64), width: Some(64), pad: true)
    ])
}

#[derive(serde::Deserialize, Default)]
pub(super) struct Transform
{
    #[serde(default)]
    width: Option<u32>,
    #[serde(default)]
    height: Option<u32>,
    #[serde(default)]
    pad: bool
}

fn pad_image(img: DynamicImage, w: u32, h: u32) -> DynamicImage
{
    use image::GenericImage;

    // If the image is already the expected dimensions, do nothing
    if img.width() == w && img.height() == h
    {
        return img;
    }

    // Create a background of the desired dimensions
    let bg_colour = image::Rgba([255, 255, 255, 0]);
    let mut bg = image::RgbaImage::from_pixel(w, h, bg_colour);

    let x_off = (w - img.width()) / 2;
    let y_off = (h - img.height()) / 2;

    // Try and copy the original image on the new background
    if bg.copy_from(&img, x_off, y_off).is_ok()
    {
        DynamicImage::from(bg)
    }
    else
    {
        // Fallback to just returning the original image
        img
    }
}

impl Transform
{
    /// Apply this transform to a loaded image.
    fn apply(&self, mut img: DynamicImage) -> DynamicImage
    {
        if let Some((w, h)) = self.max_dimensions()
        {
            img = img.resize(w, h, FilterType::Triangle);

            if self.pad { img = pad_image(img, w, h); }
        }

        img
    }

    /// Get the maximum possible dimensions for this image.
    ///
    /// If there are no dimension constriants for the image, return None.
    ///
    fn max_dimensions(&self) -> Option<(u32, u32)>
    {
        if self.width.is_some() || self.height.is_some()
        {
            Some((
                self.width.unwrap_or(u32::MAX),
                self.height.unwrap_or(u32::MAX)
            ))
        }
        else
        {
            None
        }
    }

    /// Load an image from bytes, and apply this transformation.
    pub fn load_and_apply(&self, input: Vec<u8>, config: &ImgConfig)
        -> http::Result<DynamicImage>
    {
        let img = Loader::new(&input, config)?
            .with_max_size(self.max_dimensions())
            .load()?;

        // Apply the transformation
        Ok(self.apply(img))
    }
}

