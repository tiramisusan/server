use super::*;

pub(super) enum Orientation
{
    Rotate90,
    Rotate180,
    Rotate270,
    VFlip,
    Rotate90VFlip,
    Rotate180VFlip,
    Rotate270VFlip
}

impl Orientation
{
    pub fn apply(&self, img: DynamicImage)-> DynamicImage
    {
        use Orientation::*;
        match self
        {
            Rotate90       => img.rotate90(),
            Rotate180      => img.rotate180(),
            Rotate270      => img.rotate270(),
            VFlip          => img.fliph(),
            Rotate90VFlip  => img.rotate90().fliph(),
            Rotate180VFlip => img.rotate180().fliph(),
            Rotate270VFlip => img.rotate270().fliph()
        }
    }

    pub fn from_exif_tag(s: u32) -> Option<Self>
    {
        use Orientation::*;
        match s
        {
            6 => Some(Rotate90),
            3 => Some(Rotate180),
            8 => Some(Rotate270),
            2 => Some(VFlip),
            5 => Some(Rotate90VFlip),
            4 => Some(Rotate180VFlip),
            7 => Some(Rotate270VFlip),
            _   => None
        }
    }
}
