use super::*;

use matthewdown::*;

pub struct Command<F>
    where F: Fn(&Path) -> Option<String>
{
    root_dir: PathBuf,
    cb:       F
}

struct Entry
{
    href:    String,
    display: String
}

struct Block
{
    entries: Vec<Entry>
}

impl<F> Command<F>
    where F: Fn(&Path) -> Option<String>
{
    pub fn new(root_dir: impl AsRef<Path>, cb: F) -> Self
    {
        Self { root_dir: root_dir.as_ref().to_owned(), cb }
    }

    fn get_entries(&self, args: &PosArgs) -> MdResult<Vec<Entry>>
    {
        let mut rtn = Vec::new();
        let fs_dir = self.root_dir.join(&args.0);

        for entry in std::fs::read_dir(fs_dir)?
        {
            let file_path = entry?.path();
            let href      = file_path.strip_prefix(&self.root_dir)
                .unwrap_or(&file_path)
                .to_string_lossy()
                .into_owned();

            if let Some(display) = (self.cb)(&file_path)
            {
                rtn.push(Entry { display, href })
            }
        }

        Ok(rtn)
    }
}

impl walker::Block for Block
{
    fn visit_post(
        &self,
        visitor:  &mut dyn Visitor,
        _handler: &mut walker::BlockHandler
    )
        -> MdResult
    {
        use visitor::Region::*;
        visitor.begin(&UnorderedList)?;
        for entry in self.entries.iter()
        {
            let link = Link(entry.href.to_owned());

            visitor.begin(&UnorderedItem)?;
            visitor.begin(&link)?;
            visitor.text(&entry.display)?;
            visitor.end(&link)?;
            visitor.end(&UnorderedItem)?;
        }
        visitor.end(&UnorderedList)?;

        Ok(())
    }
}

#[derive(serde::Deserialize)]
pub struct PosArgs
(
    #[serde(default)]
    PathBuf
);

impl<F> command::Command<command::Block> for Command<F>
    where F: Fn(&Path) -> Option<String>
{
    type Positional = PosArgs;
    type Named      = command::NoNamed;

    fn create<'a>(&self, args: Self::Positional, _: Self::Named)
        -> MdResult<Box<dyn walker::Block + 'a>>
    {
        Ok(Box::new(Block { entries: self.get_entries(&args)? }))
    }

    fn name(&self) -> &'static str { "file-list" }
}

