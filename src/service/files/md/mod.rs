use super::*;

use std::borrow::Cow;

mod file_list;

const INDEX_FILE_NAME: &str = "index.md";

fn use_index_files_default() -> bool { true }

#[derive(serde::Deserialize)]
pub struct Config
{
    #[serde(default)]
    matthewdown: config::MdConfig,
    #[serde(default="use_index_files_default")]
    use_index_files: bool
}

pub struct Md
{
    config: Config
}

fn title<'a>(path: &'a Path) -> Cow<'a, str>
{
    if let Some(stem) = path.file_stem()
    {
        stem.to_string_lossy()
    }
    else
    {
        Cow::from("Markdown")
    }
}

fn get_file_list_entry(path: &Path, listed: &Path) -> Option<String>
{
    // Do not display the file containing the listing
    if path.canonicalize().ok() == listed.canonicalize().ok() { return None; }

    if listed.extension()? != "md" { return None; }

    if listed.file_name()? == "index.md" { return None; }

    Some(listed.file_stem()?.to_string_lossy().into_owned())
}

impl Md
{
    fn htmlify(&self, s: &str, path: &Path)
        -> http::Result<String>
    {
        use matthewdown::{Visitor, Parser, HtmlVisitor};

        let mut parser = Parser::default();

        // If there's a parent directory for this md file, add the file-list
        // command to the matthewdown parser.
        if let Some(parent) = path.parent()
        {
            let path_buf = path.to_owned();
            parser = parser
                .with_block_command(file_list::Command::new(
                    parent,
                    move |listed| get_file_list_entry(&path_buf, listed)
                ));
        }

        let rtn = HtmlVisitor::new()
            .with_config(self.config.matthewdown.md_html_config())
            .with_parsed_str(&parser, s)?
            .into_output();

        Ok(rtn)
    }
}

#[async_trait]
impl FilesType for Md
{
    type Config = Config;

    fn name() -> &'static str { "md_files" }

    fn create(config: Config, _: Arc<scratch::Scratch>) -> Self
    {
        Self { config }
    }

    fn index_file(&self, dir_path: &Path) -> Option<PathBuf>
    {
        match self.config.use_index_files
        {
            true  => Some(dir_path.join(INDEX_FILE_NAME)),
            false => None
        }
    }

    async fn handle_file(
        &self,
        _request: &http::Request<'_>,
        _uri:     &http::Uri,
        path:     &Path
    )
        -> http::Result<http::Response>
    {
        let md   = read_file(path).await?;
        let body = self.htmlify(&md, path)?;

        let page = html::RawPage::new(&title(path), body);

        http::Response::new(page)
    }
}

component::register!(ServiceComponent, Files<Md>);
