use super::*;

mod raw;
pub use raw::Raw;

#[cfg(feature="service_md_files")]
mod md;
#[cfg(feature="service_md_files")]
pub use md::Md;

#[cfg(feature="service_img_files")]
mod img;
#[cfg(feature="service_img_files")]
pub use img::Img;

mod dir_page;
use dir_page::{DirPageConfig, make_dir_page};

mod utils;
use utils::*;

use chrono::{DateTime, Utc, Local};

use tokio::fs::File;

/// The configuration for an instance of a `FilesType` service.
#[derive(serde::Deserialize)]
struct FilesConfig<T>
{
    /// Common settings for all `FilesType` services.
    #[serde(flatten)]
    outer: OuterConfig,

    /// Settings specific to the particular `FilesType`.
    #[serde(flatten)]
    inner: T
}

#[derive(serde::Deserialize)]
enum CacheMethod
{
    /// Use the file modification time.
    ModTime,
    /// Use an MD5 hash of the file contents.
    Md5
}

fn default_cache_method() -> CacheMethod { CacheMethod::ModTime }

/// Configuration common to all `FilesType` services.
#[derive(serde::Deserialize)]
struct OuterConfig
{
    /// The root path to serve.
    ///
    /// Files from within this directory can be served.
    path: PathBuf,

    /// Configuration for rendering directory listings.
    #[serde(default)]
    dir_page: DirPageConfig,

    /// The method to use for managing caching.
    #[serde(default="default_cache_method")]
    cache_method: CacheMethod
}

struct Files<T>
{
    config: OuterConfig,
    inner:  T
}

impl<T> Files<T>
{
    async fn uri_to_path(&self, uri: &http::Uri) -> http::Result<PathBuf>
    {
        let req_path = uri.fs_path(&self.config.path);

        if is_path_within(&req_path, &self.config.path).await?
        {
            Ok(req_path)
        }
        else
        {
            Err(http::Error::Forbidden)
        }
    }

    async fn get_uri_path_type(&self, uri: &http::Uri) -> http::Result<PathType>
    {
        get_path_type(self.uri_to_path(uri).await?).await
    }

    async fn get_uri_last_modified(&self, uri: &http::Uri)
        -> http::Result<DateTime<Utc>>
    {
        last_modified(self.uri_to_path(uri).await?).await
    }

    async fn get_uri_md5(&self, uri: &http::Uri)
        -> http::Result<String>
    {
        calculate_md5(self.uri_to_path(uri).await?).await
    }
}

impl<T> Files<T>
    where T: FilesType + Sync + Send
{
    async fn serve_file(
        &self,
        request: &http::Request<'_>,
        uri:     &http::Uri,
        path:    &Path
    )
        -> http::Result<http::Response>
    {
        let mut response = self.inner.handle_file(request, uri, path).await?;

        // Apply the ?dl parameter to all files.
        //
        // If we have a ?dl parameter, add an attachment Content-Disposition
        // header so that the file is downloaded, otherwise add an inline
        // Content-Disposition header.
        use http::header::*;
        if request.uri_param("dl").is_some()
        {
            response.headers()
                .add_default(&ContentDisposition::attachment())?;
        }
        else
        {
            response.headers()
                .add_default(&ContentDisposition::inline())?;
        }

        // Use a default MIME type based on the served path.
        response.headers().add_default(
            &ContentType::from(MediaType::guess_from_path(path))
        )?;

        Ok(response)
    }

    async fn serve_dir(
        &self,
        request: &http::Request<'_>,
        uri:     &http::Uri,
        path:    &Path
    )
        -> http::Result<http::Response>
    {
        if let Some(index_path) = self.inner.index_file(path)
        {
            let path_type = get_path_type(&index_path).await;
            if matches!(path_type, Ok(PathType::File))
            {
                // If an index file exists, serve it
                return self.serve_file(request, uri, &index_path).await;
            }
        }

        // Otherwise serve a directory listing
        let dir_list_page = make_dir_page(uri, path, &self.config.dir_page)
            .await?;
        http::Response::new(dir_list_page)
    }
}

impl<T> component::Component<ServiceComponent> for Files<T>
    where T: FilesType + Sync + Send + 'static
{
    type Config = FilesConfig<T::Config>;

    fn name() -> &'static str { T::name() }

    fn create(mut conf: Self::Config, params: &Params) -> StdResult<Self>
    {
        info!("Serving files from directory {:?}", conf.outer.path);

        // Update our config to use the dir-page requested by the inner impl.
        conf.outer.dir_page.use_default_template(T::default_dir_template());

        Ok(Self {
            config: conf.outer,
            inner:  T::create(conf.inner, params.scratch.clone())
        })
    }
}

#[async_trait]
impl<T> Service for Files<T>
    where T: FilesType + Sync + Send + 'static
{
    async fn slash_disposition(&self, uri: &http::Uri) -> SlashDisposition
    {
        match self.get_uri_path_type(uri).await
        {
            Ok(PathType::Directory) => SlashDisposition::AlwaysTrail,
            Ok(PathType::File)      => SlashDisposition::NeverTrail,
            Ok(PathType::SymLink)   => SlashDisposition::DontCare,
            Err(_)                  => SlashDisposition::DontCare
        }
    }

    async fn cache_disposition(&self, uri: &http::Uri) -> CacheDisposition
    {
        match self.config.cache_method
        {
            CacheMethod::ModTime =>
                match self.get_uri_last_modified(uri).await
                {
                    Ok(modtime) => CacheDisposition::Modified(modtime),
                    Err(_)      => CacheDisposition::NoCache
                },
            CacheMethod::Md5 =>
                match self.get_uri_md5(uri).await
                {
                    Ok(md5) => CacheDisposition::Hash(md5),
                    Err(_)  => CacheDisposition::NoCache
                }
        }
    }

    async fn handle(&self, request: &mut http::Request<'_>, uri: &http::Uri)
        -> http::Result<http::Response>
    {
        let path = self.uri_to_path(uri).await?;

        info!("Serving file {:?}", path);

        match get_path_type(&path).await?
        {
            PathType::Directory => self.serve_dir(request, uri, &path).await,
            PathType::File      => self.serve_file(request, uri, &path).await,
            PathType::SymLink =>
                Err(http::Error::from("Cannot serve symlinks."))
        }
    }
}

#[async_trait]
pub trait FilesType
{
    type Config: for<'de> serde::Deserialize<'de> + Sync + Send;

    fn name() -> &'static str;

    fn default_dir_template() -> &'static str { "dir_page.html" }

    /// Given a path for a directory, suggest an index file's path.
    ///
    /// If the suggested file exists, it will be served as an index file.
    fn index_file(&self, dir_path: &Path) -> Option<PathBuf>
    {
        let _ = dir_path;
        None
    }

    fn create(config: Self::Config, scratch: Arc<scratch::Scratch>) -> Self;

    async fn handle_file(
        &self,
        request: &http::Request<'_>,
        uri:     &http::Uri,
        path:    &Path
    )
        -> http::Result<http::Response>;
}

