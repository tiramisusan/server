use super::*;

pub struct Raw;

#[async_trait]
impl FilesType for Raw
{
    type Config = ();

    fn name() -> &'static str { "raw_files" }

    fn create(_: (), _: Arc<scratch::Scratch>) -> Self { Self }

    async fn handle_file(
        &self,
        _request: &http::Request<'_>,
        _uri:     &http::Uri,
        path:     &Path,
    )
        -> http::Result<http::Response>
    {
        http::Response::new(open_file(path).await?)
    }
}

component::register!(ServiceComponent, Files<Raw>);
