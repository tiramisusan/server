//! # File Service Utilities
//!
//! This file contains miscelaneous utilities for dealing with files.

use super::*;

use tokio::fs::{metadata, canonicalize};

pub async fn open_file(path: impl AsRef<Path>) -> http::Result<File>
{
    File::open(&path).await
        .map_err(|_| http::Error::new_not_found(path))
}

pub async fn read_file(path: impl AsRef<Path>) -> http::Result<String>
{
    let mut dst = String::new();
    open_file(path).await?
        .read_to_string(&mut dst).await?;

    Ok(dst)
}

pub enum PathType
{
    File,
    SymLink,
    Directory
}

pub async fn get_path_type(path: impl AsRef<Path>) -> http::Result<PathType>
{
    match metadata(path.as_ref()).await
    {
        Ok(meta) if meta.is_symlink() => Ok(PathType::SymLink),
        Ok(meta) if meta.is_dir()     => Ok(PathType::Directory),
        Ok(meta) if meta.is_file()    => Ok(PathType::File),
        Ok(_)  => Err(http::Error::from("Cannot determine type of path")),
        Err(_) => Err(http::Error::new_not_found(path))
    }
}

pub async fn last_modified(path: impl AsRef<Path>)
    -> http::Result<DateTime<Utc>>
{
    match metadata(path.as_ref()).await
        .and_then(|meta| meta.modified())
    {
        Ok(meta) => Ok(DateTime::<Utc>::from(meta)),
        Err(_)   => Err(http::Error::new_not_found(path))
    }
}

pub async fn calculate_md5(path: impl AsRef<Path>)
    -> http::Result<String>
{
    // TODO: Something more efficient than reading the whole file to a vec.
    let mut buf = Vec::new();
    open_file(path).await?
        .read_to_end(&mut buf).await?;

    Ok(format!("{:x}", md5::compute(&buf)))
}

pub async fn is_path_within(path: impl AsRef<Path>, root: impl AsRef<Path>)
    -> http::Result<bool>
{
    let path_real = canonicalize(&path).await
        // Use a special mapping for this error so that an unknown path results
        // in a 404 rather than a 500.
        .map_err(|_| http::Error::new_not_found(path))?;
    let root_real = canonicalize(&root).await?;

    Ok(path_real.starts_with(&root_real))
}
