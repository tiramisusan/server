use super::*;

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
struct Config { path: String }

struct StaticMoved(String);

impl component::Component<ServiceComponent> for StaticMoved
{
    type Config = Config;

    fn name() -> &'static str { "static_moved" }

    fn create(conf: Self::Config, _: &Params) -> StdResult<Self>
    {
        Ok(Self(conf.path))
    }
}

component::register!(ServiceComponent, StaticMoved);

#[async_trait]
impl Service for StaticMoved
{
    async fn handle(&self, _request: &mut http::Request<'_>, _uri: &http::Uri)
        -> http::Result<http::Response>
    {
        Err(http::Error::Moved(self.0.clone()))
    }
}
