use super::*;

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
struct Config
{
    string: String
}

struct StaticStr(String);

impl component::Component<ServiceComponent> for StaticStr
{
    type Config = Config;

    fn name() -> &'static str { "static_str" }

    fn create(conf: Self::Config, _: &Params) -> StdResult<Self>
    {
        Ok(Self(conf.string))
    }
}

component::register!(ServiceComponent, StaticStr);

#[async_trait]
impl Service for StaticStr
{
    async fn handle(&self, _request: &mut http::Request<'_>, _uri: &http::Uri)
        -> http::Result<http::Response>
    {
        http::Response::new(self.0.clone())
    }
}
