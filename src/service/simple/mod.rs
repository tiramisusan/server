use super::*;

mod hello_world;
pub use hello_world::*;

mod static_str;
pub use static_str::*;

mod static_moved;
pub use static_moved::*;
