use super::*;

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
struct Config { }

struct HelloWorld;

impl component::Component<ServiceComponent> for HelloWorld
{
    type Config = Config;

    fn name() -> &'static str { "hello_world" }

    fn create(_: Self::Config, _: &Params) -> StdResult<Self> { Ok(Self) }
}

component::register!(ServiceComponent, HelloWorld);

#[async_trait]
impl Service for HelloWorld
{
    async fn handle(&self, _request: &mut http::Request<'_>, _uri: &http::Uri)
        -> http::Result<http::Response>
    {
        http::Response::new(String::from("Hello World!\n"))
    }
}
