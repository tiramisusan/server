use super::*;

use chrono::NaiveDate;
use tokio::sync::RwLock;

mod sorted;
use sorted::Sorted;

mod store;
use store::Store;

mod post;
use post::Post;

mod watcher;
use watcher::Watcher;

mod list_page;
use list_page::generate_list_page;

mod post_page;
use post_page::generate_post_page;

fn default_list_template() -> String { String::from("blog_list.html") }
fn default_post_template() -> String { String::from("blog_post.html") }

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
struct BlogConfig
{
    posts_path: PathBuf,
    name: String,
    #[serde(default)]
    blurb_text: String,
    #[serde(default="default_list_template")]
    list_template: String,
    #[serde(default="default_post_template")]
    post_template: String,
    #[serde(default)]
    matthewdown: config::MdConfig
}

/// A unique identifier to assign to each post.
type PostId = usize;

/// A state for a blog service.
struct Blog
{
    config:  BlogConfig,
    /// A store of blog posts.
    store:   RwLock<Store>,
    /// A watcher
    watcher: RwLock<Watcher>
}

/// Given an iterator of paths, load them into a store of blog posts.
async fn load_posts_from_paths<I, P>(store: &mut Store, paths: I)
    where P: AsRef<Path>, I: IntoIterator<Item=P>
{
    for path in paths.into_iter()
    {
        if let Err(e) = store.load_path(&path).await
        {
            error!("Error loading blog post {:?}: {}", path.as_ref(), e);
        }
        else
        {
            info!("Loaded blog post {:?}", path.as_ref());
        }
    }
}

/// Given an iterator of paths, remove them from a store of blog posts.
fn remove_posts_from_paths<I, P>(store: &mut Store, paths: I)
    where P: AsRef<Path>, I: IntoIterator<Item=P>
{
    for path in paths.into_iter()
    {
        store.remove_path(&path);
        info!("Removed blog post {:?}", path.as_ref())
    }
}

impl Blog
{
    /// Load or update any posts.
    ///
    /// This queries the watcher for a list of paths that need to be updated or
    /// removed, and then handles them.
    async fn update(&self) -> io::Result<()>
    {
        let mut watcher = self.watcher.write().await;
        let (add, rem) = watcher.updates().await?;
        drop(watcher);

        let mut store  = self.store.write().await;
        remove_posts_from_paths(&mut store, rem);
        load_posts_from_paths(&mut store, add).await;

        Ok(())
    }

    /// Serve a blog post as an HTTP response.
    async fn serve_post(&self, uri: &str) -> http::Result<http::Response>
    {
        let store = self.store.read().await;
        if let Some(post) = store.lookup_uri(uri)
        {
            generate_post_page(&store, &self.config, post)
        }
        else
        {
            Err(http::Error::new_not_found(uri))
        }
    }

    /// Serve the blog list as an HTTP response.
    async fn serve_list(&self) -> http::Result<http::Response>
    {
        let store = self.store.read().await;
        generate_list_page(&store, &self.config)
    }
}

impl component::Component<ServiceComponent> for Blog
{
    type Config = BlogConfig;

    fn name() -> &'static str { "blog" }

    fn create(config: Self::Config, _: &Params) -> StdResult<Self>
    {
        let     store   = Store::new(config.matthewdown.clone());
        let mut watcher = Watcher::new()?;

        watcher.add_dir(&config.posts_path)?;

        Ok(Self {
            config,
            store:   RwLock::new(store),
            watcher: RwLock::new(watcher)
        })
    }
}

component::register!(ServiceComponent, Blog);

#[async_trait]
impl Service for Blog
{
    async fn slash_disposition(&self, uri: &http::Uri) -> SlashDisposition
    {
        use SlashDisposition::*;

        // For blogs, our URI behaviour is to always trail on the root page, and
        // never trail for any other URI, since everything else is a post.
        if uri.is_empty() { AlwaysTrail } else { NeverTrail }
    }

    async fn handle(&self, _request: &mut http::Request<'_>, uri: &http::Uri)
        -> http::Result<http::Response>
    {
        // Update and re-render any changed files.
        self.update().await?;

        if uri.is_empty()
        {
            // The root URI is the post list
            self.serve_list().await
        }
        else
        {
            // Other URIs are posts
            self.serve_post(uri.as_ref()).await
        }
    }
}

