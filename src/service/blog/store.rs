use super::*;

/// A structure for storing and indexing blog posts
pub struct Store
{
    md_conf: config::MdConfig,
    next_id: PostId,
    posts:   HashMap<PostId, Post>,
    by_path: HashMap<PathBuf, PostId>,
    by_uri:  Sorted<String>,
    by_date: Sorted<NaiveDate>
}

impl Store
{
    pub fn new(md_conf: config::MdConfig) -> Self
    {
        Self
        {
            md_conf,
            next_id: Default::default(),
            posts:   Default::default(),
            by_path: Default::default(),
            by_uri:  Default::default(),
            by_date: Default::default()
        }
    }

    fn allocate_id(&mut self) -> PostId
    {
        let id = self.next_id;
        self.next_id += 1;

        id
    }

    fn add_post(&mut self, post: Post)
    {
        let id:   PostId    = post.id();
        let date: NaiveDate = post.date();
        let uri:  String    = post.preferred_uri();
        let path: PathBuf   = PathBuf::from(post.path());

        self.posts.insert(id, post);
        self.by_path.insert(path, id);
        self.by_uri.insert(uri, id);
        self.by_date.insert(date, id);
    }

    /// Load a post from a path.
    pub async fn load_path(&mut self, path: impl AsRef<Path>) -> StdResult<()>
    {
        self.remove_path(path.as_ref());

        let id = self.allocate_id();
        let post = Post::new(id, path.as_ref(), &self.md_conf).await
            .annotate_with(|| format!("Loading post {:?}", path.as_ref()))?;
        self.add_post(post);

        Ok(())
    }

    /// Remove a post loaded from a path.
    pub fn remove_path(&mut self, path: impl AsRef<Path>)
    {
        if let Some(post_id) = self.by_path.remove(path.as_ref())
        {
            if let Some(post) = self.posts.remove(&post_id)
            {
                self.by_uri.remove(post.preferred_uri(), post_id);
                self.by_date.remove(post.date(), post_id);
            }
        }
    }

    /// Get the URI of a post.
    ///
    /// Posts all want to be referred to by their preferred URI, but multiple
    /// posts may try and use the same URI, so we disambiguate them by adding
    /// an underscore (which does not appear in a normal URI) and a number.
    ///
    pub fn get_uri(&self, post: &Post) -> String
    {
        let preferred_uri = post.preferred_uri();
        let id            = post.id();

        // Get the index of this post in the list of posts with the same
        // preferred URI to disambiguate it.
        let disambig = self.by_uri.iter_matching(preferred_uri.clone())
            .position(|iter| iter == id)
            .unwrap_or(0);

        // Other posts already claimed this URI!
        if disambig > 0
        {
            format!("{}_{}", preferred_uri, disambig)
        }
        // This is the first post with this URI!
        else
        {
            preferred_uri
        }
    }

    /// Get the post corresponding to a URI.
    ///
    /// At the moment, there are two forms of URI:
    ///
    /// * `[preferred_uri]` - The first post with the specified preferred URI.
    /// * `[preferred_uri]_[n]` - The n-th post with the specified preferred
    ///                           URI.
    ///
    pub fn lookup_uri<'st>(&'st self, mut uri: &str) -> Option<&'st Post>
    {
        // A value for disambiguating collided URIs.
        let mut disambig = 0;

        // If the URI has a disambiguation suffix, parse it.
        if let Some((pre, post)) = uri.split_once('_')
        {
            // Select the pre-underscore part as the URI
            uri = pre;

            // Parse the post-underscore part as disambiguation suffix
            if let Ok(val) = post.parse::<usize>()
            {
                disambig = val;
            }
        }

        let id = self.by_uri
            .iter_matching(uri.to_owned()) // Get all matching URIs
            .nth(disambig)?;               // Disambiguate the Nth

        Some(&self.posts[&id])
    }

    /// Iter chronologically through posts.
    pub fn iter_chrono<'st>(&'st self) -> impl Iterator<Item=&'st Post> + 'st
    {
        self.by_date.iter().map(|id| &self.posts[&id])
    }

    /// Get the next chronological post.
    pub fn next_chrono_post<'st, 'p>(&'st self, post: &'p Post)
        -> Option<&'st Post>
    {
        let next_id = self.by_date.get_next(post.date(), post.id())?;
        self.posts.get(&next_id)
    }

    /// Get the previous chronological post.
    pub fn prev_chrono_post<'st, 'p>(&'st self, post: &'p Post)
        -> Option<&'st Post>
    {
        let prev_id = self.by_date.get_prev(post.date(), post.id())?;
        self.posts.get(&prev_id)
    }
}
