use super::*;

use inotify::{Inotify, WatchMask, WatchDescriptor};

/// A structure to watch a directory for blog posts.
pub struct Watcher
{
    dirs:      HashMap<WatchDescriptor, PathBuf>,
    to_reload: Vec<PathBuf>,
    inotify:   Inotify,
}

/// Get whether a file exists.
async fn exists(path: impl AsRef<Path>) -> bool
{
    if let Ok(meta) = tokio::fs::metadata(path).await
    {
        meta.is_file()
    }
    else
    {
        false
    }
}

/// Get whether a file is relevant to the blog service.
fn is_relevant(path: impl AsRef<Path>) -> bool
{
    path.as_ref().extension() == Some("md".as_ref())
}

async fn iter_dir(path: impl AsRef<Path>) -> io::Result<HashSet<PathBuf>>
{
    let mut paths = tokio::fs::read_dir(path).await?;
    let mut rtn = HashSet::new();

    while let Some(entry) = paths.next_entry().await?
    {
        let file_path = entry.path();

        if is_relevant(&file_path)
        {
            rtn.insert(file_path);
        }
    }

    Ok(rtn)
}

impl Watcher
{
    pub fn new() -> io::Result<Self>
    {
        Ok(Self {
            dirs: HashMap::new(),
            to_reload: Vec::new(),
            inotify: Inotify::init()?
        })
    }

    /// Add a directory to this loader.
    pub fn add_dir(&mut self, path: impl AsRef<Path>) -> io::Result<()>
    {
        self.to_reload.push(PathBuf::from(path.as_ref()));
        let wd = self.inotify.watches().add(
            &path, WatchMask::CREATE | WatchMask::DELETE | WatchMask::MODIFY
        )?;
        self.dirs.insert(wd, PathBuf::from(path.as_ref()));

        Ok(())
    }

    /// Get all paths that have been updated (added, modified, or removed)
    async fn all_updated_paths(&mut self) -> io::Result<HashSet<PathBuf>>
    {
        let mut buf: [u8; 4096] = [0; 4096];

        match self.inotify.read_events(&mut buf)
        {
            Ok(events) =>
            {
                Ok(events
                    // Get the full path for each event. The event name is just
                    // the filename, so we join it onto the directory associated
                    // with the watch descriptor.
                    .map(|evt| self.dirs[&evt.wd].join(evt.name.unwrap()))
                    // Get events from .md files
                    .filter(|path| is_relevant(path))
                    // Collect into a hash set
                    .collect::<HashSet<PathBuf>>())
            }
            Err(e) if e.kind() == io::ErrorKind::WouldBlock =>
            {
                Ok(HashSet::new())
            }
            Err(e) =>
            {
                Err(e)
            }
        }
    }

    /// Get all paths that have been updated, sorted into added and revmoed.
    ///
    /// The first item of the returned tuple is paths that have been added or
    /// modified, and the second is paths that have been removed.
    ///
    pub async fn updates(&mut self)
        -> io::Result<(HashSet<PathBuf>, HashSet<PathBuf>)>
    {
        let mut add = HashSet::new();
        let mut rem = HashSet::new();

        for dir in self.to_reload.drain(..)
        {
            add.extend(iter_dir(dir).await?);
        }

        for path in self.all_updated_paths().await?
        {
            match exists(&path).await
            {
                true  => { add.insert(path); },
                false => { rem.insert(path); }
            }
        }

        Ok((add, rem))
    }
}
