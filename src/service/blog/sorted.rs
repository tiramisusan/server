use super::*;

use std::ops::Bound::*;
use std::collections::BTreeSet;

/// A sortable key, value pair.
///
/// The pairs are sorted by the specified key, and then disambiguated by the ID,
/// which is always unique.
///
#[derive(PartialEq, Eq, PartialOrd, Ord)]
struct Pair<K>
    where K: Ord
{
    key: K,
    id:  PostId
}

/// A collection of `Id`s, sorted by a key, `K`.
///
/// It is assumed that given a post, the key can be looked up.
///
#[derive(Default)]
pub struct Sorted<K>
    where K: Ord
{
    set: BTreeSet<Pair<K>>
}

impl<K> Sorted<K>
    where K: Ord
{
    /// Insert a new sorted ID.
    pub fn insert(&mut self, key: K, id: PostId)
    {
        self.set.insert(Pair { key, id });
    }

    /// Remove a sorted ID.
    pub fn remove(&mut self, key: K, id: PostId)
    {
        self.set.remove(&Pair { key, id });
    }

    /// Get the next sorted ID.
    pub fn get_next(&self, key: K, id: PostId)-> Option<PostId>
    {
        let pair = Pair { key, id };
        self.set.range((Excluded(pair), Unbounded))
            .next()
            .map(|pair| pair.id)
    }

    /// Get the previous sorted ID.
    pub fn get_prev(&self, key: K, id: PostId) -> Option<PostId>
    {
        let pair = Pair { key, id };
        self.set.range(..pair)
            .rev()
            .next()
            .map(|pair| pair.id)
    }

    /// Iterate all posts in order.
    pub fn iter<'a>(&'a self) -> impl Iterator<Item=PostId> + 'a
    {
        self.set.iter().map(|pair| pair.id)
    }

    /// Iterate all the posts matching a particular key.
    pub fn iter_matching<'a>(&'a self, key: K)
        -> impl Iterator<Item=PostId> + 'a
        where K: Clone
    {
        let pair = Pair { key: key.clone(), id: 0 };
        self.set.range(pair..)
            .take_while(move |pair| pair.key == key)
            .map(|pair| pair.id)
    }
}
