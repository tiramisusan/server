use super::*;

/// Information to supply to a list-page template.
#[derive(serde::Serialize)]
struct ListInfo
{
    /// The name of the blog.
    pub name: String,

    /// A blurb discribing the blog.
    pub blurb: String,

    /// A list of `PostInfo` describing each post, in chronological order.
    pub posts: Vec<post_page::PostInfo>
}

impl ListInfo
{
    /// Collect the information from a `Store` for a list-page.
    pub fn new_from_store(store: &Store, config: &BlogConfig) -> Self
    {
        let name  = config.name.clone();
        let blurb = config.blurb_text.clone();
        let posts = store.iter_chrono()
            .filter(|p| p.is_listable())
            .map(|p| post_page::PostInfo::new_from_store(p, store))
            .collect();

        Self { name, blurb, posts }
    }
}

/// Generate a list-page.
pub fn generate_list_page(store: &Store, config: &BlogConfig)
    -> http::Result<http::Response>
{
    let info = ListInfo::new_from_store(store, config);
    let name = info.name.clone();

    http::Response::new(
        html::TemplatePage::new(&name, &config.list_template, info)
    )
}
