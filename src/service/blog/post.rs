use super::*;

#[derive(serde::Deserialize, serde::Serialize, Clone)]
pub struct Metadata
{
    pub title:    String,
    pub date:     NaiveDate,
    pub authors:  Vec<String>,
    #[serde(default)]
    pub keywords: Vec<String>,
    #[serde(default)]
    pub wip:      bool
}

pub struct Post
{
    /// The Id of this post.
    id:       PostId,
    /// Path this post was loaded from.
    path:     PathBuf,
    /// Rendered HTML of the Matthewdown content of the post.
    html:     Arc<String>,
    /// Metadata as loaded from the Matthewdown file.
    metadata: Metadata,
}

/// Parse matthewdown to a string of HTML content and some metadata.
fn parse_matthewdown(md: &str, md_conf: &config::MdConfig)
    -> StdResult<(String, Metadata)>
{
    use matthewdown::{HtmlVisitor, MetadataVisitor};
    let mut html = String::new();
    let html_visitor = HtmlVisitor::new_with_output(&mut html)
        .with_config(md_conf.md_html_config());

    let mut metadata_visitor = MetadataVisitor::from(html_visitor);

    // Parse the matthewdown. The HTML will be generated and placed in the
    // string.
    matthewdown::visit_str(md, &mut metadata_visitor)
        .annotate("Error parsing post")?;

    // Deserialize the metadata into a struct.
    let metadata = metadata_visitor.deserialize_metadata::<Metadata>()
        .annotate("Error parsing post metadata")?;

    Ok((html, metadata))
}

impl Post
{
    /// Load this post from a file.
    pub async fn new<P>(id: PostId, path_ref: P, md_conf: &config::MdConfig)
        -> StdResult<Self>
        where P: AsRef<Path>
    {
        let path = PathBuf::from(path_ref.as_ref());

        info!("Loading post from {:?} ...", path);

        // Load and parse the matthewdown from the file
        let md = tokio::fs::read_to_string(&path).await?;
        let (html, metadata) = parse_matthewdown(&md, md_conf)?;

        Ok(Self { html: Arc::new(html), metadata, path, id })
    }

    /// Get a preferred URI for this post.
    ///
    /// This URI will be used to access the post unless it collides with another
    /// post's URI.
    pub fn preferred_uri(&self) -> String
    {
        let mut prev = ' ';
        self.metadata.title.chars()
            .map(|ch| match ch {
                'A'..='Z'              => Some(ch.to_ascii_lowercase()),
                'a'..='z' | '0'..='9'  => Some(ch),
                '_' | '-' | ' ' | '\t' => Some('-'),
                _                      => None
            })
            .flatten()
            .filter(|&ch| {
                let skip = ch == '-' && prev == '-';
                prev = ch;
                !skip
            })
            .collect::<String>()
    }

    pub fn id(&self) -> PostId { self.id }

    pub fn date(&self) -> NaiveDate { self.metadata.date.clone() }

    pub fn path(&self) -> &Path { &self.path }

    pub fn metadata(&self) -> &Metadata { &self.metadata }

    /// Whether or not this post can appear in the archive list.
    pub fn is_listable(&self) -> bool { !self.metadata.wip }

    pub fn html(&self) -> Arc<String> { self.html.clone() }
}
