use super::*;

/// Information to supply to a post page template.
#[derive(serde::Serialize)]
pub struct PostInfo
{
    /// Post metadata.
    #[serde(flatten)]
    metadata: post::Metadata,

    /// HTML containing the content of the post.
    html: Arc<String>,

    /// The URI of this post.
    this_uri: String,

    /// The URI of the next chronological post, if one exists.
    next_uri: Option<String>,

    /// The URI of the previous chronological post, if one exists.
    prev_uri: Option<String>
}

impl PostInfo
{
    /// Create a new `PostInfo` from a post and store.
    pub fn new_from_store(post: &Post, store: &Store) -> Self
    {
        let metadata = post.metadata().clone();
        let html     = post.html();
        let next_uri = store.next_chrono_post(post)
            .map(|p| store.get_uri(p));
        let prev_uri = store.prev_chrono_post(post)
            .map(|p| store.get_uri(p));
        let this_uri = store.get_uri(post);

        Self { metadata, html, next_uri, prev_uri, this_uri }
    }
}

/// Generate a page from a post.
pub fn generate_post_page(store: &Store, config: &BlogConfig, post: &Post)
    -> http::Result<http::Response>
{
    let info = PostInfo::new_from_store(post, store);
    let title = info.metadata.title.clone();

    http::Response::new(
        html::TemplatePage::new(&title, &config.post_template, info)
    )
}
