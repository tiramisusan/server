use super::*;

use tokio::sync::Mutex;
use std::time::Instant;

fn default_brew_time() -> Duration { Duration::from_secs(5) }

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
struct Config
{
    #[serde(default="default_brew_time")]
    brew_time: Duration,

    #[serde(default)]
    browser_template: Option<String>,

    #[serde(default)]
    tty_template: Option<String>
}

enum State
{
    Idle,
    Brewing(Instant, i32),
    Done
}

struct Moka
{
    config:    Config,
    state:     Mutex<State>
}

impl component::Component<ServiceComponent> for Moka
{
    type Config = Config;

    fn name() -> &'static str { "moka" }

    fn create(config: Self::Config, _: &Params) -> StdResult<Self>
    {
        Ok(Self { config, state: Mutex::new(State::Idle) })
    }
}

component::register!(ServiceComponent, Moka);

impl State
{
    fn update_state(&mut self)
    {
        if let State::Brewing(inst, ref mut frame) = self
        {
            if *inst < Instant::now()
            {
                *self = State::Done;
            }
            else
            {
                /* Advance the frame of the brewing animation */
                const NUM_FRAMES: i32 = 2;
                *frame = (*frame + 1) % NUM_FRAMES;
            }
        }
    }

    fn art(&self) -> &'static str
    {
        match self
        {
            State::Idle          => include_str!("moka-idle.txt"),
            State::Brewing(_, 0) => include_str!("moka-brewing.txt"),
            State::Brewing(_, _) => include_str!("moka-brewing-2.txt"),
            State::Done          => include_str!("moka-done.txt")
        }
    }

    fn title(&self) -> &'static str
    {
        match self
        {
            State::Done => "Coffee is served!",
            _               => "Moka"
        }
    }

    fn get_response(&mut self, config: &Config)
        -> http::Result<http::Response>
    {
        /* Build a page displaying our moka art */
        let mut page = html::TextPage::new(self.title(), self.art().to_owned());

        /* Apply any custom templates from the config */
        config.browser_template
            .as_ref()
            .map(|tmpl| page.set_html_template(&tmpl));

        config.tty_template
            .as_ref()
            .map(|tmpl| page.set_text_template(&tmpl));

        let mut response = http::Response::new(page)?;

        /* Drink the coffee if it was made! */
        if matches!(self, State::Done)
        {
            response.set_status(201, "Created");
            *self = State::Idle;
        }

        Ok(response)
    }

    fn brew_stop(&mut self) -> http::Result
    {
        if !matches!(self, State::Brewing(_, _))
        {
            return Err(http::Error::from(
                "Cannot stop brewing - Moka is not currently brewing."
            ));
        }

        *self = State::Idle;

        Ok(())
    }

    fn brew_start(&mut self, brew_time: Duration) -> http::Result
    {
        if !matches!(self, State::Idle)
        {
            return Err(http::Error::from(
                "Cannot start brewing - Moka is not idle."
            ));
        }

        *self = State::Brewing(Instant::now() + brew_time, 0);

        Ok(())
    }
}

impl Moka
{
    async fn handle_get(&self, _request: &mut http::Request<'_>)
        -> http::Result<http::Response>
    {
        let mut state = self.state.lock().await;
        state.update_state();
        state.get_response(&self.config)
    }

    async fn handle_brew(&self, request: &mut http::Request<'_>)
        -> http::Result<http::Response>
    {
        let mut state = self.state.lock().await;
        state.update_state();

        let media_type = request.headers()
            .decode_or_error::<http::header::ContentType>()?
            .media_type();

        if !matches!(
            media_type.tuple(),
            ("message", "coffeepot") | ("application", "coffee-pot-command")
        )
        {
            return Err(http::Error::new_bad_request(
                "Expected a body of type 'message/coffeepot' \
                or 'application/coffee-pot-command"
            ));
        }

        use http::Upload;
        let message = request.body()?.into_string().await?;

        match message.trim()
        {
            "start" =>
                state.brew_start(self.config.brew_time)?,
            "stop" =>
                state.brew_stop()?,
            _ =>
                return Err(http::Error::new_bad_request(
                    "Expected body to say 'start' or 'stop'"
                ))
        }

        state.get_response(&self.config)
    }
}

use http::Method::*;

#[async_trait]
impl Service for Moka
{
    fn allowed_methods(&self) -> http::MethodSet { Get | Brew | Post }

    async fn handle(&self, request: &mut http::Request<'_>, uri: &http::Uri)
        -> http::Result<http::Response>
    {
        let uri_str: &str = uri.as_ref();
        match (uri_str, request.method())
        {
            ("", Post | Brew) =>
                self.handle_brew(request).await,
            ("", Get) =>
                self.handle_get(request).await,
            ("", _) =>
                Err(http::Error::MethodNotAllowed(Post | Brew | Get)),
            (_, _) =>
                Err(http::Error::new_not_found(uri_str))
        }
    }
}
