use super::*;

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
struct Config {}

struct Teapot;

impl component::Component<ServiceComponent> for Teapot
{
    type Config = Config;

    fn name() -> &'static str { "teapot" }

    fn create(_: Self::Config, _: &Params) -> StdResult<Self> { Ok(Self) }
}

component::register!(ServiceComponent, Teapot);

#[async_trait]
impl Service for Teapot
{
    fn allowed_methods(&self) -> http::MethodSet
    {
        http::Method::Get | http::Method::Brew | http::Method::Post
    }

    async fn handle(&self, _request: &mut http::Request<'_>, _uri: &http::Uri)
        -> http::Result<http::Response>
    {
        Err(http::Error::IAmATeapot(include_str!("teapot.txt").to_owned()))
    }
}
