use super::*;

mod limited_file;
use limited_file::LimitedFile;

mod backend;
use backend::Backend;

mod metadata;
use metadata::UploadMetadata;

mod source;

fn default_max_bytes() -> usize { 5 * 1024 * 1024 }
fn default_max_files() -> usize { 100 }

fn default_sources() -> Vec<source::SourceConfig>
{
    vec![source::SourceConfig::Http {}]
}

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
struct Config
{
    /// The maximum number of bytes per file.
    #[serde(default="default_max_bytes")]
    max_bytes: usize,
    /// The maximum number of files.
    #[serde(default="default_max_files")]
    max_files: usize,
    /// The base href to use when returning URLs.
    base_href: String,

    #[serde(default="default_sources")]
    sources: Vec<source::SourceConfig>
}

struct Paste
{
    backend: Arc<Backend>,
    sources: Vec<Box<dyn source::Source>>
}

impl component::Component<ServiceComponent> for Paste
{
    type Config = Config;

    fn name() -> &'static str { "paste" }

    fn create(conf: Self::Config, params: &Params) -> StdResult<Self>
    {
        let config  = Arc::new(conf);
        let backend = Arc::new(
            Backend::new(config.clone(), params.scratch.clone())
        );

        let sources = config.sources.iter()
            .map(|src_config|
                source::create(src_config, config.clone(), backend.clone())
            )
            .collect();

        Ok(Self { backend, sources })
    }
}

component::register!(ServiceComponent, Paste);

impl Paste
{
    async fn handle_download(
        &self,
        request: &http::Request<'_>,
        uri:     &http::Uri
    )
        -> http::Result<http::Response>
    {
        use http::header::*;

        let uri: &str = uri.as_ref();

        request.expect_method(http::Method::Get)?;

        let id   = uri.split('.').next().unwrap_or("");
        let file = self.backend.download(id).await?;

        http::Response::new(file)?
            .with_header(&ContentType::from(MediaType::guess_from_path(uri)))
    }
}


#[async_trait]
impl Service for Paste
{
    /// Support ALL methods, and handle them explicitly.
    ///
    /// This means we need to check the method ourselves.
    fn allowed_methods(&self) -> http::MethodSet { http::MethodSet::all() }

    async fn handle(&self, request: &mut http::Request<'_>, uri: &http::Uri)
        -> http::Result<http::Response>
    {
        for source in self.sources.iter()
        {
            if let Some(rsp) = source.handle_http(request, uri).await
            {
                return rsp;
            }
        }

        self.handle_download(request, uri).await
    }
}
