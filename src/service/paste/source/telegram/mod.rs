use super::*;

use teloxide::prelude::*;

mod bot;

/// A pastebin source for messages uploaded from telegram
pub(super) struct Telegram
{
    shutdown: tokio::task::JoinHandle<()>
}

impl Drop for Telegram
{
    fn drop(&mut self) { self.shutdown.abort() }
}

impl Source for Telegram
{
}

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Config { }

impl Telegram
{
    pub fn new(backend: Arc<Backend>) -> Self
    {
        info!("Using telegram as a paste source.");
        Self { shutdown: bot::build(backend) }
    }
}
