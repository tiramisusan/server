use super::*;

use teloxide::{
    types::FileMeta,
    net::Download
};

struct Context
{
    bot:     Bot,
    msg:     Message,
    backend: Arc<Backend>
}

impl Context
{
    fn new(bot: Bot, msg: Message, backend: Arc<Backend>) -> Context
    {
        Self { bot, msg, backend }
    }

    async fn reply(&self, s: &str) -> ResponseResult<()>
    {
        (&self.bot).parse_mode(teloxide::types::ParseMode::Html)
            .send_message(self.msg.chat.id, s).await?;

        Ok(())
    }

    async fn upload_str(&self, s: &str) -> ResponseResult<()>
    {
        let (mut dst, metadata) = self.backend.upload("msg.txt").await?;

        dst.write(s.as_bytes()).await?;
        drop(dst);

        self.reply(&metadata.absolute_href()).await?;

        Ok(())
    }

    async fn upload_file(&self, file: &FileMeta, name: &str)
        -> ResponseResult<()>
    {
        let (mut dst, metadata) = self.backend.upload(name).await?;

        let dl_file = self.bot.get_file(&file.id).await?;
        self.bot.download_file(&dl_file.path, &mut dst).await?;
        drop(dst);

        self.reply(&metadata.absolute_href()).await?;

        Ok(())
    }

    async fn handle(self) -> ResponseResult<()>
    {
        match self.msg.text()
            .and_then(|s| s.trim().split_once(' '))
        {
            Some(("/upload", payload)) =>
            {
                self.upload_str(payload).await
            }
            _ => if let Some(doc) = self.msg.document()
            {
                let file_name = doc.file_name.as_deref().unwrap_or("");
                self.upload_file(&doc.file, file_name).await
            }
            else if let Some(photo) = self.msg.photo()
                .and_then(|list| list.iter().max_by_key(|p| p.width * p.height))
            {
                self.upload_file(&photo.file, "img.jpg").await
            }
            else if let Some(video) = self.msg.video()
            {
                let name = video.file_name.as_deref().unwrap_or("video.mp4");
                self.upload_file(&video.file, name).await
            }
            else
            {
                self.reply(&HELP_PAGE).await
            }
        }
    }

}

const HELP_PAGE: &'static str = r#"<b>Usage</b>

I am a bot that uploads text and images to a temporary pastebin.

/help - Display this page.
/upload <em>Text</em> - Upload the contents of your message to the pastebin.
<em>File or Photo</em> - Upload the file or photo to the pastebin.
"#;

pub(crate) fn build(backend: Arc<Backend>)
    -> tokio::task::JoinHandle<()>
{
    let schema = Update::filter_message()
        .endpoint(move |bot, msg| {
            Context::new(bot, msg, backend.clone()).handle()
        });

    tokio::task::spawn(async {
        Dispatcher::builder(Bot::from_env(), schema)
            .build()
            .dispatch()
            .await
    })
}

