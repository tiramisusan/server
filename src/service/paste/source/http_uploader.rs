use super::*;

use http::Upload;

/// A pastebin source for messages upload:ed from telegram
pub(super) struct HttpUploader
{
    backend: Arc<Backend>,
    config:  Arc<Config>
}

impl HttpUploader
{
    pub fn new(config: Arc<Config>, backend: Arc<Backend>) -> Self
    {
        Self { backend, config }
    }

    async fn handle_upload(&self, request: &mut http::Request<'_>)
        -> http::Result<http::Response>
    {
        request.expect_method(http::Method::Post | http::Method::Put)?;
        match request.body_len()
        {
            // A body of acceptable length was found.
            Some(len) if len <= self.config.max_bytes =>
            {
                let (mut file, meta) = self.backend.upload("").await?;

                let len = request.body()?.read_body(&mut file).await?;

                let href = meta.absolute_href();
                info!("Uploaded {} bytes to {:?}", len, href);
                http::Response::new(href + "\n")
            }
            // A body was found but was too large.
            Some(_) => Err(http::Error::EntityTooLarge),
            // We need a length to decide whether we can upload the file.
            None    => Err(http::Error::LengthRequired)
        }
    }
}


#[async_trait]
impl Source for HttpUploader
{
    async fn handle_http(
        &self,
        request: &mut http::Request<'_>,
        uri:     &http::Uri
    )
        -> Option<http::Result<http::Response>>
    {
        if uri.is_empty()
        {
            Some(self.handle_upload(request).await)
        }
        else
        {
            None
        }
    }
}
