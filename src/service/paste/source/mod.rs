use super::*;

mod http_uploader;
use http_uploader::HttpUploader;

mod form_uploader;
use form_uploader::FormUploader;

mod telegram;
use telegram::Telegram;

fn default_list_template() -> String { String::from("paste_list.html") }
fn default_form_template() -> String { String::from("paste_form.html") }

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
#[serde(rename_all="kebab-case")]
pub(super) enum SourceConfig
{
    Http {},
    Telegram {},
    Form
    {
        #[serde(default="default_list_template")]
        list_template: String,
        #[serde(default="default_form_template")]
        form_template: String
    }
}

#[async_trait]
pub trait Source: Sync + Send + 'static
{
    async fn handle_http(
        &self,
        request: &mut http::Request<'_>,
        uri:     &http::Uri
    )
        -> Option<http::Result<http::Response>>
    {
        let _ = request;
        let _ = uri;
        None
    }
}

pub(crate) fn create(
    src_config: &SourceConfig,
    config:     Arc<Config>,
    backend:    Arc<Backend>
)
    -> Box<dyn Source>
{
    match src_config
    {
        SourceConfig::Http {} =>
            Box::new(HttpUploader::new(config, backend)),
        SourceConfig::Form { list_template, form_template } =>
            Box::new(
                FormUploader::new(
                    config,
                    backend,
                    list_template.clone(),
                    form_template.clone()
                )
            ),
        SourceConfig::Telegram {} =>
            Box::new(Telegram::new(backend))
    }
}
