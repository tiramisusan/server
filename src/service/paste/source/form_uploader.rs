use super::*;

use http::{FormData, FormField};

pub(super) struct FormUploader
{
    backend:       Arc<Backend>,
    config:        Arc<Config>,
    list_template: String,
    form_template: String
}

impl FormUploader
{
    pub fn new(
        config:        Arc<Config>,
        backend:       Arc<Backend>,
        list_template: String,
        form_template: String
    )
        -> Self
    {
        Self { backend, config, list_template, form_template }
    }

    fn make_form(&self) -> http::Result<http::Response>
    {
        #[derive(serde::Serialize)]
        struct FormInfo { max_bytes: usize }

        let info = FormInfo { max_bytes: self.config.max_bytes };
        let name = "Upload File";

        let page = html::TemplatePage::new(
            &name, &self.form_template, info
        );

        http::Response::new(page)
    }

    fn make_list(&self, metadata: Vec<UploadMetadata>)
        -> http::Result<http::Response>
    {
        #[derive(serde::Serialize)]
        struct ListInfo { metadata: Vec<UploadMetadata> }

        let info = ListInfo { metadata };
        let name = "Upload Successful";

        let page = html::TemplatePage::new(&name, &self.list_template, info);
        http::Response::new(page)
    }

    fn redirect_to(&self, metadata: &UploadMetadata)
        -> http::Result<http::Response>
    {
        Err(http::Error::Found(metadata.relative_href()))
    }

    async fn upload_form_field(&self, field: impl http::FormField)
        -> http::Result<UploadMetadata>
    {
        let fname = field.filename().unwrap_or_else(Default::default);

        let (mut file, meta) = self.backend.upload(&fname).await?;
        let len = field.read_body(&mut file).await?;

        info!("Uploaded {} bytes to {:?}", len, meta.absolute_href());

        Ok(meta)
    }

    async fn handle_form(&self, request: &mut http::Request<'_>)
        -> http::Result<http::Response>
    {
        let mut meta = Vec::new();
        let mut do_list = false;
        let mut form = request.multipart_form()?;
        while let Some(field) = form.get_next().await?
        {
            match field.field_name()
            {
                /* The upload field contains our file to upload */
                "upload" => meta.push(self.upload_form_field(field).await?),
                /* The do_list field makes us display our uploads in a list. */
                "do-list" => do_list = true,
                /* Ignore unknown fields */
                _ => {}
            }
        }

        match meta.len()
        {
            0             => Err(http::Error::from("Not attached file.")),
            1 if !do_list => self.redirect_to(&meta[0]),
            _             => self.make_list(meta)
        }
    }

    async fn handle_upload(&self, request: &mut http::Request<'_>)
        -> http::Result<http::Response>
    {
        match request.body_len()
        {
            // A body of acceptable length was found.
            Some(len) if len <= self.config.max_bytes =>
            {
                // Treat the request as a form.
                self.handle_form(request).await
            }
            // A body was found but was too large.
            Some(_) => Err(http::Error::EntityTooLarge),
            // We need a length to decide whether we can upload the file.
            None    => Err(http::Error::LengthRequired)
        }
    }
}

#[async_trait]
impl Source for FormUploader
{
    async fn handle_http(
        &self,
        req: &mut http::Request<'_>,
        uri: &http::Uri
    )
        -> Option<http::Result<http::Response>>
    {
        let uri_str: &str = uri.as_ref();
        if uri_str == "form"
        {
            match req.method()
            {
                http::Method::Get  => { Some(self.make_form()) }
                http::Method::Post => { Some(self.handle_upload(req).await) }
                _ =>
                {
                    Some(Err(http::Error::new_method_not_allowed(
                        http::Method::Get | http::Method::Post
                    )))
                }
            }
        }
        else
        {
            None
        }
    }
}

