use super::*;

pub(super) struct Backend
{
    scratch: Arc<scratch::Scratch>,
    config:  Arc<Config>
}

/// The file type used to store pasted files.
type WrFile = LimitedFile<scratch::WriteFile>;
type RdFile = scratch::ReadFile;

impl Backend
{
    pub(super) fn new(config: Arc<Config>, scratch: Arc<scratch::Scratch>)
        -> Self
    {
        Self { config, scratch }
    }

    pub async fn upload(&self, filename: &str)
        -> io::Result<(WrFile, UploadMetadata)>
    {
        let id   = self.allocate_id().await?;
        let file = LimitedFile::new(
            self.scratch.open_write(&["files", &id]).await?,
            self.config.max_bytes
        );

        let meta = UploadMetadata {
            filename: filename.to_owned(),
            id,
            config: self.config.clone()
        };

        Ok((file, meta))
    }

    pub async fn download(&self, id: &str) -> io::Result<RdFile>
    {
        let file = self.scratch.open_read(&["files", id]).await?;
        Ok(file)
    }

    fn increment_id(&self, n: usize) -> String
    {
        ((n + 1) % self.config.max_files).to_string()
    }

    async fn allocate_id(&self) -> io::Result<String>
    {
        let mut file = self.scratch.open_modify(&["next_id"]).await?;

        let mut id_buf = String::new();
        file.read_to_string(&mut id_buf).await?;
        let id = id_buf.trim().parse().unwrap_or(0);

        file.rewind().await?;
        file.write(self.increment_id(id).as_bytes()).await?;

        info!("Allocated paste ID {:?}", id);

        Ok(id.to_string())
    }
}

