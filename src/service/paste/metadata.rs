use super::*;

pub(crate) struct UploadMetadata
{
    pub filename:  String,
    pub id:        String,
    pub config:    Arc<Config>
}

impl serde::Serialize for UploadMetadata
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where S: serde::Serializer
    {
        use serde::ser::SerializeStruct;

        let mut s = serializer.serialize_struct("UploadMetadata", 4)?;
        s.serialize_field("relative_href", &self.relative_href())?;
        s.serialize_field("absolute_href", &self.absolute_href())?;
        s.serialize_field("filename",      &self.filename)?;
        s.serialize_field("id",            &self.id)?;
        s.end()
    }
}

impl UploadMetadata
{
    pub fn relative_href(&self) -> String
    {
        match self.filename.rsplit_once('.')
        {
            Some((_, ext)) => format!("{}.{}", self.id, ext),
            None           => self.id.clone()
        }
    }

    pub fn absolute_href(&self) -> String
    {
        format!("{}/{}", self.config.base_href, self.relative_href())
    }
}

