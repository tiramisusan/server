use super::*;

use std::{
    task::{Context, Poll},
    pin::Pin
};

/// A wrapper for `AsyncWrite` files, that limits how many bytes can be written.
///
/// Any attempt to write beyond the byte limit will result in an error.
pub struct LimitedFile<F>
{
    remaining: usize,
    inner:     F
}

impl<F> io::AsyncWrite for LimitedFile<F>
    where F: io::AsyncWrite + Unpin
{
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx:       &mut Context<'_>,
        buf:      &[u8]
    )
        -> Poll<Result<usize, io::Error>>
    {
        // Trim the buffer to ensure we don't write too much
        let trimmed = &buf[..std::cmp::min(buf.len(), self.remaining)];

        let result = Pin::new(&mut self.inner).poll_write(cx, trimmed);

        if let Poll::Ready(Ok(n)) = &result
        {
            self.remaining -= n;
        }

        result
    }

    fn poll_flush(
        mut self: Pin<&mut Self>,
        cx:       &mut Context<'_>
    )
        -> Poll<Result<(), io::Error>>
    {
        Pin::new(&mut self.inner).poll_flush(cx)
    }

    fn poll_shutdown(
        mut self: Pin<&mut Self>,
        cx:       &mut Context<'_>
    )
        -> Poll<Result<(), io::Error>>
    {
        Pin::new(&mut self.inner).poll_shutdown(cx)
    }
}

impl<F> LimitedFile<F>
{
    /// Create a new `LimitedFile`.
    ///
    /// # Arguments
    ///  * `inner`     - A file to wrap.
    ///  * `remaining` - The maximum number of bytes to write to the file.
    pub fn new(inner: F, remaining: usize) -> Self
    {
        Self { remaining, inner }
    }
}

#[cfg(test)]
mod test
{
    use super::*;
    #[tokio::test]
    async fn test_limited_file()
    {
        const LIMIT: u64 = 4000;

        let dir = tempdir::TempDir::new("test").unwrap();

        // Get the path for a temporary file.
        let path = |name| { dir.path().join(format!("file-{}", name)) };

        // Create a file, and return a limited handle to it.
        let create = |name| {
            let std_file   = std::fs::File::create(path(name)).unwrap();
            let tokio_file = tokio::fs::File::from_std(std_file);
            LimitedFile::new(tokio_file, LIMIT as usize)
        };

        // Get the length of a file.
        let file_len = |name| {
            std::fs::read_to_string(path(name)).unwrap().len() as u64
        };

        // Get a source of N bytes.
        let src = |n| { io::repeat(b'x').take(n) };

        // Test case 1: Copy an infinite source of data to the limited file.
        assert!(
            io::copy(&mut src(u64::MAX), &mut create("a"))
                .await.is_err()
        );
        assert_eq!(file_len("a"), LIMIT);

        // Test case 2: Copy exactly the correct amount of data.
        assert_eq!(
            io::copy(&mut src(LIMIT), &mut create("b")).await.unwrap(),
            LIMIT
        );
        assert_eq!(file_len("b"), LIMIT);

        // Test case 3: Copy one too many bytes.
        assert!(
            io::copy(&mut src(LIMIT + 1), &mut create("c")).await.is_err()
        );
        assert_eq!(file_len("c"), LIMIT);

        // Test case 4: Copy one byte less than the limit.
        assert_eq!(
            io::copy(&mut src(LIMIT - 1), &mut create("d")).await.unwrap(),
            LIMIT - 1
        );
        assert_eq!(file_len("d"), LIMIT - 1);
    }
}
