use super::*;

mod simple;

mod files;

#[cfg(feature="service_cgi")]
mod cgi;

#[cfg(feature="service_blog")]
mod blog;

#[cfg(feature="service_paste")]
mod paste;

#[cfg(feature="service_beverage")]
mod beverage;

use endpoint::{CacheDisposition, SlashDisposition};

/// An implementable trait for a service.
///
/// A service is a type that can be created from a config, and that handles
/// requests and produces responses.
///
#[async_trait]
pub trait Service: Sync + Send + 'static
{
    /// Whether this service supports a certain method.
    fn allowed_methods(&self) -> http::MethodSet
    {
        // Unless explicitly specified otherwise, services only accept GET.
        From::from(http::Method::Get)
    }

    /// Get the slash disposition of a URI within this service.
    ///
    /// The slash disposition describes how the service treats trailing slashes
    /// at the end of URIs.
    ///
    /// # Arguments
    ///
    ///  * `uri` - A URI relative to the mount-point of this service.
    ///
    async fn slash_disposition(&self, uri: &http::Uri) -> SlashDisposition
    {
        let _ = uri;
        SlashDisposition::DontCare
    }

    /// Get the cache dispotion of a URI within this service.
    ///
    /// The cache disposition describes how the cache headers are populated by
    /// the server.
    ///
    /// # Arguments
    ///
    ///  * `uri` - A URI relative to the mount-point of this service.
    ///
    async fn cache_disposition(&self, uri: &http::Uri) -> CacheDisposition
    {
        let _ = uri;
        CacheDisposition::NoCache
    }

    /// Handle an HTTP request.
    ///
    /// An `http::Response` is returned in case of successs (i.e. 200 response),
    /// or an `http::Error` describing an error is returned otherwise.
    ///
    /// # Arguments
    ///
    ///  * `request` - A structure describing the HTTP request and its headers.
    ///  * `uri` - A URI relative to the mount-point of this service. If the
    ///            absolute URI is required for some reason,
    ///            `request.full_uri()` can be used.
    ///
    async fn handle(&self, request: &mut http::Request<'_>, uri: &http::Uri)
        -> http::Result<http::Response>;
}

pub(crate) struct Params
{
    pub scratch: Arc<scratch::Scratch>
}

component::declare_kind!(
    pub(crate) ServiceComponent, Params=Params, Output=Box<dyn Service>
);

/// Create a generic boxed service from any specific service component.
impl<T> component::ComponentOutput<ServiceComponent> for T
    where T: Service
{
    fn output(self) -> Box<dyn Service> { Box::new(self) }
}

