use super::*;

pub mod header;
pub use header::Header;

pub(crate) mod request;
pub use request::{Request, Method, MethodSet, receive_request};
pub(crate) use request::{Upload, FormData, FormField};

mod uri;
pub use uri::{Uri, UriBuf, UriAndParams};

mod response;
pub use response::{Response, Error, ContentData, Render};

pub type Result<T=()> = std::result::Result<T, Error>;

pub enum Until
{
    CrLf,
    Eof,
    Multipart
}

#[async_trait]
pub trait HttpConnection: Send
{
    fn set_multipart_boundary(&mut self, boundary: &[u8]);

    fn set_byte_limit(&mut self, limit: Option<usize>);

    async fn read_line(&mut self, until: Until)
        -> http::Result<Option<Cow<str>>>;

    async fn read_content<D>(&mut self, dst: &mut D, until: Until)
        -> http::Result<Option<usize>>
        where D: io::AsyncWrite + Unpin + Send;

    async fn write_line(&mut self, line: &str) -> io::Result<()>;

    async fn write_content<S>(&mut self, src: S) -> io::Result<()>
        where S: io::AsyncRead + Unpin + Send;

    fn get_addr(&self) -> SocketAddr;
}
