use super::*;

mod alternatives;
pub(crate) use alternatives::{Alternatives, Quality, choose_alternative};

mod method;
pub use method::{Method, MethodSet};

mod multipart;
use multipart::{Multipart, MultipartBody};

mod upload;
pub(crate) use upload::{Upload, UploadFile};

mod body;
use body::Body;

mod form;
pub(crate) use form::{FormData, FormField, MultipartForm};

/// A structure containing information pertaining to an HTTP request.
pub struct Request<'a, Conn=conn::Connection>
{
    /// The request method.
    method:         Method,
    /// The parsed URI, along with URI params and trailing slash.
    uri:            UriAndParams,
    /// The address that made this request.
    addr:           SocketAddr,
    /// The headers sent with this request.
    headers:        header::Map,
    /// The HTTP connection that made this request.
    ///
    /// This can be used to get a request body if one is required.
    connection:     &'a mut Conn,
    /// The headers that the response was varied according to.
    ///
    /// This is used to populate the `Vary` response header.
    varied:         std::collections::HashSet<header::Name>
}

impl<'a, Conn> Request<'a, Conn>
    where Conn: HttpConnection
{
    fn new(
        method:         Method,
        uri:            UriAndParams,
        addr:           SocketAddr,
        headers:        header::Map,
        connection:     &'a mut Conn
    )
        -> Self
    {
        Self
        {
            method, uri, addr, headers, connection,
            varied: Default::default()
        }
    }

    /// Get the HTTP method of this request.
    pub fn method(&self) -> Method { self.method }

    pub fn expect_method(&self, methods: impl Into<MethodSet>) -> Result
    {
        let set = methods.into();
        if !set.contains(self.method)
        {
            Err(http::Error::new_method_not_allowed(set))
        }
        else
        {
            Ok(())
        }
    }

    /// Get the absolute parsed URI of the request.
    ///
    /// Parameters and trailing slash are not included in this URI.
    pub fn full_uri(&self) -> &Uri { &self.uri.uri }

    /// Get absolute URI and parameters of the request.
    ///
    /// Parameters and trailing slash are included in this URI.
    pub fn full_uri_and_params(&self) -> &UriAndParams { &self.uri }

    /// Get the value of a URI parameter.
    pub fn uri_param(&self, name: &str) -> Option<&str>
    {
        self.uri.params.get(name).map(|s| s.as_str())
    }

    /// Get the port of this request.
    pub fn port(&self) -> u16 { self.addr.port() }

    /// Get the IP of this request.
    pub fn ip(&self) -> IpAddr { self.addr.ip() }

    /// Get a map of the HTTP headers of this request
    pub fn headers(&self) -> &header::Map { &self.headers }

    /// If this HTTP request has an attached body, get its content-length.
    pub fn body_len(&self) -> Option<usize>
    {
        body_len_impl(&self.method, &self.headers)
    }

    pub(crate) fn body<'b>(&'b mut self) -> Result<Body<'a, 'b, Conn>>
    {
        Body::new(self)
    }

    pub(crate) fn multipart<'b>(&'b mut self)
        -> Result<Multipart<'a, 'b, Conn>>
    {
        Multipart::new(self)
    }

    pub(crate) fn multipart_form<'b>(&'b mut self)
        -> Result<MultipartForm<'a, 'b, Conn>>
    {
        MultipartForm::new(self)
    }

    pub(crate) fn alternatives<'b, Tag>(&'b mut self)
        -> Alternatives<'a, 'b, Conn, Tag>
        where Tag: Clone + PartialEq
    {
        Alternatives::new(self)
    }

    /// Record a header in this request as varying the response.
    ///
    /// This header can then be sent in the `Vary` response header to inform
    /// caches that the header is relevant.
    ///
    pub(crate) fn vary<'b, H>(&mut self) where H: Header<'b>
    {
        self.varied.insert(H::name());
    }

    /// Consume and discard the body if it has not been read.
    ///
    /// This is necessary if we're going to reuse the connection.
    pub async fn consume_unread_body(&mut self) -> Result
    {
        if self.body_len().is_some()
        {
            if let Some(received) = self.connection
                .read_content(&mut io::sink(), Until::Eof).await?
            {
                warn!("Dumped {} bytes of unread body.", received);
            }
        }

        Ok(())
    }

    /// Add any reponse headers from this request.
    pub(crate) fn add_response_headers(&self, response: &mut header::Map)
    {
        // Add the Vary header.
        if self.varied.len() > 0
        {
            response.add_default(
                &header::Vary::from(self.varied.iter().cloned())
            )
                // Ignore errors here
                .ok();
        }
    }

}

fn body_len_impl(method: &Method, headers: &header::Map) -> Option<usize>
{
    if method.can_have_request_body()
    {
        headers.decode::<header::ContentLen>().map(|head| head.0)
    }
    else
    {
        None
    }
}

pub async fn receive_request<'a, Conn>(connection: &'a mut Conn)
    -> Result<Request<'a, Conn>>
    where Conn: HttpConnection
{
    // Headers can be arbitrarily long
    connection.set_byte_limit(None);

    // Read and parse the initial request line.
    let addr = connection.get_addr();
    let (method, uri) = receive_request_line(connection).await?;

    info!("<- {} {} {:?}", method, uri, addr);

    // Read all HTTP headers
    let headers = receive_headers(connection).await?;

    // Set the max byte count based on the content-len.
    connection.set_byte_limit(body_len_impl(&method, &headers));

    Ok(Request::new(method, uri, addr, headers, connection))
}

async fn receive_headers(connection: &mut impl HttpConnection)
    -> Result<header::Map>
{
    let mut map = header::Map::default();

    // Read any headers from the request.
    'read_headers: while let Some(header_line)
        = connection.read_line(Until::CrLf).await?
    {
        // If we encounter a zero length header, the headers are over.
        if header_line.len() == 0
        {
            break 'read_headers;
        }

        if let Some((name, val)) = header_line.split_once(':')
        {
            debug!("<- Header {:?}: {:?}", name.trim(), val.trim());

            match (name.parse(), val.parse())
            {
                (Ok(name), Ok(val)) => map.insert(name, val),
                (maybe_name, maybe_val) =>
                {
                    if let Err(e) = maybe_name
                    {
                        warn!("Error parsing header name {:?}: {:?}", name, e);
                    }
                    if let Err(e) = maybe_val
                    {
                        warn!("Error parsing header value {:?}: {:?}", val, e);
                    }
                }
            }
        }
        else
        {
            return Err(Error::new_bad_request("No colon in HTTP header line"));
        }
    }

    Ok(map)
}

async fn receive_request_line(connection: &mut impl HttpConnection)
    -> Result<(Method, UriAndParams)>
{
    let Some(request_line) = connection.read_line(Until::CrLf).await?
        else { return Err(Error::new_bad_request("No request line")); };

    let mut words = request_line.trim().split_whitespace();

    let Some(method) = words.next() else
        { return Err(Error::new_bad_request("No method in request line")); };

    let Some(uri) = words.next() else
        { return Err(Error::new_bad_request("No URI in request line")); };

    // TODO: Worry about version
    let Some(_version) = words.next() else
        { return Err(Error::new_bad_request("No version in request line")); };

    Ok((method.parse()?, UriAndParams::decode(uri)?))
}

