use super::*;

/// Data uploaded to the server.
#[async_trait]
pub(crate) trait Upload: Send + Unpin
{
    async fn read_body<D>(self, dst: &mut D) -> Result<usize>
        where D: io::AsyncWrite + Unpin + Send;

    async fn into_vec(mut self) -> Result<Vec<u8>>
        where Self: Sized
    {
        let mut rtn = Vec::new();

        self.read_body(&mut rtn).await?;

        Ok(rtn)
    }

    async fn into_string(mut self) -> Result<String>
        where Self: Sized
    {
        // TODO: Respect encoding rather than just using UTF-8!
        let rtn = String::from_utf8(self.into_vec().await?)?;

        Ok(rtn)
    }
}

/// A file uploaded to the server.
pub(crate) trait UploadFile: Upload
{
    fn headers(&self) -> &header::Map;
}
