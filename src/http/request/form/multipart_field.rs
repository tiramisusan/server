use super::*;

pub(crate) struct MultipartFormField<'a, 'b, 'c, Conn>
{
    body: MultipartBody<'a, 'b, 'c, Conn>,
    name: String
}

impl<'a, 'b, 'c, Conn> MultipartFormField<'a, 'b, 'c, Conn>
{
    pub fn new(body: MultipartBody<'a, 'b, 'c, Conn>) -> Result<Self>
        where Conn: HttpConnection
    {
        if let Some(name) = body.headers()
            .decode_or_error::<header::ContentDisposition>()?
            .name()
            .map(ToOwned::to_owned)
        {
            Ok(Self { body, name })
        }
        else
        {
            Err(Error::new_bad_request("Name missing in form-data."))
        }
    }
}

#[async_trait]
impl<'a, 'b, 'c, Conn> Upload for MultipartFormField<'a, 'b, 'c, Conn>
    where Conn: HttpConnection
{
    async fn read_body<D>(mut self, dst: &mut D) -> Result<usize>
        where D: io::AsyncWrite + Unpin + Send
    {
        self.body.read_body(dst).await
    }
}

impl<'a, 'b, 'c, Conn> UploadFile for MultipartFormField<'a, 'b, 'c, Conn>
    where Conn: HttpConnection
{
    fn headers(&self) -> &header::Map { self.body.headers() }
}

impl<'a, 'b, 'c, Conn> FormField for MultipartFormField<'a, 'b, 'c, Conn>
    where Conn: HttpConnection
{
    fn field_name(&self) -> &str { &self.name }

    fn media_type(&self) -> Option<header::MediaType>
    {
        self.body.headers()
            .decode::<header::ContentType>()
            .map(|head| head.media_type())
    }

    fn filename(&self) -> Option<String>
    {
        self.body.headers()
            .decode::<header::ContentDisposition>()
            .and_then(|head| head.filename().map(ToOwned::to_owned))
    }
}
