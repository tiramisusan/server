use super::*;

pub(crate) struct MultipartForm<'a, 'b, Conn>(Multipart<'a, 'b, Conn>);

impl<'a, 'b, Conn> MultipartForm<'a, 'b, Conn>
    where Conn: HttpConnection
{
    pub fn new(req: &'b mut Request<'a, Conn>) -> Result<Self>
    {
        check_request(req)?;

        Ok(Self(req.multipart()?))
    }
}

#[async_trait]
impl<'a, 'b, 'c, Conn> FormData<'c> for MultipartForm<'a, 'b, Conn>
    where Conn: HttpConnection, 'a: 'b, 'b: 'c
{
    type Field = MultipartFormField<'a, 'b, 'c, Conn>;

    async fn get_next(&'c mut self) -> Result<Option<Self::Field>>
    {
        match self.0.get_next().await?
        {
            Some(body) => Ok(Some(MultipartFormField::new(body)?)),
            None       => Ok(None)
        }
    }
}

fn check_request<Conn>(req: &Request<'_, Conn>) -> Result
{
    // We assume that the request has already been checked to be multipart in
    // general, and we just need to check that it's a form-data.
    let cnt = req.headers.decode_or_error::<header::ContentType>()?;

    if cnt.media_type().get_subtype() != "form-data"
    {
        return Err(Error::new_bad_request(
            format!("Expected form-data, but found {:?}", cnt.media_type())
        ));
    }

    Ok(())
}
