use super::*;

mod multipart_form;
pub(crate) use multipart_form::MultipartForm;

mod multipart_field;
pub(crate) use multipart_field::MultipartFormField;

#[async_trait]
pub(crate) trait FormField: Upload
{
    fn field_name(&self) -> &str;

    fn media_type(&self) -> Option<header::MediaType>;

    fn filename(&self) -> Option<String>;
}

#[async_trait]
pub(crate) trait FormData<'b>
{
    type Field: FormField + 'b;

    async fn get_next(&'b mut self) -> Result<Option<Self::Field>>;
}
