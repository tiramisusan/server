use super::*;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug)]
pub(crate) enum Quality
{
    NoMatch,        // The resource is definitely not a match.
    Unknown,        // It is not known whether the resource might be a match.
    Match(u32, i32) // The resource matches, with a quality between 0 and
                    // u32::MAX, and a nudge value.
}

impl std::ops::Add for Quality
{
    type Output = Quality;

    fn add(self, rhs: Quality) -> Quality
    {
        match (self, rhs)
        {
            (Quality::NoMatch, _) | (_, Quality::NoMatch) =>
                Quality::NoMatch,
            (Quality::Unknown, _) | (_, Quality::Unknown) =>
                Quality::Unknown,
            (Quality::Match(q1, n1), Quality::Match(q2, n2)) =>
            {
                /* Calculate the quality as the product of the two qualities, *
                 * if they were scaled to be between 0 and 1.                 */
                let q3 = (q1 as u64) * (q2 as u64);
                let upper_word = (q3 >> 32) as u32;       /* Divide by 2^32 */
                let rounding   = ((q3 >> 31) & 1) as u32; /* Round up from .5 */

                Quality::Match(upper_word + rounding, n1 + n2)
            }
        }
    }
}

impl From<f32> for Quality
{
    fn from(f: f32) -> Quality
    {
        if f <= 0.0
        {
            Quality::NoMatch
        }
        else
        {
            /* Clamp quality between 0 and 1 */
            let clamped = (f as f64).clamp(0.0, 1.0);

            Quality::Match((clamped * (u32::MAX as f64)).round() as u32, 0)
        }
    }
}

impl Quality
{
    pub const fn new_max() -> Self { Quality::Match(u32::MAX, 0) }

    pub fn nudge(self, n: i32) -> Quality
    {
        match self
        {
            Quality::Match(q, n_old) => Quality::Match(q, n_old + n),
            oth => oth
        }
    }

    pub fn for_uri_param<'a, Conn>(
        maybe_name: Option<&'_ str>,
        req:        &mut Request<'a, Conn>
    )
        -> Self
        where Conn: HttpConnection
    {
        let Some(name) = maybe_name else { return Quality::Unknown; };

        if req.uri_param(name).is_some()
        {
            Quality::new_max()
        }
        else
        {
            Quality::NoMatch
        }
    }

    pub fn for_media_type<'a, Conn>(
        maybe_media: Option<header::MediaType<'_>>,
        req:         &mut Request<'a, Conn>
    )
        -> Self
        where Conn: HttpConnection
    {
        let Some(media) = maybe_media else { return Quality::Unknown; };
        let Some(accept) = req.headers().decode::<header::Accept>()
            else { return Quality::Unknown };

        match accept.quality(media)
        {
            Some(q) => Quality::from(q),
            None    => Quality::NoMatch
        }
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn test_quality_ord()
    {
        assert!(Quality::Unknown > Quality::NoMatch);
        assert!(Quality::Match(0, 0) > Quality::Unknown);
        assert!(Quality::Match(0, 1) > Quality::Match(0, 0));
        assert!(Quality::Match(1, 0) > Quality::Match(0, 1));
        assert!(Quality::new_max() > Quality::Match(1, 0));
    }

    #[test]
    fn test_quality_from_f32()
    {
        assert_eq!(Quality::from(1.0),  Quality::new_max());
        assert_eq!(Quality::from(0.5),  Quality::Match((u32::MAX / 2) + 1, 0));
        assert_eq!(Quality::from(0.25), Quality::Match((u32::MAX / 4) + 1, 0));
    }

    #[test]
    fn test_quality_clamping()
    {
        // Test clamping
        assert_eq!(Quality::from(-5.0), Quality::NoMatch);
        assert_eq!(Quality::from(5.0), Quality::new_max());
    }

    #[test]
    fn test_quality_add()
    {
        assert_eq!(
            Quality::from(0.75) + Quality::from(0.25),
            Quality::from(0.75 * 0.25)
        );
        assert_eq!(Quality::new_max() + Quality::Unknown, Quality::Unknown);
        assert_eq!(Quality::new_max() + Quality::NoMatch, Quality::NoMatch);
        assert_eq!(Quality::Unknown + Quality::new_max(), Quality::Unknown);
        assert_eq!(Quality::NoMatch + Quality::new_max(), Quality::NoMatch);
    }

    #[test]
    fn test_quality_nudge()
    {
        assert!(Quality::from(0.5).nudge(1)  > Quality::from(0.5));
        assert!(Quality::from(0.5).nudge(1)  < Quality::from(0.51));

        assert!(Quality::from(0.5).nudge(-1) < Quality::from(0.5));
        assert!(Quality::from(0.5).nudge(-1) > Quality::from(0.49));

        assert!(Quality::new_max().nudge(1) > Quality::new_max());
        assert!(Quality::new_max().nudge(-1) < Quality::new_max());
    }

    #[test]
    fn test_quality_zero()
    {
        assert_eq!(Quality::from(0.0), Quality::NoMatch)
    }
}
