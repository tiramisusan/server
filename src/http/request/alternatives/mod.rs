use super::*;

mod dimension;
use dimension::Dimension;

mod quality;
pub(crate) use quality::Quality;

mod choice;
use choice::Choice;

mod macros;
pub(crate) use macros::choose_alternative;

/// A structure for choosing between alternatives.
///
/// The algorithm is as follows:
///
///
pub(crate) struct Alternatives<'a, 'b, Conn, Tag>
{
    req:      &'b mut Request<'a, Conn>,
    choices:  Vec<Choice<Tag>>,
    strict:   bool
}

impl<'a, 'b, Conn, Tag> Alternatives<'a, 'b, Conn, Tag>
    where Conn: HttpConnection, Tag: Clone + PartialEq
{
    pub(super) fn new(req: &'b mut Request<'a, Conn>) -> Self
    {
        Self
        {
            req,
            choices: Default::default(),
            strict:  false
        }
    }

    pub fn add_choice(&mut self, tag: Tag) -> &mut Choice<Tag>
    {
        if self.choices.iter().any(|cmp| *cmp.tag() == tag)
        {
            panic!("Duplicate choice!");
        }

        self.choices.push(Choice::from(tag));
        self.choices.last_mut().unwrap()
    }

    #[allow(dead_code)]
    pub fn set_strict(&mut self, strict: bool)
    {
        self.strict = strict;
    }

    pub fn choose(self) -> http::Result<Tag>
    {
        if self.choices.len() == 0
        {
            return Err(From::from("No alternative choices found."));
        }

        let mut current: Vec<Quality> =
            vec![Quality::new_max(); self.choices.len()];

        for dimension in Dimension::iter_all()
        {
            /* Consider only the dimensions that the choices care about */
            if !self.choices.iter().any(|choice| choice.cares_about(dimension))
            {
                continue;
            }

            /* Vary the request to this dimension */
            dimension.vary(self.req);

            /* Calculate the quality of each choice after evaluating this *
             * dimension.                                                 */
            let next: Vec<Quality> =
                std::iter::zip(self.choices.iter(), current.iter())
                    .map(|(ch, q)| *q + ch.get_quality(dimension, self.req))
                    .collect();

            /* Get a vector of references to the old quality, and the new  *
             * quality for each choice. The old can be updated to the new. */
            let mut to_update = std::iter::zip(current.iter_mut(), next.iter())
                .collect::<Vec<_>>();

            /* If evaluating this dimension did not produce any match ... */
            if next.iter().max().unwrap() <= &Quality::Unknown
            {
                if self.strict
                {
                    /* In strict mode, only update choices that result in a *
                     * definite no-match.                                   */
                    to_update.retain(|(_, new)| **new == Quality::NoMatch);
                }
                else
                {
                    /* In non-strict mode, discard the entire dimension */
                    to_update.drain(..);
                }
            }

            for (old, new) in to_update
            {
                *old = *new;
            }
        }

        /* Find the best quality */
        let best_quality = *current.iter().max().unwrap();

        /* The best choice is the first choice that has the best quality */
        let best_choice = std::iter::zip(self.choices.iter(), current.iter())
            .find(|(_, quality)| **quality == best_quality)
            .unwrap().0;

        /* If we definitely have no matches, return an error.                 *
         *                                                                    *
         * Note that this can only happen in strict mode, since in non-strict *
         * mode, dimensions are not considered if they don't produce a match. */
        if best_quality == Quality::NoMatch
        {
            return Err(http::Error::NotAcceptable);
        }

        Ok(best_choice.tag().clone())
    }
}
