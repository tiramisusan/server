use super::*;

#[derive(Copy, Clone, Debug)]
pub(crate) enum Dimension
{
    UriParam,
    MediaType
}

impl Dimension
{
    pub fn vary<'a, Conn>(&self, req: &mut Request<'a, Conn>)
        where Conn: HttpConnection
    {
        match self
        {
            Dimension::UriParam  => {},
            Dimension::MediaType => req.vary::<header::Accept>()
        }
    }

    /// Iterate all dimensions.
    ///
    /// More important dimensions are considered first.
    pub fn iter_all() -> impl Iterator<Item=Dimension>
    {
        const DIMS: &'static [Dimension] = &[
            Dimension::UriParam,
            Dimension::MediaType
        ];

        DIMS.iter().cloned()
    }
}
