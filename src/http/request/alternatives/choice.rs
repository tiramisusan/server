use super::*;

pub(crate) struct Choice<Tag>
{
    tag:        Tag,
    uri_param:  Option<&'static str>,
    media_type: Option<header::MediaType<'static>>
}

impl<Tag> From<Tag> for Choice<Tag>
{
    fn from(tag: Tag) -> Self
    {
        Self { tag, uri_param: None, media_type: None }
    }
}

impl<Tag> Choice<Tag>
{
    pub(super) const fn tag(&self) -> &Tag { &self.tag }

    pub fn media_type(
        &mut self,
        media_type: header::MediaType<'static>
    )
        -> &mut Self
    {
        self.media_type = Some(media_type);
        self
    }

    pub fn uri_param(&mut self, param: &'static str) -> &mut Self
    {
        self.uri_param = Some(param);
        self
    }

    pub(super) fn cares_about(&self, dimension: Dimension) -> bool
    {
        match dimension
        {
            Dimension::UriParam  => self.uri_param.is_some(),
            Dimension::MediaType => self.media_type.is_some()
        }
    }

    pub(super) fn get_quality<'a, Conn>(
        &self,
        dimension: Dimension,
        req:       &mut Request<'a, Conn>
    )
        -> Quality
        where Conn: HttpConnection
    {
        match dimension
        {
            Dimension::UriParam =>
                Quality::for_uri_param(self.uri_param, req),
            Dimension::MediaType =>
                Quality::for_media_type(self.media_type, req)
        }
    }
}

