use super::*;

macro_rules! choose_alternative
{
    {
        request=$req:expr,
        $( $enum:ident ( $($name:ident $args:tt),* ) => $result:expr $(,)? )+
    }
        =>
    {{
        let request = $req;

        #[derive(Clone, PartialEq)]
        enum TagEnum { $($enum),+ }

        let mut alts = request.alternatives();

        $( alts.add_choice(TagEnum::$enum) $( . $name $args )*; )+

        match alts.choose()
        {
            $( Ok(TagEnum::$enum) => { $result } )+
            Err(e) => Err(e)
        }
    }}
}

pub(crate) use choose_alternative;
