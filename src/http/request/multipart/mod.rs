use super::*;

mod multipart_body;
pub(crate) use multipart_body::MultipartBody;

pub enum State
{
    /// The preamble of the multipart.
    Preamble,
    /// The start of a part.
    ///
    /// If this is "--", then this is the end, and if this is "\r\n", this is
    /// another part. Anything else is an error.
    Start,
    /// The start of the first header of a part.
    Headers,
    /// The start of the body of a part.
    Body,
    /// The end of the multipart.
    End
}

pub(crate) struct Multipart<'a, 'b, Conn>
{
    req:      &'b mut Request<'a, Conn>,
    state:    State,
    boundary: String
}

impl<'a, 'b, Conn> Multipart<'a, 'b, Conn>
    where Conn: HttpConnection
{
    pub fn new(req: &'b mut Request<'a, Conn>) -> Result<Self>
    {
        let boundary = check_request_and_get_boundary(req)?;

        req.connection.set_multipart_boundary(&boundary.as_bytes()[2..]);

        Ok(Self { req, state: State::Preamble, boundary })
    }

    pub async fn get_next<'c>(&'c mut self)
        -> Result<Option<MultipartBody<'a, 'b, 'c, Conn>>>
    {
        loop
        {
            match &self.state
            {
                State::Preamble =>
                {
                    self.receive_preamble().await?;
                }
                State::Start    =>
                {
                    self.receive_start().await?;
                }
                State::Headers  =>
                {
                    return Ok(Some(MultipartBody::new(
                        self.receive_headers().await?,
                        self
                    )));
                }
                State::Body =>
                {
                    self.receive_body(&mut io::sink()).await?;
                }
                State::End =>
                {
                    return Ok(None);
                }
            }
        }
    }

    async fn receive_preamble(&mut self) -> Result
    {
        debug!("Receiving multipart preamble");

        // Read multipart preamble
        self.req.connection
            .read_content(&mut io::sink(), Until::Multipart).await?;

        self.req.connection.set_multipart_boundary(self.boundary.as_bytes());

        self.state = State::Start;

        Ok(())
    }

    async fn receive_start(&mut self) -> Result
    {
        debug!("Receiving multipart start");

        match self.req.connection.read_line(Until::CrLf).await?
        {
            // The boundary is followed by \r\n immediately.
            Some(line) if line.len() == 0 =>
            {
                self.state = State::Headers;
                Ok(())
            }
            // The boundary is followed by -- immediately.
            //
            // This is the end of the multipart.
            Some(line) if line.starts_with("--") =>
            {
                self.state = State::End;
                Ok(())
            }
            _ =>
            {
                self.state = State::End;
                Err(Error::new_bad_request(
                    "Invalid multipart boundary - expected CRLF or --"
                ))
            }
        }
    }

    async fn receive_headers(&mut self) -> Result<header::Map>
    {
        debug!("Receiving multipart headers");

        self.state = State::Body;
        receive_headers(self.req.connection).await
    }

    async fn receive_body<D>(&mut self, dst: &mut D) -> Result<usize>
        where D:io::AsyncWrite + Unpin + Send
    {
        debug!("Receiving multipart body");

        let nbytes = self.req.connection
            .read_content(dst, Until::Multipart).await?
            .unwrap_or(0);

        self.state = State::Start;

        Ok(nbytes)
    }
}

fn check_request_and_get_boundary<Conn>(req: &Request<'_, Conn>)
    -> Result<String>
{
    // Check the request type is okay
    if !req.method.can_have_request_body()
    {
        return Err(Error::from(
            format!("Tried to get multipart from {:?} request.", req.method)
        ));
    }

    let cnt = req.headers.decode_or_error::<header::ContentType>()?;

    if !cnt.media_type().is_multipart()
    {
        return Err(Error::new_bad_request(
            format!("Expected multipart body, but found {:?}", cnt.media_type())
        ));
    }

    let Some(boundary) = cnt.boundary()
        else
        {
            return Err(Error::new_bad_request(
                "Expected boundary parameter in multipart request."
            ))
        };

    Ok("\r\n--".to_owned() + boundary)
}

#[cfg(test)]
mod test
{
    use super::*;

    async fn expect_form_part(
        name:      &str,
        multipart: &mut Multipart<'_, '_, conn::MockConnection>
    )
        -> String
    {
        info!("Expecting form part {:?}", name);

        // Receive the next form part.
        let part = multipart.get_next().await.unwrap().unwrap();

        // Check the content-disposition header.
        let dispos = part.headers()
            .decode::<header::ContentDisposition>().unwrap();
        assert_eq!(dispos.dispos(), header::DispositionType::FormData);
        assert_eq!(dispos.name().unwrap(), name);

        part.into_string().await.unwrap()
    }

    #[tokio::test]
    async fn test_http_multipart()
    {
        let mut conn = conn::MockConnection::new_mock(
            b"POST / HTTP/1.0\r\n\
            Content-Type: multipart/form-data; boundary=\"<BOUNDARY>\"\r\n\
            \r\n\
            Preamble! This should be ignored.\r\n\
            \r\n\
            --<BOUNDARY>\r\n\
            Content-Disposition: form-data; name=part1\r\n\
            \r\n\
            This is part 1.\r\n\
            --<BOUNDARY>\r\n\
            Content-Disposition: form-data; name=part2\r\n\
            \r\n\
            This is part 2.\r\n\
            --<BOUNDARY>--\r\n\
            Postamble! This should be ignored.\r\n"
        );

        let mut req = receive_request(&mut conn).await.unwrap();
        let mut multipart = req.multipart().unwrap();

        assert_eq!(
            expect_form_part("part1", &mut multipart).await,
            "This is part 1."
        );
        assert_eq!(
            expect_form_part("part2", &mut multipart).await,
            "This is part 2."
        );

        assert!(multipart.get_next().await.unwrap().is_none());
    }

    #[tokio::test]
    async fn test_http_multipart_empty_preamble()
    {
        let mut conn = conn::MockConnection::new_mock(
            b"POST / HTTP/1.0\r\n\
            Content-Type: multipart/form-data; boundary=\"<BOUNDARY>\"\r\n\
            \r\n\
            --<BOUNDARY>\r\n\
            Content-Disposition: form-data; name=part1\r\n\
            \r\n\
            This is part 1.\r\n\
            --<BOUNDARY>--"
        );

        let mut req = receive_request(&mut conn).await.unwrap();
        let mut multipart = req.multipart().unwrap();

        assert_eq!(
            expect_form_part("part1", &mut multipart).await,
            "This is part 1."
        );

        assert!(multipart.get_next().await.unwrap().is_none());
    }
}
