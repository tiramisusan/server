use super::*;

pub(crate) struct MultipartBody<'a, 'b, 'c, Conn>
{
    headers:   header::Map,
    multipart: &'c mut Multipart<'a, 'b, Conn>
}

impl<'a, 'b, 'c, Conn> MultipartBody<'a, 'b, 'c, Conn>
{
    pub fn new(headers: header::Map, multipart: &'c mut Multipart<'a, 'b, Conn>)
        -> Self
    {
        Self { headers, multipart }
    }
}

#[async_trait]
impl<'a, 'b, 'c, Conn> Upload for MultipartBody<'a, 'b, 'c, Conn>
    where Conn: HttpConnection
{
    async fn read_body<D>(mut self, dst: &mut D) -> Result<usize>
        where D: io::AsyncWrite + Unpin + Send
    {
        self.multipart.receive_body(dst).await
    }
}

impl<'a, 'b, 'c, Conn> UploadFile for MultipartBody<'a, 'b, 'c, Conn>
    where Conn: HttpConnection
{
    fn headers(&self) -> &header::Map { &self.headers }
}

