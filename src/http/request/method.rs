use super::*;

use std::fmt;

pub type MethodSet = enumset::EnumSet<Method>;

#[derive(enumset::EnumSetType)]
pub enum Method
{
    Get,
    Put,
    Post,
    Patch,
    Delete,
    Brew,
    When
    // TODO: Connect
    // TODO: Head
    // TODO: Options
    // TODO: Trace
}

impl Method
{
    pub fn can_have_request_body(&self) -> bool
    {
        use Method::*;
        match self
        {
            Delete | Patch | Post | Put | Brew => true,
            Get | When => false
        }
    }
}

impl fmt::Debug for Method
{
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        use Method::*;
        fmt.write_str(
            match self
            {
                Get    => "GET",
                Put    => "PUT",
                Post   => "POST",
                Patch  => "PATCH",
                Delete => "DELETE",
                Brew   => "BREW",
                When   => "WHEN"
            }
        )
    }
}

impl fmt::Display for Method
{
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        <Self as fmt::Debug>::fmt(self, fmt)
    }
}

impl FromStr for Method
{
    type Err = Error;

    fn from_str(s: &str) -> Result<Self>
    {
        use Method::*;
        match s
        {
            "GET"    => Ok(Get),
            "PUT"    => Ok(Put),
            "POST"   => Ok(Post),
            "PATCH"  => Ok(Patch),
            "DELETE" => Ok(Delete),
            "BREW"   => Ok(Brew),
            "WHEN"   => Ok(When),
            _        => Err(Error::NotImplemented)
        }
    }
}

