use super::*;

pub(crate) struct Body<'a, 'b, Conn>(&'b mut Request<'a, Conn>);

impl<'a, 'b, Conn> Body<'a, 'b, Conn>
{
    pub(super) fn new(req: &'b mut Request<'a, Conn>) -> http::Result<Self>
    {
        check_request(req)?;

        Ok(Self(req))
    }
}

#[async_trait]
impl<'a, 'b, Conn> Upload for Body<'a, 'b, Conn>
    where Conn: HttpConnection
{
    async fn read_body<D>(mut self, dst: &mut D) -> Result<usize>
        where D: io::AsyncWrite + Unpin + Send
    {
        self.0.connection.read_content(dst, Until::Eof).await?
            .ok_or_else(|| Error::from("Expected body data but found none."))
    }
}

impl<'a, 'b, Conn> UploadFile for Body<'a, 'b, Conn>
    where Conn: HttpConnection
{
    fn headers(&self) -> &header::Map { &self.0.headers }
}

fn check_request<Conn>(req: &Request<'_, Conn>) -> Result
{
    // Check the request type is okay
    if !req.method.can_have_request_body()
    {
        return Err(Error::from(
            format!("Tried to get multipart from {:?} request.", req.method)
        ));
    }

    Ok(())
}
