use super::*;

/// A buffer for owning a decoded URI.
#[derive(Clone)]
pub struct UriBuf(String);

impl std::ops::Deref for UriBuf
{
    type Target = Uri;

    fn deref(&self) -> &Uri { uri_from_str(self.0.deref()) }
}

impl std::borrow::Borrow<Uri> for UriBuf
{
    fn borrow(&self) -> &Uri { &*self }
}

impl UriBuf
{
    pub fn decode(s: &str) -> Result<UriBuf>
    {
        let uri = coding::decode_str(s)?;
        Ok(UriBuf(uri.trim_matches('/').to_owned()))
    }
}

impl From<&Uri> for UriBuf
{
    fn from(uri: &Uri) -> UriBuf { UriBuf(uri.0.to_owned()) }
}
