use super::*;

fn parse_single_param(s: &str) -> Result<(String, String)>
{
    if let Some((name, val)) = s.split_once('=')
    {
        Ok((coding::decode_str(name)?, coding::decode_str(val)?))
    }
    else
    {
        Ok((coding::decode_str(s)?, String::new()))
    }
}

fn parse_params(s: &str) -> Result<HashMap<String, String>>
{
    let mut rtn = HashMap::new();

    // For all nonzero-length parameter strings
    for param in s.split('&').filter(|p| p.len() > 0)
    {
        let (key, val) = parse_single_param(param)?;

        if key.len() > 0
        {
            rtn.insert(key, val);
        }
    }

    Ok(rtn)
}

fn has_trailing_slash(s: &str) -> bool
{
    s.chars().rev().next() == Some('/')
}

/// A structure containing a decoded URI and its parameters.
#[derive(Clone)]
pub struct UriAndParams
{
    pub uri:            UriBuf,
    pub trailing_slash: bool,
    pub params:         HashMap<String, String>,
}

impl UriAndParams
{
    /// Parse a URI and its parameters from a string
    pub fn decode(s: &str) -> Result<UriAndParams>
    {
        let (uri_str, params_str) = s.split_once('?').unwrap_or((s, ""));

        let uri            = UriBuf::decode(uri_str)?;
        let trailing_slash = has_trailing_slash(uri_str);
        let params = parse_params(params_str)
            .unwrap_or_else(|_| HashMap::new());

        Ok(UriAndParams { uri, trailing_slash, params })
    }
}

impl std::fmt::Display for UriAndParams
{
    fn fmt(&self, out: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        use std::fmt::Write;

        write!(out, "{}", &*self.uri)?;
        if self.trailing_slash
        {
            out.write_char('/')?;
        }

        for (i, (k, v)) in self.params.iter().enumerate()
        {
            write!(
                out, "{sep}{k}={v}",
                sep=(if i == 0 { '?' } else { '&' }),
                k=uri_from_str(k),
                v=uri_from_str(v)
            )?;
        }

        Ok(())
    }
}

