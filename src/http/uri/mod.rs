use super::*;

use std::str::Chars;

mod coding;

mod uri_and_params;
pub use uri_and_params::UriAndParams;

mod uri_buf;
pub use uri_buf::UriBuf;

/// A type containing a decoded URI.
///
/// The URI has no leading or trailing slash.
///
#[repr(transparent)]
#[derive(Debug)]
pub struct Uri(str);

impl AsRef<std::ffi::OsStr> for Uri
{
    fn as_ref(&self) -> &std::ffi::OsStr { self.0.as_ref() }
}

impl AsRef<str> for Uri
{
    fn as_ref(&self) -> &str { self.0.as_ref() }
}

/// Construct a URI from the inner Path
fn uri_from_str<'a>(path: &'a str) -> &'a Uri
{
    unsafe { std::mem::transmute(path) }
}

impl ToOwned for Uri
{
    type Owned = UriBuf;
    fn to_owned(&self) -> UriBuf { UriBuf::from(self) }
}

impl Uri
{
    pub fn mounted_uri<'a>(&'a self, base: &'_ Uri) -> Option<&'a Self>
    {
        let path: &Path = self.0.as_ref();
        path.strip_prefix(&base.0).ok()
            .and_then(Path::to_str)
            .map(uri_from_str)
    }

    pub fn is_empty(&self) -> bool
    {
        let path: &Path = self.0.as_ref();
        path.components().next().is_none()
    }

    /// Get a filesystem path that this URI refers to.
    ///
    /// # Arguments
    ///
    ///  * `base` - The base filesystem path that the URI should be evaluated
    ///             relative to.
    ///
    pub fn fs_path(&self, base: impl AsRef<Path>) -> PathBuf
    {
        let mut rtn = PathBuf::new();

        rtn.push(base);
        if !self.is_empty()
        {
            rtn.push(&self.0);
        }

        rtn
    }
}

impl std::fmt::Display for Uri
{
    fn fmt(&self, out: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        coding::encode(out, &self.0)
    }
}
