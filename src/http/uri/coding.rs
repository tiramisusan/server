use super::*;

/// Consume a single hex digit and return its value.
fn decode_hex_digit(ch: char) -> Result<u8>
{
    match ch.to_digit(16)
    {
        Some(v) =>
            Ok(v as u8),
        None =>
            Err(Error::new_bad_request(
                format!("Unknown URI hex digit {:?}", ch)
            ))
    }
}

/// Consume a two-digit hex code and return the corresponding character.
///
/// Assumes the escape sequences are Latin-1 encoded.
///
fn expect_hex<'a>(ch: &mut Chars<'a>) -> Result<char>
{
    let upper = ch.next();
    let lower = ch.next();

    if let (Some(u), Some(l)) = (upper, lower)
    {
        Ok((decode_hex_digit(u)? * 16 + decode_hex_digit(l)?) as char)
    }
    else
    {
        Err(Error::new_bad_request("URI hex escape ended prematurely"))
    }
}

/// Decode and return a URI-encoded string.
pub fn decode_str(s: &str) -> Result<String>
{
    let mut chars  = s.chars();
    let mut string = String::new();

    while let Some(ch) = chars.next()
    {
        match ch
        {
            // Handle plusses
            '+' => string.push(' '),
            // Handle hex escape sequences
            '%' => string.push(expect_hex(&mut chars)?),
            // Handle normal characters
            _   => string.push(ch)
        }
    }

    Ok(string)
}

/// Format a byte in Latin-1 encoded URI coding.
fn encode_byte(out: &mut std::fmt::Formatter<'_>, b: u8)
    -> std::fmt::Result
{
    use std::fmt::Write;

    match b
    {
        b'a'..=b'z' | b'A'..=b'Z' | b'0'..=b'9' |
        b'-' | b'_' | b'.' | b'~' | b'/' =>
            out.write_char(b as char),
        b' ' =>
            out.write_char('+'),
        _ =>
            write!(out, "%{:02x}", b)
    }
}

/// Format a string as a Latin-1 encoded URI.
pub fn encode(out: &mut std::fmt::Formatter<'_>, uri: &str)
    -> std::fmt::Result
{
    for byte in uri.as_bytes()
    {
        encode_byte(out, *byte)?;
    }

    Ok(())
}
