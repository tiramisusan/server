use super::*;

use std::pin::Pin;

pub mod error;
pub use error::Error;

mod render;
pub use render::Render;

pub type ContentData = Pin<Box<dyn io::AsyncRead + Sync + Send>>;

pub struct Response
{
    status:  i32,
    message: &'static str,
    headers: header::Map,
    render:  Box<dyn Render + Sync + Send>
}

fn status_can_have_body(status: i32) -> bool
{
    match status
    {
        // Informational responses
        100..=199 => false,
        // No Content
        204       => false,
        // Not Modified
        304       => false,
        // Any other response can have a body
        _         => true
    }
}

fn build_error_response(e: Error) -> Result<Response>
{
    let (status, message) = e.status_code_and_message();

    let mut response = if status_can_have_body(status)
        {
            let page = html::make_error_page(&e);
            Response::new(page)?
        }
        else
        {
            Response::new(())?
        };

    response.set_status(status, message);

    e.add_headers(&mut response)?;
    e.cache_disposition().add_headers(&mut response)?;

    Ok(response)
}

impl From<http::Error> for Response
{
    fn from(err: Error) -> Self
    {
        build_error_response(err)
            .expect("Could not build error response")
    }
}

impl Response
{
    pub fn new<R>(render: R) -> Result<Self>
        where R: Render + Sync + Send + Sized + 'static
    {
        Self {
            status:  200,
            message: "OK",
            headers: Default::default(),
            render:  Box::new(render)
        }
            .with_header(&http::header::Server("Tiramisusan"))?
            .with_header(&http::header::Date::now())?
            .with_header(&http::header::Connection::Close)
    }

    pub fn is_ok(&self) -> bool { self.status == 200 }

    /// Whether this response is allowed to send an HTTP body.
    ///
    /// Some responses such as `304 Not Modified` are not allowed to send a
    /// body.
    pub fn can_have_body(&self) -> bool { status_can_have_body(self.status) }

    pub async fn render(
        &mut self,
        html_env: &html::Environment,
        mut req:  Option<Request<'_>>
    )
        -> ContentData
    {
        loop
        {
            let was_ok = self.is_ok();

            let renderable = std::mem::replace(
                &mut self.render,
                Box::new(String::new())
            );

            let content_data =
                renderable.render(self, html_env, req.as_mut()).await;

            match content_data
            {
                Ok(content) =>
                {
                    if let Some(r) = &req
                    {
                        r.add_response_headers(&mut self.headers);
                    }
                    return content;
                }
                Err(err) if was_ok =>
                {
                    warn!("Error rendering response: {:?}", err);

                    // Could not render 200 response
                    *self = Self::from(err);
                }
                Err(err) =>
                {
                    // Could not render non-200 response
                    //
                    // Do not try and render an error page to avoid recursion
                    panic!("Could not render error response: {}", err);
                }
            }
        }
    }

    pub async fn send(
        self,
        conn:     &mut impl HttpConnection,
        content:  ContentData
    )
        -> io::Result<()>
    {
        info!("-> {} {}", self.status, self.message);

        let status = format!("HTTP/1.1 {} {}", self.status, self.message);
        conn.write_line(&status).await?;

        for (name, value) in self.headers.iter()
        {
            debug!("-> Header {:?}: {:?}", name.as_str(), value.as_str());

            conn.write_line(&header::format_header(name, value)).await?;
        }

        conn.write_line("").await?;

        if self.can_have_body()
        {
            conn.write_content(content).await?;
        }

        Ok(())
    }

    pub fn headers(&mut self) -> &mut header::Map { &mut self.headers }

    pub fn with_header<'a>(mut self, head: &impl Header<'a>) -> Result<Self>
    {
        self.headers().add(head)?;
        Ok(self)
    }

    pub fn set_status(&mut self, status: i32, message: &'static str)
    {
        self.status  = status;
        self.message = message;
    }
}
