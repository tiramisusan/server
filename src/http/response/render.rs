use super::*;

use std::io::Cursor;

/// Something that can be rendered into content.
///
#[async_trait]
pub trait Render
{
    /// Render into data.
    ///
    /// Return content data, and a vector of default headers.
    async fn render<'a, 'b>(
        self:     Box<Self>,
        response: &mut Response,
        html_env: &html::Environment,
        req:      Option<&'b mut http::Request<'a>>
    )
        -> Result<ContentData>;
}

fn content_from_bytes(bytes: Vec<u8>) -> ContentData
{
    Box::pin(Cursor::new(bytes))
}

#[async_trait]
impl Render for ()
{
    async fn render<'a, 'b>(
        self:      Box<Self>,
        _response: &mut Response,
        _html_env: &html::Environment,
        _req:      Option<&'b mut http::Request<'a>>
    )
        -> Result<ContentData>
    {
        Ok(Box::pin(io::empty()))
    }
}

#[async_trait]
impl Render for Vec<u8>
{
    async fn render<'a, 'b>(
        self:      Box<Self>,
        response:  &mut Response,
        _html_env: &html::Environment,
        _req:      Option<&'b mut http::Request<'a>>
    )
        -> Result<ContentData>
    {
        use http::header::*;

        response.headers()
            .add_default(&ContentLen(self.len()))?
            .add_default(&ContentType::from(MediaType::data()))?;

        Ok(content_from_bytes(*self))
    }
}

#[async_trait]
impl Render for String
{
    async fn render<'a, 'b>(
        self:      Box<Self>,
        response:  &mut Response,
        _html_env: &html::Environment,
        _req:      Option<&'b mut http::Request<'a>>
    )
        -> Result<ContentData>
    {
        use http::header::*;

        response.headers()
            .add_default(&ContentLen(self.len()))?
            .add_default(&ContentType::from(MediaType::text()))?;

        Ok(content_from_bytes(self.into_bytes()))
    }
}

pub async fn file_len(file: &mut (impl io::AsyncSeek + Unpin))
    -> io::Result<usize>
{
    let original = file.stream_position().await?;
    let length   = file.seek(io::SeekFrom::End(0)).await?;

    file.seek(io::SeekFrom::Start(original)).await?;

    Ok(length as usize)
}

#[async_trait]
impl Render for tokio::fs::File
{
    async fn render<'a, 'b>(
        mut self:  Box<Self>,
        response:  &mut Response,
        _html_env: &html::Environment,
        _req:      Option<&'b mut http::Request<'a>>
    )
        -> Result<ContentData>
    {
        use http::header::*;

        let len = file_len(&mut self).await?;
        response.headers()
            .add_default(&ContentLen(len))?
            .add_default(&ContentType::from(MediaType::data()))?;

        Ok(Box::pin(self))
    }
}

#[async_trait]
impl Render for scratch::ReadFile<scratch::DefaultLock>
{
    async fn render<'a, 'b>(
        mut self:  Box<Self>,
        response:  &mut Response,
        _html_env: &html::Environment,
        _req:      Option<&'b mut http::Request<'a>>
    )
        -> Result<ContentData>
    {
        use http::header::*;

        let len = file_len(&mut self).await?;
        response.headers()
            .add_default(&ContentLen(len))?
            .add_default(&ContentType::from(MediaType::data()))?;

        Ok(Box::pin(self))
    }
}
