use super::*;

use endpoint::CacheDisposition;

#[derive(Debug)]
#[allow(unused)]
pub enum Error
{
    Moved(String),
    Found(String),
    NotModified(CacheDisposition),
    BadRequest(String),
    Forbidden,
    NotFound(PathBuf),
    MethodNotAllowed(MethodSet),
    NotAcceptable,
    RequestTimeout,
    LengthRequired,
    EntityTooLarge,
    IAmATeapot(String),
    InternalError(StdError),
    NotImplemented
}

impl Error
{
    pub fn status_code_and_message(&self) -> (i32, &'static str)
    {
        use Error::*;
        match self
        {
            Moved(_)            => (301, "Moved Permanently"),
            Found(_)            => (302, "Found"),
            NotModified(_)      => (304, "Not Modified"),
            BadRequest(_)       => (400, "Bad Request"),
            Forbidden           => (403, "Forbidden"),
            NotFound(_)         => (404, "Not Found"),
            MethodNotAllowed(_) => (405, "Method Not Allowed"),
            NotAcceptable       => (406, "Not Acceptable"),
            RequestTimeout      => (408, "Request Timeout"),
            LengthRequired      => (411, "Length Required"),
            EntityTooLarge      => (413, "Request Entity Too Large"),
            IAmATeapot(_)       => (418, "I'm a Teapot"),
            InternalError(_)    => (500, "Internal Server Error"),
            NotImplemented      => (501, "Not Implemented")
        }
    }

    pub fn add_headers(&self, response: &mut Response) -> Result
    {
        use Error::*;
        match self
        {
            Moved(loc) | Found(loc) =>
            {
                response.headers().add(&header::Location(loc))?;
            }
            NotModified(dispos) =>
            {
                dispos.add_headers(response)?;
            }
            MethodNotAllowed(allowed) =>
            {
                response.headers().add(&http::header::Allow(*allowed))?;
            }
            _ => {}
        }

        Ok(())
    }

    pub fn cache_disposition(&self) -> CacheDisposition
    {
        CacheDisposition::NoCache
    }

    pub fn new_not_found<P>(path: P) -> Self
        where P: AsRef<Path>
    {
        Error::NotFound(PathBuf::from(path.as_ref()))
    }

    pub fn new_bad_request<M>(msg: M) -> Self
        where String: From<M>
    {
        let s = String::from(msg);
        error!("Bad Request: {}", &s);
        Error::BadRequest(s)
    }

    pub fn new_method_not_allowed<M>(methods: M) -> Self
        where MethodSet: From<M>
    {
        Error::MethodNotAllowed(From::from(methods))
    }
}

impl std::fmt::Display for Error
{
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        match self
        {
            Error::InternalError(e)   => { format_error(e.as_ref(), fmt) }
            Error::IAmATeapot(teapot) => { write!(fmt, "{}\n", teapot) }
            Error::BadRequest(reason) => { write!(fmt, "{}\n", reason) }
            _ => <Self as std::fmt::Debug>::fmt(self, fmt)
        }
    }
}

impl<E> From<E> for Error where StdError: From<E>
{
    fn from(e: E) -> Error
    {
        let boxed: StdError = From::from(e);
        error!("Error:");
        error!("    {}", boxed.to_string());
        Self::InternalError(boxed)
    }
}

