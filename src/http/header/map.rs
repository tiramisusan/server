use super::*;

#[derive(Default, Clone)]
pub struct Map(HashMap<Name, Value>);

impl Map
{
    pub fn insert(&mut self, name: Name, value: Value)
    {
        if self.0.contains_key(&name)
        {
            // TODO: Handle duplicate headers!
            warn!("Unsupported duplicate header {:?}", name.as_str());
        }

        self.0.insert(name, value);
    }

    pub fn add_default<'a, H>(&mut self, head: &H) -> Result<&mut Self>
        where H: Header<'a>
    {
        let name = H::name();
        if !self.0.contains_key(&name)
        {
            let value = Value::try_from(head.encode_value())?;
            self.0.insert(name, value);
        }

        Ok(self)
    }

    pub fn add<'a, H>(&mut self, head: &H) -> Result<&mut Self>
        where H: Header<'a>
    {
        let name  = H::name();
        let value = Value::try_from(head.encode_value())?;

        self.0.insert(name, value);

        Ok(self)
    }

    pub fn iter<'a>(&'a self)
        -> impl Iterator<Item=(&'a Name, &'a Value)>
    {
        self.0.iter()
    }

    fn decode_impl<'a, H>(&'a self) -> Option<http::Result<H>>
        where H: Header<'a> + std::fmt::Debug
    {
        let name = H::name();

        let Some(value) = self.0.get(&name) else
            {
                debug!("Header {:?} not present in request", name.as_str());
                return None;
            };

        match H::decode_value(value.as_str())
        {
            Ok(decoded) =>
            {
                debug!("Decoded header {:?}: {:?}", name.as_str(), decoded);
                return Some(Ok(decoded));
            }
            Err(e) =>
            {
                warn!("Could not decode header {:?}: {}", name.as_str(), e);
                return Some(Err(e));
            }
        }
    }

    pub fn decode_or_error<'a, H>(&'a self) -> http::Result<H>
        where H: Header<'a> + std::fmt::Debug
    {
        self.decode_impl()
            .unwrap_or_else(|| Err(Error::new_bad_request(
                format!("Expected {:?} header.", H::name().as_str())
            )))
    }

    pub fn decode<'a, H>(&'a self) -> Option<H>
        where H: Header<'a> + std::fmt::Debug
    {
        self.decode_impl()?.ok()
    }
}
