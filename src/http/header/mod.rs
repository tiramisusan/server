use super::*;

mod name;
pub use name::Name;

mod value;
pub use value::Value;

mod map;
pub use map::Map;

mod types;
pub use types::*;

mod parse;
use parse::*;

pub trait Header<'a>
{
    fn name() -> Name;

    fn decode_value(s: &'a str) -> http::Result<Self>
        where Self: Sized;

    fn encode_value(&self) -> String;
}

pub fn format_header(name: &Name, value: &Value) -> String
{
    format!("{}: {}", name.as_str(), value.as_str())
}

#[cfg(test)]
fn test_reencode<'a, H>(value: H)
    where H: Header<'a> + PartialEq + std::fmt::Debug
{
    info!("Encoding {:?}", value);

    let encoded = value.encode_value();
    info!("Encoded as {:?}", encoded);

    let encoded_static: &'static String =
        unsafe { &*(&encoded as *const String) };

    let decoded = H::decode_value(&encoded_static)
        .expect("Could not decode header I just encoded");

    info!("Decoded {:?}", value);

    assert_eq!(value, decoded);
}
