use super::*;

#[derive(Clone)]
pub struct Value(String);

/// Check whether a character is not allowed in an header value.
fn invalid_char(ch: char) -> bool
{
    !(
        ch.is_alphanumeric() ||
        "_ :;.,\\/\"'?!(){}[]@<>=-+*#$&`|~^%".contains(ch)
    )
}

impl FromStr for Value
{
    type Err = Error;

    fn from_str(s: &str) -> Result<Value>
    {
        Value::try_from(String::from(s.trim_start()))
    }
}

impl TryFrom<String> for Value
{
    type Error = Error;

    fn try_from(mut s: String) -> Result<Value>
    {
        /* Don't allow whitespace at the start! */
        if s.starts_with(char::is_whitespace)
        {
            let new = String::from(s.trim_start());
            s = new;
        }

        // Check all the characters are valid
        if s.find(invalid_char).is_some()
        {
            Err(Error::from(
                format!("Invalid character in header value {:?}", s)
            ))
        }
        else
        {
            Ok(Value(s))
        }
    }
}

impl Value
{
    pub fn as_str(&self) -> &str { &self.0 }
}
