use super::*;

pub struct Optional<'a>(pub Cow<'a, str>, pub Option<Cow<'a, str>>);
pub struct KeyValue<'a>(pub Cow<'a, str>, pub Cow<'a, str>);
pub struct Token<'a>(pub Cow<'a, str>);

impl<'a> ListItem<'a> for Optional<'a>
{
    fn decode(buf: &mut ParseBuffer<'a>) -> http::Result<Self>
    {
        let key = to_lowercase(buf.expect_token()?);

        // TODO: Implement RFC 5987 here!
        //
        // We need to un-urlencode parameters whose names end in '*'

        if buf.maybe_expect('=')
        {
            if buf.maybe_expect('"')
            {
                Ok(Self(key, Some(buf.expect_quoted_string()?)))
            }
            else
            {
                Ok(Self(key, Some(From::from(buf.expect_token()?))))
            }
        }
        else
        {
            Ok(Self(key, None))
        }
    }
}

impl<'a> ListItem<'a> for KeyValue<'a>
{
    fn decode(buf: &mut ParseBuffer<'a>) -> http::Result<Self>
    {
        match Optional::decode(buf)?
        {
            Optional(token, None) =>
                Err(http::Error::new_bad_request(format!(
                    "Missing parameter for key '{}' in header.", token
                ))),
            Optional(key, Some(val)) =>
                Ok(KeyValue(key, val))
        }
    }
}

impl<'a> ListItem<'a> for Token<'a>
{
    fn decode(buf: &mut ParseBuffer<'a>) -> http::Result<Self>
    {
        match Optional::decode(buf)?
        {
            Optional(token, None) =>
                Ok(Token(token)),
            Optional(key, Some(_)) =>
                Err(http::Error::new_bad_request(format!(
                    "Unexpected parameter for key '{}' in header.", key
                )))
        }
    }
}
