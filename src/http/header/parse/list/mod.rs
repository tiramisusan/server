use super::*;

mod iter;
use iter::Iter;

mod test;

mod list_item;
pub(crate) use list_item::ListItem;

pub(crate) mod parameter;

pub(crate) struct List<'de>
{
    buf:    ParseBuffer<'de>,
    sep:    char,
    first:  bool
}

impl<'de> List<'de>
{
    pub fn new(string: &'de str) -> Self
    {
        Self { buf: From::from(string), sep: ';', first: true }
    }
}

impl<'de> List<'de>
{
    pub fn with_sep(mut self, sep: char) -> Self
    {
        self.sep = sep;
        self
    }

    pub fn iter<'lst, LItem>(&'lst mut self) -> Iter<'de, 'lst, LItem>
    {
        Iter::<LItem>::from(self)
    }

    pub fn get_next<LItem>(&mut self) -> http::Result<LItem>
        where LItem: ListItem<'de>
    {
        self.iter().next()
            .ok_or_else(|| http::Error::new_bad_request(
                "Expected item in header."
            ))?
    }

    pub fn next_string<'lst>(&'lst mut self)
        -> http::Result<Option<&'lst mut ParseBuffer<'de>>>
    {
        self.buf.consume_ows();

        if self.buf.remaining().len() > 0
        {
            if self.first
            {
                self.first = false;
            }
            else
            {
                self.buf.expect(self.sep)?;
                self.buf.consume_ows();
            }

            Ok(Some(&mut self.buf))
        }
        else
        {
            Ok(None)
        }
    }
}
