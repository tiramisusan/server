use super::*;

pub(crate) struct Iter<'de, 'lst, LItem>
{
    list:  &'lst mut List<'de>,
    _item: std::marker::PhantomData<LItem>
}

impl<'de, 'lst, LItem> From<&'lst mut List<'de>> for Iter<'de, 'lst, LItem>
{
    fn from(list: &'lst mut List<'de>) -> Iter<'de, 'lst, LItem>
    {
        Self { list, _item: Default::default() }
    }
}

impl<'de, 'lst, LItem> Iterator for Iter<'de, 'lst, LItem>
    where LItem: ListItem<'de>
{
    type Item=http::Result<LItem>;

    fn next(&mut self) -> Option<http::Result<LItem>>
    {
        match self.list.next_string()
        {
            Err(e)            => Some(Err(e)),
            Ok(None)          => None,
            Ok(Some(buf_ref)) => Some(LItem::decode(buf_ref))
        }
    }
}
