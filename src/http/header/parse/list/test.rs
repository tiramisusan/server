#[cfg(test)]
use super::*;

#[cfg(test)]
mod test_inner
{
    use super::*;

    #[test]
    fn test_parameter_optional()
    {
        let mut list = List::new("token; key=value");
        let mut iter = list.iter::<parameter::Optional>();

        let item = iter.next().unwrap().unwrap();
        assert_eq!(item.0, "token");
        assert!(&item.1.is_none());

        let item = iter.next().unwrap().unwrap();
        assert_eq!(item.0, "key");
        assert_eq!(&item.1.unwrap(), "value");

        assert!(iter.next().is_none());
    }

    #[test]
    fn test_parameter_key_value()
    {
        let mut list = List::new(r#"a=b ; c="d" ; e="\"f\"""#);
        let mut iter = list.iter::<parameter::KeyValue>();

        let item = iter.next().unwrap().unwrap();
        assert_eq!(item.0, "a");
        assert_eq!(&item.1, "b");

        let item = iter.next().unwrap().unwrap();
        assert_eq!(item.0, "c");
        assert_eq!(&item.1, "d");

        let item = iter.next().unwrap().unwrap();
        assert_eq!(item.0, "e");
        assert_eq!(&item.1, "\"f\"");

        assert!(iter.next().is_none());
    }

    #[test]
    fn test_parameter_token()
    {
        let mut list = List::new("a;b;c");
        let mut iter = list.iter::<parameter::Token>();

        assert_eq!(iter.next().unwrap().unwrap().0, "a");
        assert_eq!(iter.next().unwrap().unwrap().0, "b");
        assert_eq!(iter.next().unwrap().unwrap().0, "c");
        assert!(iter.next().is_none());
    }

}
