use super::*;

pub(crate) trait ListItem<'a>
{
    fn decode(buf: &mut ParseBuffer<'a>) -> http::Result<Self>
        where Self: Sized;

//    TODO: Encode
//    fn encode(&self, out: &mut String)

    fn decode_single(s: &'a str) -> http::Result<Self>
        where Self: Sized
    {
        let mut buf = ParseBuffer::from(s);

        buf.consume_ows();
        let rtn = Self::decode(&mut buf)?;
        buf.consume_ows();

        if buf.remaining().len() == 0
        {
            Ok(rtn)
        }
        else
        {
            Err(http::Error::new_bad_request(format!(
                "Unexpected trailing characters in header."
            )))
        }
    }
}
