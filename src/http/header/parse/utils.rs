use super::*;

/// A stopgap until `std::str::Pattern` is stable!
pub(crate) trait MyPattern
{
    fn do_strip_prefix(self, s: &str) -> Option<&str>;
}

macro_rules! impl_my_pattern
{
    ($type:ty) =>
    {
        impl MyPattern for $type
        {
            fn do_strip_prefix(self, s: &str) -> Option<&str>
            {
                s.strip_prefix(self)
            }
        }
    }
}

impl_my_pattern!(&str);
impl_my_pattern!(char);
impl_my_pattern!(&[char]);

/// Whether a character can be part of a header parameter token.
///
/// See RFC9110 S. 5.6.2
///
pub(crate) fn is_token(c: char) -> bool
{
    matches!(
        c,
        '!' | '#' | '$' | '%' | '&' | '\'' | '*' |
        '+' | '-' | '.' | '^' | '_' | '`'  | '|' | '~' |
        '0'..='9' | 'A'..='Z' | 'a'..='z'
    )
}

/// Whether is character is whitespace.
///
/// See RFC9110 S. 5.6.3
///
pub(crate) fn is_ws(c: char) -> bool
{
    matches!(c, ' ' | '\t')
}

pub(crate) fn to_lowercase<'a>(s: &'a str) -> Cow<'a, str>
{
    if s.chars().all(char::is_lowercase)
    {
        Cow::Borrowed(s)
    }
    else
    {
        Cow::Owned(s.to_lowercase())
    }
}

/// Add backslashes to a string.
///
/// Backslashes are inserted before any characters matching `to_escape`.
pub(crate) fn escape_string<'a>(s: &'a str, to_escape: impl Fn(char) -> bool)
    -> Cow<'a, str>
{
    if s.chars().any(|c| to_escape(c))
    {
        let mut string = String::new();
        for ch in s.chars()
        {
            if to_escape(ch)
            {
                string.push('\\')
            }

            string.push(ch);
        }

        Cow::Owned(string)
    }
    else
    {
        Cow::Borrowed(s)
    }
}

/// Resolve backslashes in a string.
///
/// Backslashes escape the byte following the backslash.
///
/// Return None if the string is not properly formed.
///
pub(crate) fn unescape_string<'a>(s: &'a str) -> Option<Cow<'a, str>>
{
    if !s.contains('\\')
    {
        Some(Cow::Borrowed(s))
    }
    else
    {
        let mut rtn     = String::new();
        let mut escaped = false;

        for c in s.chars()
        {
            match c
            {
                '\\' if !escaped => { escaped = true },
                other            => { escaped = false; rtn.push(other); }
            }
        }

        if escaped
        {
            None
        }
        else
        {
            Some(Cow::Owned(rtn))
        }
    }
}

pub(crate) fn parse_date(s: &str) -> http::Result<DateTime<Utc>>
{
    let utc = DateTime::parse_from_rfc2822(s)
        .map_err(|_| http::Error::new_bad_request("Could not parse date."))?
        .with_timezone(&Utc);

    Ok(utc)
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn test_escape_string()
    {
        assert_eq!(&*escape_string("hello", |_| true),        r#"\h\e\l\l\o"#);
        assert_eq!(&*escape_string("hello", |_| false),       r#"hello"#);
        assert_eq!(&*escape_string("hello", |ch| ch == 'e'),  r#"h\ello"#);
    }
}
