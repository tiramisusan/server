use super::*;

pub(crate) struct ParseBuffer<'a>(&'a str);

impl<'a> From<&'a str> for ParseBuffer<'a>
{
    fn from(s: &'a str) -> ParseBuffer<'a> { ParseBuffer(s) }
}

impl<'a> ParseBuffer<'a>
{
    pub fn remaining(&self) -> &'a str { self.0 }

    /// Expect and consume optional whitespace.
    ///
    /// See RFC9110 S. 5.6.3
    ///
    pub fn consume_ows(&mut self)
    {
        self.0 = self.0.trim_start_matches(is_ws);
    }


    /// If a specific pattern is next, consume it and return true.
    pub fn maybe_expect(&mut self, expect: impl MyPattern) -> bool
    {
        if let Some(remainder) = expect.do_strip_prefix(self.0)
        {
            self.0 = remainder;
            true
        }
        else
        {
            false
        }
    }

    /// Expect and consume a particular pattern.
    ///
    /// If the pattern is not found, return an error.
    ///
    pub fn expect<P>(&mut self, expect: P) -> http::Result
        where P: MyPattern + std::fmt::Debug + Copy
    {
        if self.maybe_expect(expect)
        {
            Ok(())
        }
        else
        {
            Err(http::Error::new_bad_request(
                format!("Expected {:?} in header value.", expect)
            ))
        }
    }

    /// Expect and consume a token.
    ///
    /// See RFC9110 S. 5.6.2
    ///
    pub fn expect_token(&mut self) -> http::Result<&'a str>
    {
        let token_len = self.0.chars().take_while(|&c| is_token(c)).count();

        if token_len == 0
        {
            Err(http::Error::new_bad_request("Expected token in header value."))
        }
        else
        {
            let (token, rest) = self.0.split_at(token_len);
            self.0 = rest;
            Ok(token)
        }
    }

    /// Expect and consume a quoted string.
    ///
    /// See RFC9110 S. 5.6.4
    ///
    pub fn expect_quoted_string(&mut self)
        -> http::Result<Cow<'a, str>>
    {
        for (index, _) in self.0.match_indices('"')
        {
            let (content, rest) = self.0.split_at(index);

            if let Some(rtn) = unescape_string(content)
            {
                self.0 = &rest[1..];
                return Ok(rtn);
            }
        }

        Err(http::Error::new_bad_request(
            "Unterminated quoted string in header."
        ))
    }

    /// Expect and consume a floating point number
    pub fn expect_float(&mut self) -> http::Result<f32>
    {
        let float_len = self.0.chars()
            .take_while(|&c| matches!(c, '0'..='9' | '.'))
            .count();

        if float_len == 0
        {
            Err(http::Error::new_bad_request(
                "Expected float in header value."
            ))
        }
        else
        {
            let (float, rest) = self.0.split_at(float_len);
            self.0 = rest;
            float.parse::<f32>()
                .map_err(
                    |_| http::Error::new_bad_request("Could not parse float.")
                )
        }
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn test_maybe_expect()
    {
        let mut buf = ParseBuffer::from("(tok)rest");

        assert!(buf.maybe_expect("(tok)"));
        assert_eq!(buf.remaining(), "rest");

        assert!(!buf.maybe_expect("(tok)"));
        assert_eq!(buf.remaining(), "rest");
    }

    #[test]
    fn test_expect()
    {
        let mut buf = ParseBuffer::from("(tok)rest");

        assert!(buf.expect("(tok)").is_ok());
        assert_eq!(buf.remaining(), "rest");

        assert!(buf.expect("(tok)").is_err());
        assert_eq!(buf.remaining(), "rest");
    }

    #[test]
    fn test_maybe_expect_with_char()
    {
        let mut buf = ParseBuffer::from(":::");

        assert!(buf.maybe_expect(':'));
        assert_eq!(buf.remaining(), "::");

        assert!(buf.maybe_expect(':'));
        assert_eq!(buf.remaining(), ":");

        assert!(buf.maybe_expect(':'));
        assert_eq!(buf.remaining(), "");

        assert!(!buf.maybe_expect(':'));
        assert_eq!(buf.remaining(), "");
    }

    #[test]
    fn test_expect_token_with_ows()
    {
        let mut buf = ParseBuffer::from(" word word ");

        assert!(buf.expect_token().is_err());

        buf.consume_ows();
        assert_eq!(buf.remaining(), "word word ");
        assert_eq!(buf.expect_token().unwrap(), "word");
        assert_eq!(buf.remaining(), " word ");

        assert!(buf.expect_token().is_err());

        buf.consume_ows();
        assert_eq!(buf.remaining(), "word ");
        assert_eq!(buf.expect_token().unwrap(), "word");
        assert_eq!(buf.remaining(), " ");

        buf.consume_ows();
        assert_eq!(buf.remaining(), "");

        // Check that consuming OWS on an empty string is okay
        buf.consume_ows();
        assert_eq!(buf.remaining(), "");
    }

    #[test]
    fn test_expect_token_with_semicolon()
    {
        let mut buf = ParseBuffer::from("tok1;tok2;tok3");

        assert_eq!(buf.expect_token().unwrap(), "tok1");
        assert_eq!(buf.remaining(), ";tok2;tok3");

        assert!(buf.expect_token().is_err());
        assert!(buf.expect(';').is_ok());
        assert_eq!(buf.remaining(), "tok2;tok3");

        assert_eq!(buf.expect_token().unwrap(), "tok2");
        assert_eq!(buf.remaining(), ";tok3");

        assert!(buf.expect_token().is_err());
        assert!(buf.maybe_expect(';'));
        assert_eq!(buf.remaining(), "tok3");

        assert_eq!(buf.expect_token().unwrap(), "tok3");
        assert_eq!(buf.remaining(), "");

        assert!(buf.expect_token().is_err());
        assert!(!buf.maybe_expect(';'));
    }

    #[test]
    fn test_expect_float()
    {
        let mut buf = ParseBuffer::from("1,1.0,2.0");

        assert_eq!(buf.expect_float().unwrap(), 1.0);
        assert!(buf.expect_float().is_err());
        assert!(buf.expect(',').is_ok());

        assert_eq!(buf.expect_float().unwrap(), 1.0);
        assert!(buf.expect_float().is_err());
        assert!(buf.expect(',').is_ok());

        assert_eq!(buf.expect_float().unwrap(), 2.0);
        assert!(buf.expect_float().is_err());
    }

    use test_case::test_case;

    #[test_case(r#"string""#,       r#"string"#   ; "Plain quoted string")]
    #[test_case(r#"\s\t\r\i\n\g""#, r#"string"#   ; "Escape every character")]
    #[test_case(r#"\"string\"""#,   r#""string""# ; "Escaped quotes")]
    #[test_case(r#"\"\\\"""#,       r#""\""#      ; "Escaped escapes")]
    fn test_expect_quoted_string(inp: &str, out: &str)
    {
        let mut buf = ParseBuffer::from(inp);

        assert_eq!(buf.expect_quoted_string().unwrap(), out);
        assert_eq!(buf.remaining(), "");
    }

    #[test_case(1)]
    #[test_case(2)]
    #[test_case(15)]
    #[test_case(16)]
    #[test_case(17)]
    fn test_expect_quoted_string_n_backslashes(n: usize)
    {
        let s: String = std::iter::repeat('\\')
            .take(n)
            .chain(std::iter::once('\"'))
            .collect();

        let mut buf = ParseBuffer::from(s.as_str());

        if n % 2 == 0
        {
            let expect: String = std::iter::repeat('\\')
                .take(n / 2)
                .collect();

            assert_eq!(&buf.expect_quoted_string().unwrap(), &expect);
            assert_eq!(buf.remaining(), "");
        }
        else
        {
            assert!(buf.expect_quoted_string().is_err());
            assert_eq!(buf.remaining(), &s);
        }
    }

    #[test]
    fn test_expect_quoted_string_unterminated()
    {
        let mut buf = ParseBuffer::from("unterminated");
        assert!(buf.expect_quoted_string().is_err());
        assert_eq!(buf.remaining(), "unterminated");
    }

}
