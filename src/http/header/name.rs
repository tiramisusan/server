use super::*;

/// The name of an HTTP header.
#[derive(Clone)]
pub enum Name
{
    Static { mix: &'static str, lower: &'static str },
    Heap { mix: String, lower: String }
}

impl Name
{
    pub const fn new_static(mix: &'static str, lower: &'static str)
        -> Self
    {
        // Do a compile-time check on the header values.
        assert!(mix.len() == lower.len());
        let mut ind = 0;
        while ind < mix.len()
        {
            let mix_ch:   char = mix.as_bytes()[ind]   as char;
            let lower_ch: char = lower.as_bytes()[ind] as char;

            assert!(!lower_ch.is_ascii_uppercase());

            assert!(lower_ch == mix_ch.to_ascii_lowercase());

            assert!(!invalid_char(mix_ch));
            assert!(!invalid_char(lower_ch));

            ind += 1;
        }

        Name::Static { mix, lower }
    }

    fn lower(&self) -> &str
    {
        match self
        {
            Self::Static { lower, .. } => lower,
            Self::Heap   { lower, .. } => lower
        }
    }

    fn mix(&self) -> &str
    {
        match self
        {
            Self::Static { mix, .. } => mix,
            Self::Heap   { mix, .. } => mix
        }
    }
}

impl PartialEq for Name
{
    fn eq(&self, oth: &Self) -> bool { self.lower().eq(oth.lower()) }
}

impl Eq for Name {}

impl PartialOrd for Name
{
    fn partial_cmp(&self, oth: &Self) -> Option<std::cmp::Ordering>
    {
        self.lower().partial_cmp(oth.lower())
    }
}

impl Ord for Name
{
    fn cmp(&self, oth: &Self) -> std::cmp::Ordering
    {
        self.lower().cmp(oth.lower())
    }
}

impl std::hash::Hash for Name
{
    fn hash<H>(&self, state: &mut H)
        where H: std::hash::Hasher
    {
        self.lower().hash(state)
    }
}

/// Check whether a character is not allowed in an header name.
const fn invalid_char(ch: char) -> bool
{
    !(ch.is_ascii_alphabetic() || ch == '-' || ch == '_')
}

impl FromStr for Name
{
    type Err = Error;

    fn from_str(s: &str) -> Result<Name>
    {
        Name::try_from(String::from(s))
    }
}

impl TryFrom<String> for Name
{
    type Error = Error;

    fn try_from(s: String) -> Result<Name>
    {
        // Check all the characters are valid
        if s.find(invalid_char).is_some()
        {
            Err(Error::from("Invalid character found in header name"))
        }
        else
        {
            let lower = s.to_lowercase();
            let mix   = s;
            Ok(Name::Heap { mix, lower })
        }
    }
}

impl std::fmt::Debug for Name
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        f.write_str(self.as_str())
    }
}

impl Name
{
    pub fn as_str(&self) -> &str { &self.mix() }
}
