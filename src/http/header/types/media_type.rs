use super::*;

/// A MIME media type.
///
/// This consists of a type, and a subtype.
///
/// See RFC 2046.
///
#[derive(Copy, Clone)]
pub struct MediaType<'a>(&'a str, &'a str);

impl<'a> ListItem<'a> for MediaType<'a>
{
    fn decode(buf: &mut ParseBuffer<'a>) -> http::Result<Self>
    {
        let a = buf.expect_token()?;
        buf.expect('/')?;
        let b = buf.expect_token()?;

        Ok(Self(a, b))
    }
}

impl Default for MediaType<'static>
{
    fn default() -> Self { MediaType::data() }
}

impl<'a> MediaType<'a>
{
    pub fn get_type(&self) -> &'a str { self.0 }

    pub fn get_subtype(&self) -> &'a str { self.1 }

    pub fn is_image(&self) -> bool { self.get_type() == "image" }

    pub fn is_multipart(&self) -> bool { self.get_type() == "multipart" }

    pub fn tuple(&self) -> (&'a str, &'a str) { (self.0, self.1) }
}

impl<'a> std::fmt::Display for MediaType<'a>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "{}/{}", self.0, self.1)
    }
}

impl<'a> std::fmt::Debug for MediaType<'a>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "{}", self)
    }
}

impl MediaType<'static>
{
    /// Guess the content type from a path.
    pub fn guess_from_path(path: impl AsRef<Path>) -> Self
    {
        match path.as_ref()
            .extension()
            .and_then(|os| os.to_str())
        {
            Some("css")  => Self("text", "css"),
            Some("txt")  => Self("text", "plain"),
            Some("md")   => Self("text", "plain"),
            Some("html") => Self("text", "html"),
            Some("jpg")  => Self("image", "jpeg"),
            Some("jpeg") => Self("image", "jpeg"),
            Some("png")  => Self("image", "png"),
            Some("gif")  => Self("image", "gif"),
            Some("mp4")  => Self("video", "mp4"),
            Some("json") => Self("application", "json"),
            _            => Self("application", "octet-stream")
        }
    }

    pub const fn text() -> Self { Self("text", "plain") }

    pub const fn data() -> Self { Self("application", "octet-stream") }

    pub const fn html() -> Self { Self("text", "html") }
}
