use super::*;

const NAME: Name = Name::new_static("Content-Length", "content-length");

#[derive(Debug)]
pub struct ContentLen(pub usize);

impl<'a> Header<'a> for ContentLen
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &'a str) -> http::Result<Self>
    {
        let len: usize = s.parse()
            .map_err(|_| http::Error::new_bad_request("Could not parse len."))?;

        Ok(ContentLen(len))
    }

    fn encode_value(&self) -> String
    {
        self.0.to_string()
    }
}
