use super::*;

const NAME: Name = Name::new_static("Server", "server");

#[derive(Debug)]
pub struct Server<'a>(pub &'a str);

impl<'a> Header<'a> for Server<'a>
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &'a str) -> http::Result<Self> { Ok(Self(s)) }

    fn encode_value(&self) -> String { self.0.to_owned() }
}
