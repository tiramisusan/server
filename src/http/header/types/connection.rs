use super::*;

const NAME: Name = Name::new_static("Connection", "connection");

#[derive(Debug)]
pub enum Connection
{
    KeepAlive,
    Close
}

impl<'a> Header<'a> for Connection
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &'a str) -> http::Result<Self>
    {
        match s.trim()
        {
            "keep-alive" => Ok(Connection::KeepAlive),
            "close"      => Ok(Connection::Close),
            _            => Err(Error::new_bad_request("Unknown value."))
        }
    }

    fn encode_value(&self) -> String
    {
        match self
        {
            Connection::KeepAlive => "keep-alive".to_owned(),
            Connection::Close     => "close".to_owned()
        }
    }
}
