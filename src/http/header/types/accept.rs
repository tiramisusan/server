use super::*;

const NAME: Name = Name::new_static("Accept", "accept");

/// Accept header
///
/// See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept
///
#[cfg_attr(test, derive(PartialEq))]
#[derive(Debug)]
pub(crate) struct Accept<'a>(Vec<AcceptItem<'a>>);

/// A specification for matching media-types in an Accept header.
#[cfg_attr(test, derive(PartialEq))]
#[derive(Debug)]
pub(crate) struct AcceptItem<'a>
{
    /// The media-type to match, or None to match any.
    ///
    /// e.g. `"text"` or `"image"`
    ///
    match_type:    Option<&'a str>,
    /// The media-subtype to match, or None to match any.
    ///
    /// e.g. `"html"` or `"jpeg"`
    ///
    match_subtype: Option<&'a str>,
    /// The quality of the match, 1.0 by default.
    quality:       f32
}

fn decode_tok_or_star<'a>(buf: &mut ParseBuffer<'a>)
    -> http::Result<Option<&'a str>>
{
    if buf.maybe_expect('*')
    {
        Ok(None)
    }
    else
    {
        Ok(Some(buf.expect_token()?))
    }
}

impl<'a> AcceptItem<'a>
{
    fn matches(&self, media: MediaType<'_>) -> bool
    {
        let type_ok = self.match_type
            .map(|mat| mat == media.get_type()).unwrap_or(true);
        let subtype_ok = self.match_subtype
            .map(|mat| mat == media.get_subtype()).unwrap_or(true);

        type_ok && subtype_ok
    }

    fn encode(&self) -> String
    {
        format!(
            "{}/{}; q={}",
            self.match_type.unwrap_or("*"),
            self.match_subtype.unwrap_or("*"),
            self.quality
        )
    }
}

impl<'a> ListItem<'a> for AcceptItem<'a>
{
    fn decode(buf: &mut ParseBuffer<'a>) -> http::Result<Self>
    {
        let match_type = decode_tok_or_star(buf)?;
        buf.expect('/')?;
        let match_subtype = decode_tok_or_star(buf)?;

        let quality;
        if buf.maybe_expect(';')
        {
            buf.consume_ows();
            buf.expect("q=")?;
            quality = buf.expect_float()?;
        }
        else
        {
            quality = 1.0;
        }

        Ok(Self { match_type, match_subtype, quality })
    }
}

impl<'a> Header<'a> for Accept<'a>
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &'a str) -> http::Result<Self>
    {
        let items = List::new(s)
            .with_sep(',')
            .iter::<AcceptItem<'a>>()
            .collect::<http::Result<Vec<_>>>()?;

        Ok(Self(items))
    }

    fn encode_value(&self) -> String
    {
        self.0.iter()
            .map(AcceptItem::encode)
            .collect::<Vec<_>>()
            .join(", ")
    }
}

use request::Quality;

fn calculate_quality(ind: usize, item: &AcceptItem<'_>) -> Quality
{
    Quality::from(item.quality).nudge(-(ind as i32))
}

impl<'a> Accept<'a>
{
    /// If a media type matches this header, return an opaque value representing
    /// the match. The greater the value, the greater the match.
    pub fn quality(&self, media: MediaType<'_>) -> Option<Quality>
    {
        info!("Accept quality {:?}", media);
        self.0.iter()
            .enumerate()
            .filter(|(_, item)| item.matches(media))
            .map(|(ind, item)| calculate_quality(ind, item))
            .max()
    }
}

#[cfg(test)]
mod test
{
    use super::*;
    use test_case::test_case;

    #[test_case(
        "*/*", MediaType::html() => Some(Quality::new_max());
        "Match any"
    )]
    #[test_case(
        "text/html", MediaType::html() => Some(Quality::new_max());
        "Match specific"
    )]
    #[test_case(
        "text/html", MediaType::text() => None;
        "No match"
    )]
    #[test_case(
        "text/html,*/*;q=0.5", MediaType::text()
            => Some(Quality::from(0.5).nudge(-1));
        "Match non-preferred any"
    )]
    #[test_case(
        "text/html,*/*;q=0.5", MediaType::html() => Some(Quality::new_max());
        "Match preferred"
    )]
    #[test_case(
        "text/html,*/*;q=0.5,text/plain;q=0.8", MediaType::text()
            => Some(Quality::from(0.8).nudge(-2));
        "Match specific non-preferred"
    )]
    fn test_quality(header: &str, media: MediaType) -> Option<Quality>
    {
        let parsed = Accept::decode_value(header).unwrap();
        parsed.quality(media)
    }

    fn item(
        match_type:    impl Into<Option<&'static str>>,
        match_subtype: impl Into<Option<&'static str>>,
        quality:       f32
    )
        -> AcceptItem<'static>
    {
        AcceptItem {
            match_type:    match_type.into(),
            match_subtype: match_subtype.into(),
            quality
        }
    }

    #[test_case(
        Accept(vec![
            item(None, None, 1.0),
            item("a",  None, 1.0),
            item(None, "b",  1.0),
            item("c",  "d",  1.0)
        ]);
        "Wildcards"
    )]
    #[test_case(
        Accept(vec![
            item("text", "html", 1.0),
            item("text", "xhtml+xml", 0.75),
            item("application", "xml", 0.5),
            item(None, None, 0.25)
        ]);
        "Qualities"
    )]
    #[test_case(Accept(vec![]); "Empty")]
    fn test_accept_reencode(h: Accept<'static>) { test_reencode(h) }
}
