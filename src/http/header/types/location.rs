use super::*;

const NAME: Name = Name::new_static("Location", "location");

#[derive(Debug)]
pub struct Location<'a>(pub &'a str);

impl<'a> Header<'a> for Location<'a>
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &'a str) -> http::Result<Self> { Ok(Self(s)) }

    fn encode_value(&self) -> String { self.0.to_owned() }
}
