use super::*;

const NAME: Name = Name::new_static("Content-Type", "content-type");

#[derive(Debug)]
pub struct ContentType<'a>
{
    media_type: MediaType<'a>,
    charset:    Option<Cow<'a, str>>,
    boundary:   Option<Cow<'a, str>>
}

impl<'a> Header<'a> for ContentType<'a>
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &'a str) -> http::Result<Self>
    {
        let mut parameters = List::new(s).with_sep(';');

        let mut rtn = Self::from(parameters.get_next::<MediaType>()?);

        for maybe_key_value in parameters.iter::<parameter::KeyValue>()
        {
            let key_value = maybe_key_value?;
            match &*key_value.0
            {
                "charset"    => rtn.charset    = Some(key_value.1),
                "boundary"   => rtn.boundary   = Some(key_value.1),
                _ => {} /* Ignore unknown params */
            }
        }

        Ok(rtn)
    }

    fn encode_value(&self) -> String
    {
        let mut rtn = self.media_type.to_string();

        let mut add_param = |k, maybe_v: &Option<Cow<str>>| {
            if let Some(v) = maybe_v
            {
                rtn.push_str(&format!(
                    "; {}=\"{}\"", k,
                    escape_string(&v, |ch| matches!(ch, '\\' | '"'))
                ));
            }
        };

        add_param("charset",    &self.charset);
        add_param("boundary",   &self.boundary);

        rtn
    }
}

impl<'a> From<MediaType<'a>> for ContentType<'a>
{
    fn from(media_type: MediaType<'a>) -> Self
    {
        Self { media_type, charset: None, boundary: None }
    }
}

impl<'a> ContentType<'a>
{
    pub fn media_type(&self) -> MediaType<'a> { self.media_type }

    #[allow(dead_code)]
    pub fn charset(&self) -> Option<&str>
    {
        self.charset.as_ref().map(|s| &**s)
    }

    pub fn boundary(&self) -> Option<&str>
    {
        self.boundary.as_ref().map(|s| &**s)
    }
}
