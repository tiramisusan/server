use super::*;

const NAME: Name =
    Name::new_static("Content-Disposition", "content-disposition");

#[derive(Debug)]
pub struct ContentDisposition<'a>
{
    dispos:   DispositionType,
    name:     Option<Cow<'a, str>>,
    filename: Option<Cow<'a, str>>
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum DispositionType
{
    Attachment,
    Inline,
    FormData
}

impl<'a> ContentDisposition<'a>
{
    pub fn attachment() -> Self
    {
        Self::with_dispos(DispositionType::Attachment)
    }

    pub fn inline() -> Self
    {
        Self::with_dispos(DispositionType::Inline)
    }

    fn with_dispos(dispos: DispositionType) -> Self
    {
        Self { dispos, name: None, filename: None }
    }

    fn with_parsed_dispos(dispos_str: &str) -> http::Result<Self>
    {
        Ok(Self::with_dispos(
            match dispos_str
            {
                "attachment" => DispositionType::Attachment,
                "inline"     => DispositionType::Inline,
                "form-data"  => DispositionType::FormData,
                _ => return Err(Error::new_bad_request("Unknown disposition."))
            }
        ))
    }

    pub fn dispos(&self) -> DispositionType { self.dispos }

    pub fn name(&self) -> Option<&str>
    {
        self.name.as_ref().map(|s| &**s)
    }

    pub fn filename(&self) -> Option<&str>
    {
        self.filename.as_ref().map(|s| &**s)
    }
}

impl<'a> Header<'a> for ContentDisposition<'a>
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &'a str) -> http::Result<Self>
    {
        let mut parameters = List::new(s).with_sep(';');

        let dispos = parameters.get_next::<parameter::Token>()?;

        let mut rtn = Self::with_parsed_dispos(&dispos.0)?;

        for maybe_key_value in parameters
            .iter::<parameter::KeyValue>()
        {
            let key_value = maybe_key_value?;
            match &*key_value.0
            {
                "name"     => rtn.name     = Some(key_value.1),
                "filename" => rtn.filename = Some(key_value.1),
                _ => { /* Unknown directive - ignore */ }
            }
        }

        Ok(rtn)
    }

    fn encode_value(&self) -> String
    {
        let mut rtn = match self.dispos
            {
                DispositionType::Attachment => "attachment".to_owned(),
                DispositionType::Inline     => "inline".to_owned(),
                DispositionType::FormData   => "form-data".to_owned()
            };

        let mut add_param = |k, maybe_v: &Option<Cow<str>>| {
            if let Some(v) = maybe_v
            {
                rtn.push_str(&format!(
                    "; {}=\"{}\"", k,
                    escape_string(&v, |ch| matches!(ch, '\\' | '"'))
                ));
            }
        };

        add_param("name",     &self.name);
        add_param("filename", &self.filename);

        rtn
    }
}
