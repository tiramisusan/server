use super::*;

const NAME: Name = Name::new_static("Vary", "vary");

#[derive(Debug)]
pub struct Vary(Vec<Name>);

impl<'a> Header<'a> for Vary
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &'a str) -> http::Result<Self>
    {
        let headers = List::new(s)
            .with_sep(',')
            .iter::<parameter::Token<'a>>()
            .flat_map(|t| t.map(|tok| Name::try_from(tok.0.into_owned())))
            .collect::<http::Result<Vec<_>>>()?;

        Ok(From::from(headers))
    }

    fn encode_value(&self) -> String
    {
        self.0.iter()
            .map(|i| i.as_str())
            .collect::<Vec<_>>()
            .join(", ")
    }
}

impl<T> From<T> for Vary where T: IntoIterator<Item=Name>
{
    fn from(headers: T) -> Self { Self(headers.into_iter().collect()) }
}

