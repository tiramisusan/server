use super::*;

use chrono::{DateTime, Utc};

const NAME: Name = Name::new_static("Date", "date");

#[derive(Debug)]
pub struct Date(DateTime<Utc>);

impl Date
{
    pub fn now() -> Self { Self(Utc::now()) }
}

impl<'a> Header<'a> for Date
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &str) -> http::Result<Self> { Ok(Self(parse_date(s)?)) }

    fn encode_value(&self) -> String
    {
        self.0.to_rfc2822()
    }
}
