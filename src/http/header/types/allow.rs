use super::*;

const NAME: Name = Name::new_static("Allow", "allow");

#[derive(Debug)]
pub struct Allow(pub MethodSet);

impl<'a> Header<'a> for Allow
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &'a str) -> http::Result<Self>
    {
        let mut methods = MethodSet::default();

        for maybe_token in List::new(s)
            .with_sep(',')
            .iter::<parameter::Token>()
        {
            methods |= Method::from_str(&maybe_token?.0)?;
        }

        Ok(Self(methods))
    }

    fn encode_value(&self) -> String
    {
        let strings = self.0.iter()
            .map(|meth| meth.to_string())
            .collect::<Vec<_>>();

        strings.join(", ")
    }
}
