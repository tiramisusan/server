use super::*;

const NAME: Name = Name::new_static("User-Agent", "user-agent");

#[derive(Debug)]
pub struct UserAgent<'a>(&'a str);

impl<'a> Header<'a> for UserAgent<'a>
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &'a str) -> http::Result<Self> { Ok(UserAgent(s)) }

    fn encode_value(&self) -> String { self.0.to_owned() }
}

