use super::*;

const NAME: Name =
    Name::new_static("Cache-Control", "cache-control");

#[derive(Debug)]
pub struct CacheControl<'a>(Cow<'a, [CacheControlDirective]>);

impl<'a, T> From<T> for CacheControl<'a>
    where Cow<'a, [CacheControlDirective]>: From<T>
{
    fn from(val: T) -> Self { Self(From::from(val)) }
}

impl<'a> Header<'a> for CacheControl<'static>
{
    fn name() -> Name { NAME }

    fn decode_value(s: &'a str) -> http::Result<Self>
    {
        let directives = List::new(s)
            .with_sep(',')
            .iter::<CacheControlDirective>()
            .collect::<http::Result<Vec<_>>>()?;

        Ok(From::from(directives))
    }

    fn encode_value(&self) -> String
    {
        self.0.iter()
            .map(CacheControlDirective::encode)
            .collect::<Vec<_>>()
            .join(", ")
    }
}

#[cfg(test)]
mod test
{
    use super::*;
    use CacheControlDirective::*;

    fn secs(n: u64) -> Duration { Duration::from_secs(n) }

    #[test]
    fn test_decode()
    {
        let header = CacheControl::decode_value(
            "max-age=123, \
            stale-while-revalidate=456, \
            immutable, \
            must-revalidate, \
            no-store"
        )
            .unwrap();

        let mut iter = header.0.into_iter();

        assert_eq!(iter.next(), Some(&MaxAge(secs(123))));
        assert_eq!(iter.next(), Some(&StaleWhileRevalidate(secs(456))));
        assert_eq!(iter.next(), Some(&Immutable));
        assert_eq!(iter.next(), Some(&MustRevalidate));
        assert_eq!(iter.next(), Some(&NoStore));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_encode()
    {
        let header = CacheControl::from(vec![
            MaxAge(secs(123)),
            StaleWhileRevalidate(secs(456)),
            Immutable,
            MustRevalidate,
            NoStore
        ]);

        assert_eq!(
            header.encode_value(),
            "max-age=123, \
            stale-while-revalidate=456, \
            immutable, \
            must-revalidate, \
            no-store"
        );
    }
}
