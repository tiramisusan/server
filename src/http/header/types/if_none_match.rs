use super::*;

const NAME: Name =
    Name::new_static("If-None-Match", "if-none-match");

#[derive(Debug)]
pub struct IfNoneMatch<'a>(Cow<'a, [ETag<'a>]>);

impl<'a> Header<'a> for IfNoneMatch<'a>
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &'a str) -> http::Result<Self>
    {
        let etags = List::new(s)
            .with_sep(',')
            .iter::<ETag<'a>>()
            .collect::<http::Result<Vec<_>>>()?;

        Ok(From::from(etags))
    }

    fn encode_value(&self) -> String
    {
        self.0.iter()
            .map(|etag| etag.encode_value())
            .collect::<Vec<_>>()
            .join(", ")
    }
}

impl<'a, T> From<T> for IfNoneMatch<'a>
    where Cow<'a, [ETag<'a>]>: From<T>
{
    fn from(tags: T) -> Self
    {
        Self(From::from(tags))
    }
}

impl<'a> IfNoneMatch<'a>
{
    pub fn matches(&self, hash: &str) -> bool
    {
        self.0.iter().any(|sent| sent.matches(hash))
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn test_decode()
    {
        let header = IfNoneMatch::decode_value("\"Strong\", W/\"Weak\"")
            .unwrap();

        let mut iter = header.0.into_iter();

        assert_eq!(iter.next(), Some(&ETag::new_strong("Strong")));
        assert_eq!(iter.next(), Some(&ETag::new_weak("Weak")));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_decode_any()
    {
        let header = IfNoneMatch::decode_value("*")
            .unwrap();

        let mut iter = header.0.into_iter();

        assert_eq!(iter.next(), Some(&ETag::Any));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_encode()
    {
        let header = IfNoneMatch::from(
            vec![ETag::new_strong("Strong"), ETag::new_weak("Weak")]);

        assert_eq!(header.encode_value(), "\"Strong\", W/\"Weak\"");
    }

    #[test]
    fn test_matches()
    {
        let header = IfNoneMatch::decode_value("\"Strong\", W/\"Weak\"")
            .unwrap();

        assert!(header.matches("Strong"));
        assert!(header.matches("Weak"));
        assert!(!header.matches("Wrong"));
    }

    #[test]
    fn test_matches_any()
    {
        let header = IfNoneMatch::decode_value("*")
            .unwrap();

        assert!(header.matches("It should"));
        assert!(header.matches("match"));
        assert!(header.matches("anything!"));
    }
}

