use super::*;

const NAME: Name = Name::new_static("ETag", "etag");

#[derive(PartialEq, Debug, Clone)]
pub enum ETag<'a>
{
    Strong(Cow<'a, str>),
    Weak(Cow<'a, str>),
    Any
}

impl<'a> ListItem<'a> for ETag<'a>
{
    fn decode(buf: &mut ParseBuffer<'a>) -> http::Result<Self>
    {
        if buf.maybe_expect('*')
        {
            Ok(ETag::Any)
        }
        else if buf.maybe_expect("W/\"")
        {
            Ok(ETag::new_weak(buf.expect_quoted_string()?))
        }
        else
        {
            buf.expect('"')?;
            Ok(ETag::new_strong(buf.expect_quoted_string()?))
        }
    }
}

impl<'a> Header<'a> for ETag<'a>
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &'a str) -> http::Result<Self>
    {
        Self::decode_single(s)
    }

    fn encode_value(&self) -> String
    {
        match self
        {
            Self::Strong(s) => format!("\"{}\"", s),
            Self::Weak(s)   => format!("W/\"{}\"", s),
            Self::Any       => "*".to_owned()
        }
    }
}

impl<'a> ETag<'a>
{
    pub fn new_strong<S>(src: S) -> Self
        where Cow<'a, str>: From<S>
    {
        Self::Strong(From::from(src))
    }

    pub fn new_weak<S>(src: S) -> Self
        where Cow<'a, str>: From<S>
    {
        Self::Weak(From::from(src))
    }

    pub fn matches(&self, hash: &str) -> bool
    {
        match self
        {
            ETag::Strong(inner) => *inner == hash,
            ETag::Weak(inner)   => *inner == hash,
            ETag::Any           => true
        }
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn test_decode()
    {
        assert_eq!(
            ETag::decode_value("\"Tag1\"").unwrap(),
            ETag::new_strong("Tag1")
        );
        assert_eq!(
            ETag::decode_value("W/\"Tag1\"").unwrap(),
            ETag::new_weak("Tag1")
        );
        assert_eq!(
            ETag::decode_value("*").unwrap(),
            ETag::Any
        );
    }

    #[test]
    fn test_encode()
    {
        assert_eq!(ETag::new_strong("Tag1").encode_value(), "\"Tag1\"");
        assert_eq!(ETag::new_weak("Tag1").encode_value(), "W/\"Tag1\"");
        assert_eq!(ETag::Any.encode_value(), "*");
    }
}
