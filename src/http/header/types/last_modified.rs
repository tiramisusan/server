use super::*;

use chrono::{DateTime, Utc};

const NAME: Name =
    Name::new_static("Last-Modified", "last-modified");

#[derive(Debug)]
pub struct LastModified(pub DateTime<Utc>);

impl<'a> Header<'a> for LastModified
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &str) -> http::Result<Self> { Ok(Self(parse_date(s)?)) }

    fn encode_value(&self) -> String
    {
        self.0.to_rfc2822()
    }
}
