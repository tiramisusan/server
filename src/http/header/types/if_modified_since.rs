use super::*;

use chrono::{DateTime, Utc};

const NAME: Name =
    Name::new_static("If-Modified-Since", "if-modified-since");

#[derive(Debug)]
pub struct IfModifiedSince(pub DateTime<Utc>);

impl<'a> Header<'a> for IfModifiedSince
{
    fn name() -> Name { NAME.clone() }

    fn decode_value(s: &str) -> http::Result<Self> { Ok(Self(parse_date(s)?)) }

    fn encode_value(&self) -> String
    {
        self.0.to_rfc2822()
    }
}
