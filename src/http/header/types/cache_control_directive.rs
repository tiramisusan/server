use super::*;

#[derive(PartialEq, Clone, Copy, Debug)]
pub enum CacheControlDirective
{
    MaxAge(Duration),
    StaleWhileRevalidate(Duration),
    NoStore,
    Immutable,
    MustRevalidate
}

fn parse_duration(s: &Option<Cow<str>>) -> http::Result<Duration>
{
    if let Some(secs) = s.as_ref().and_then(|s| s.parse::<u64>().ok())
    {
        Ok(Duration::from_secs(secs))
    }
    else
    {
        Err(http::Error::new_bad_request(
            "Missing or invalid cache-control duration."
        ))
    }
}

impl<'a> ListItem<'a> for CacheControlDirective
{
    fn decode(buf: &mut ParseBuffer<'a>) -> http::Result<Self>
    {
        use CacheControlDirective::*;

        let param = parameter::Optional::decode(buf)?;

        match &*param.0
        {
            "max-age" =>
                Ok(MaxAge(parse_duration(&param.1)?)),
            "stale-while-revalidate" =>
                Ok(StaleWhileRevalidate(parse_duration(&param.1)?)),
            "immutable" =>
                Ok(Immutable),
            "must-revalidate" =>
                Ok(MustRevalidate),
            "no-store" =>
                Ok(NoStore),
            directive =>
            {
                Err(http::Error::new_bad_request(
                    format!("Unknown cache-control directive: {:?}", directive)
                ))
            }
        }
    }
}

impl CacheControlDirective
{
    pub fn encode(&self) -> String
    {
        use CacheControlDirective::*;
        match self
        {
            Immutable      => "immutable".to_owned(),
            MustRevalidate => "must-revalidate".to_owned(),
            NoStore        => "no-store".to_owned(),
            MaxAge(dur) =>
                format!("max-age={}", dur.as_secs()),
            StaleWhileRevalidate(dur) =>
                format!("stale-while-revalidate={}", dur.as_secs())
        }
    }
}
