use super::*;

/// A structure managing authorisation for HTTP connections.
pub struct Authoriser
{
    semaphore:       Arc<tokio::sync::Semaphore>,
    next_id:         std::sync::atomic::AtomicUsize,
    request_timeout: Option<Duration>
}

/// A handle for an HTTP request.
///
/// This represents permission from the server to handle an HTTP request.
///
pub struct Permit
{
    _permit:             tokio::sync::OwnedSemaphorePermit,
    pub id:              usize,
    pub request_timeout: Option<Duration>
}

impl Authoriser
{
    pub fn new(conf: &config::ServerConfig) -> Self
    {
        // Create a semaphore lock to limit concurrent requests.
        let semaphore = tokio::sync::Semaphore::new(conf.max_concurrent);

        Self
        {
            semaphore:       Arc::new(semaphore),
            next_id:         Default::default(),
            request_timeout: conf.request_timeout.clone()
        }
    }

    /// Authorise for a connection, or return None.
    pub fn permit(&self) -> Option<Permit>
    {
        use std::sync::atomic::Ordering;

        let permit = self.semaphore.clone().try_acquire_owned().ok()?;
        let id = self.next_id.fetch_add(1, Ordering::Relaxed);
        let request_timeout = self.request_timeout.clone();

        Some(Permit { _permit: permit, id, request_timeout })
    }

    /// Get the current number of remaining unused permits.
    pub fn available_permits(&self) -> usize
    {
        self.semaphore.available_permits()
    }
}
