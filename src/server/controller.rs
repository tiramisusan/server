use super::*;

/// A structure used to control a server running in a tokio runtime.
pub struct Controller
{
    running_tx: watch::Sender<bool>,
    join:       tokio::task::JoinHandle<()>,
    tokio:      Option<tokio::runtime::Runtime>
}

impl Drop for Controller
{
    fn drop(&mut self)
    {
        self.stop();
    }
}

impl Controller
{
    pub(super) fn new(server: Server) -> Self
    {
        let task =
            ServerTask::new(server.listener, server.mounts, &server.conf);

        let (running_tx, running_rx) = watch::channel(true);
        let join = server.tokio.spawn(task.task(running_rx));

        Self { running_tx, join, tokio: Some(server.tokio) }
    }

    pub fn stop(&mut self)
    {
        if let Some(tokio) = self.tokio.take()
        {
            // Spawn a task on the runtime whose job is to kill the server.
            tokio.block_on(async move {
                if let Err(e) = self.running_tx.send(false)
                {
                    error!("Error sending disable to running server: {}", e);
                    self.join.abort();
                }
            });

            // Wait up to 5 seconds for the server to die.
            tokio.shutdown_timeout(Duration::from_secs(5));
        }
    }
}
