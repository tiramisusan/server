use super::*;

pub(super) struct ServerTask
{
    listener: TcpListener,
    mounts:   Arc<endpoint::Node>,
    html_env: Arc<html::Environment>,
    auth:     Arc<Authoriser>
}

fn create_html_env(conf: &config::ServerConfig) -> html::Environment
{
    html::Environment::new(&conf.template_directories)
}

impl ServerTask
{
    pub fn new(
        listener: TcpListener,
        mounts:   endpoint::Node,
        conf:     &config::ServerConfig
    )
        -> Self
    {
        ServerTask
        {
            listener,
            mounts:   Arc::new(mounts),
            html_env: Arc::new(create_html_env(conf)),
            auth:     Arc::new(Authoriser::new(conf))
        }
    }

    async fn handle_connection(&self, mut stream: TcpStream, addr: SocketAddr)
    {
        if let Some(permit) = self.auth.permit()
        {
            info!(
                "Accepted request {} from {:?} ({} permits left)",
                permit.id, addr, self.auth.available_permits()
            );

            let conn = conn::Connection::new(stream, addr);
            let mut handler =
                Handler::new(conn, &self.mounts, &self.html_env, permit);

            task::spawn(async move {
                if let Err(e) = handler.handle().await
                {
                    error!("Error handling connection: {}", e);
                }
            });
        }
        else
        {
            warn!("Rejected connection from {:?}", addr);
            stream.shutdown().await.unwrap()
        }
    }

    pub async fn task(self, mut running_rx: watch::Receiver<bool>)
    {
        while *running_rx.borrow() == true
        {
            tokio::select!
            {
                maybe_connection = self.listener.accept() =>
                {
                    match maybe_connection
                    {
                        Ok((stream, addr)) =>
                        {
                            self.handle_connection(stream, addr).await;
                        }
                        Err(e) =>
                        {
                            error!("Error accepting connection: {}", e);
                        }
                    }
                }
                _ = running_rx.changed() => { }
            }
        }
    }
}
