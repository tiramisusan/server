use super::*;

/// A structure to handle a connection from a client.
pub(super) struct Handler
{
    conn:     conn::Connection,
    mounts:   Arc<endpoint::Node>,
    html_env: Arc<html::Environment>,
    permit:   Permit
}

async fn apply_http_timeout<F, O>(timeout: Option<Duration>, fut: F)
    -> http::Result<O>
    where F: std::future::Future<Output=http::Result<O>>
{
    match timeout
    {
        None =>
            fut.await,
        Some(dur) =>
            tokio::time::timeout(dur, fut).await
                .unwrap_or(Err(http::Error::RequestTimeout))
    }
}

async fn receive_request<'a>(permit: &Permit, conn: &'a mut conn::Connection)
    -> http::Result<http::Request<'a>>
{
    let req_fut = http::receive_request(conn);
    apply_http_timeout(permit.request_timeout, req_fut).await
}


impl Handler
{
    pub fn new(
        conn:     conn::Connection,
        mounts:   &Arc<endpoint::Node>,
        html_env: &Arc<html::Environment>,
        permit:   Permit
    )
        -> Self
    {
        Self
        {
            conn,
            mounts:   mounts.clone(),
            html_env: html_env.clone(),
            permit
        }
    }

    /// Get a response to a client request.
    async fn get_response(&mut self) -> (http::Response, http::ContentData)
    {
        match receive_request(&self.permit, &mut self.conn).await
        {
            // The request was received successfully
            Ok(mut req) =>
            {
                // Try to find an endpoint
                let uri = req.full_uri().to_owned();
                let result = self.mounts.handle(&mut req, &uri).await
                    .unwrap_or_else(
                        // No endpoint was found - 404
                        || Err(http::Error::new_not_found(
                            req.full_uri().fs_path("")
                        ))
                    );

                // Get the response if Ok, or create one from the Err.
                let mut rsp = result.unwrap_or_else(From::from);
                let rendered = rsp.render(&self.html_env, Some(req)).await;

                (rsp, rendered)
            }
            // An error during the request parsing
            Err(req_err) =>
            {
                let mut rsp = http::Response::from(req_err);
                let rendered = rsp.render(&self.html_env, None).await;

                (rsp, rendered)
            }
        }
    }

    /// Handle the client.
    pub async fn handle(&mut self) -> io::Result<()>
    {
        let request_id = self.permit.id;
        logging::context::run(
            async move {
                let (rsp, rendered) = self.get_response().await;
                rsp.send(&mut self.conn, rendered).await
            },
            request_id
        ).await
    }
}
