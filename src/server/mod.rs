use super::*;

use net::{TcpListener, TcpStream};
use tokio::sync::watch;
use std::net::SocketAddr;

mod controller;
pub use controller::Controller;

mod handler;
use handler::Handler;

mod authoriser;
use authoriser::{Permit, Authoriser};

mod server_task;
use server_task::ServerTask;

pub struct Server
{
    conf:     config::ServerConfig,
    tokio:    tokio::runtime::Runtime,
    listener: TcpListener,
    mounts:   endpoint::Node
}

async fn create_listener(
    conf:          &config::ServerConfig,
    override_addr: &Option<SocketAddr>
)
    -> StdResult<TcpListener>
{
    TcpListener::bind(override_addr.unwrap_or(conf.addr.parse()?))
        .await
        .annotate("Could not bind TCP Listener")
}

fn create_tokio_runtime(conf: &config::ServerConfig)
    -> StdResult<tokio::runtime::Runtime>
{
    tokio::runtime::Builder::new_multi_thread()
        .worker_threads(conf.num_threads)
        .enable_all()
        .build()
        .annotate("Could not build tokio runtime")
}

fn create_conf(block: confiner::Block) -> StdResult<config::ServerConfig>
{
    use serde::Deserialize;
    config::ServerConfig::deserialize(block.as_ref())
        .annotate("Could not deserialize server config")
}

async fn create_mounts(
    block:  confiner::Block,
    loader: &confiner::Loader,
    conf:   &config::ServerConfig
)
    -> StdResult<endpoint::Node>
{
    let mut by_name = HashMap::new();
    endpoint::Node::create_list(&block, loader, &conf, &mut by_name)
        .annotate("could not create mounts")
}

impl Server
{
    pub fn new(
        block:         confiner::Block,
        loader:        &confiner::Loader,
        override_addr: &Option<SocketAddr>
    )
        -> StdResult<Self>
    {
        let conf     = create_conf(block.clone())?;
        let tokio    = create_tokio_runtime(&conf)?;
        let mounts   = tokio.block_on(create_mounts(block, loader, &conf))?;
        let listener = tokio.block_on(create_listener(&conf, override_addr))?;

        Ok(Self { conf, tokio, listener, mounts })
    }

    pub fn local_addr(&self) -> SocketAddr
    {
        self.listener.local_addr()
            .expect("Could not get socket local address")
    }

    pub fn start(self) -> Controller
    {
        info!("Starting server on {:?}", self.local_addr());
        Controller::new(self)
    }
}
