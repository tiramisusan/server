//use super::*;

pub struct AnnotatedError
{
    outer: StdError,
    inner: StdError
}

impl AnnotatedError
{
    fn new<O, I>(outer: O, inner: I) -> Self
        where StdError: From<O>, StdError: From<I>
    {
        Self { outer: From::from(outer), inner: From::from(inner) }
    }
}

impl std::fmt::Debug for AnnotatedError
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        self.outer.fmt(f)
    }
}

impl std::fmt::Display for AnnotatedError
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        self.outer.fmt(f)
    }
}

impl std::error::Error for AnnotatedError
{
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)>
    {
        Some(self.inner.as_ref())
    }
}

pub type StdError = Box<dyn std::error::Error + Sync + Send>;
pub type StdResult<T=()> = Result<T, StdError>;

pub trait AnnotateResult
{
    type Output;
    fn annotate_with<F, E>(self, cb: F) -> Self::Output
        where F: FnOnce() -> E, StdError: From<E>;

    fn annotate<E>(self, e: E) -> Self::Output
        where StdError: From<E>;
}

impl<T, I> AnnotateResult for Result<T, I>
    where StdError: From<I>
{
    type Output = StdResult<T>;

    fn annotate_with<F, E>(self, cb: F) -> StdResult<T>
        where F: FnOnce() -> E, StdError: From<E>
    {
        self.map_err::<StdError, _>(
            |inner| Box::new(AnnotatedError::new(cb(), inner))
        )
    }

    fn annotate<E>(self, outer: E) -> StdResult<T>
        where StdError: From<E>
    {
        self.map_err::<StdError, _>(
            |inner| Box::new(AnnotatedError::new(outer, inner))
        )
    }
}

pub fn format_error(
    mut e: &dyn std::error::Error,
    fmt: &mut impl std::fmt::Write
)
    -> std::fmt::Result
{
    write!(fmt, "{}\n", e)?;

    let mut index = 1;

    while let Some(src) = e.source()
    {
        write!(fmt, " [{}] => {}\n", index, src)?;
        e = src;
        index += 1;
    }

    Ok(())
}

pub fn format_error_string(e: &dyn std::error::Error) -> String
{
    let mut s = String::new();
    format_error(e, &mut s).unwrap();
    s
}
